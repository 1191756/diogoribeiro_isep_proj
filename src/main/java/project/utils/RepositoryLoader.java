package project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import project.model.account.Account;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

@Profile("!test")
@Component
public class RepositoryLoader implements ApplicationRunner {
    @Autowired
    private final PersonRepository personRepository;
    @Autowired
    private final GroupRepository groupRepository;
    @Autowired
    private final AccountRepository accountRepository;
    @Autowired
    private final LedgerRepository ledgerRepository;

    public RepositoryLoader(PersonRepository personRepository, GroupRepository groupRepository, AccountRepository accountRepository, LedgerRepository ledgerRepository) {
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
        this.accountRepository = accountRepository;
        this.ledgerRepository = ledgerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria Albertina Kyriacos");
        Email mariaEmail = new Email("maria@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José Adolfo Panayiotou");
        Email joseEmail = new Email("jose@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Georgios Kyriacos Panayiotou");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());
        son.addSibling(sibling.getPersonID());

        //Categories
        Category categoryAllowances = new Category(new Designation("Allowances"));
        Category categoryShoes = new Category(new Designation("Shoes"));
        Category categoryUtilities = new Category(new Designation("Utilities"));
        mother.addCategory(categoryAllowances);
        son.addCategory(categoryAllowances);
        son.addCategory(categoryShoes);
        father.addCategory(categoryAllowances);
        father.addCategory(categoryShoes);
        father.addCategory(categoryUtilities);

        personRepository.save(mother);
        personRepository.save(father);
        personRepository.save(son);
        personRepository.save(sibling);

        // GROUPS
        Description groupDescription = new Description("Family");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();
        familyGroup.addMember(mother.getPersonID());

        Description group2Description = new Description("Friends");
        Group friendsGroup = new Group(group2Description, father.getPersonID());
        GroupID friendsGroupID = friendsGroup.getID();

        Description group3Description = new Description("Students");
        Group estudantesGroup = new Group(group3Description, father.getPersonID());
        GroupID estudantesGroupID = estudantesGroup.getID();
        groupRepository.save(estudantesGroup);

        familyGroup.addCategory(categoryAllowances);
        familyGroup.addCategory(categoryShoes);
        Group savedGroup = groupRepository.save(familyGroup);

        friendsGroup.addCategory(categoryAllowances);
        friendsGroup.addCategory(categoryShoes);
        friendsGroup.addMember(father.getPersonID());
        groupRepository.save(friendsGroup);

        Account account1 = new Account(new Denomination("Paycheck account"), new Description("Salary values"), father.getPersonID());
        Account account2 = new Account(new Denomination("Home spending account"), new Description("Shoes, utilities, etc"), father.getPersonID());
        Account account3 = new Account(new Denomination("General spending account"), new Description("Generic devit account"), father.getPersonID());
        Account account4 = new Account(new Denomination("Conta Grupo 1"), new Description("Fabulous Group 1"), familyGroup.getID());
        Account account5 = new Account(new Denomination("Conta Grupo 2"), new Description("Fabulous Group 2"), familyGroup.getID());
        Account account6 = new Account(new Denomination("Conta Grupo 3"), new Description("Fabulous Group 3"), familyGroup.getID());
        Account account7 = new Account(new Denomination("Friends account"), new Description("Fabulous Group 3"), friendsGroup.getID());
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
        accountRepository.save(account4);
        accountRepository.save(account5);
        accountRepository.save(account6);
        accountRepository.save(account7);

        Ledger fatherLedger = new Ledger(new LedgerID(father.getPersonID()));
        Ledger familyLedger = new Ledger(new LedgerID(familyGroup.getID()));
        Ledger friendsLedger = new Ledger(new LedgerID(friendsGroup.getID()));
        Ledger estudantesGroupLedger = new Ledger(new LedgerID(estudantesGroup.getID()));

        Transaction transaction = new Transaction(50.0, new Description("Son's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 0)), categoryAllowances, account3.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);
        Transaction transaction2 = new Transaction(50.0, new Description("Daughter's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 1)), categoryAllowances, account3.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);
        Transaction transaction3 = new Transaction(432.0, new Description("Shoes week 1"), new TransactionDate(LocalDateTime.of(2020, 6, 5, 0, 0, 2)), categoryShoes, account3.getAccountID(), account2.getAccountID(), TransactionType.DEBIT);
        Transaction transaction4 = new Transaction(118.0, new Description("Shoes week 2"), new TransactionDate(LocalDateTime.of(2020, 6, 10, 0, 0, 3)), categoryShoes, account3.getAccountID(), account2.getAccountID(), TransactionType.DEBIT);
        Transaction transaction5 = new Transaction(122.0, new Description("Electricity"), new TransactionDate(LocalDateTime.of(2020, 6, 11, 0, 0, 4)), categoryUtilities, account3.getAccountID(), account2.getAccountID(), TransactionType.DEBIT);
        Transaction transaction6 = new Transaction(34.0, new Description("Water"), new TransactionDate(LocalDateTime.of(2020, 6, 12, 0, 0, 5)), categoryUtilities, account3.getAccountID(), account2.getAccountID(), TransactionType.DEBIT);
        Transaction transaction7 = new Transaction(53.0, new Description("Natural Gas"), new TransactionDate(LocalDateTime.of(2020, 6, 12, 0, 0, 6)), categoryUtilities, account3.getAccountID(), account2.getAccountID(), TransactionType.DEBIT);
        Transaction transaction8 = new Transaction(42.0, new Description("Natural Gas"), new TransactionDate(LocalDateTime.of(2020, 6, 14, 0, 0, 6)), categoryUtilities, account4.getAccountID(), account5.getAccountID(), TransactionType.DEBIT);

        fatherLedger.addTransaction(transaction);
        fatherLedger.addTransaction(transaction2);
        fatherLedger.addTransaction(transaction3);
        fatherLedger.addTransaction(transaction4);
        fatherLedger.addTransaction(transaction5);
        fatherLedger.addTransaction(transaction6);
        fatherLedger.addTransaction(transaction7);
        ledgerRepository.save(fatherLedger);

        familyLedger.addTransaction(transaction8);
        familyLedger.addTransaction(transaction2);
        familyLedger.addTransaction(transaction4);
        familyLedger.addTransaction(transaction6);
        ledgerRepository.save(familyLedger);

        friendsLedger.addTransaction(transaction);
        friendsLedger.addTransaction(transaction3);
        friendsLedger.addTransaction(transaction5);
        friendsLedger.addTransaction(transaction7);
        ledgerRepository.save(friendsLedger);
        ledgerRepository.save(estudantesGroupLedger);
    }
}

