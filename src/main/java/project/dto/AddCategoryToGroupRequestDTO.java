package project.dto;

import project.model.shared.Designation;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddCategoryToGroupRequestDTO {
    private Designation designation;
    private GroupID groupDescription;
    private PersonID responsibleEmail;

    public AddCategoryToGroupRequestDTO(Designation designation, GroupID groupDescription, PersonID responsibleEmail) {
        this.designation = designation;
        this.groupDescription = groupDescription;
        this.responsibleEmail = responsibleEmail;
    }

    public GroupID getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(GroupID groupDescription) {
        this.groupDescription = groupDescription;
    }

    public PersonID getResponsibleEmail() {
        return responsibleEmail;
    }

    public void setResponsibleEmail(PersonID responsibleEmail) {
        this.responsibleEmail = responsibleEmail;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryToGroupRequestDTO)) return false;
        AddCategoryToGroupRequestDTO that = (AddCategoryToGroupRequestDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, groupDescription, responsibleEmail);
    }
}