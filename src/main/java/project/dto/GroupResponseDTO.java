package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GroupResponseDTO extends RepresentationModel<GroupResponseDTO> {
    private String groupDescription;
    private List<String> responsibles;
    private List<String> members;
    private List<String> categories;

    public GroupResponseDTO(String groupDescription, List<String> responsibles, List<String> members, List<String> categories) {
        this.groupDescription = groupDescription;
        this.responsibles = new ArrayList<>(responsibles);
        this.members = new ArrayList<>(members);
        this.categories = new ArrayList<>(categories);
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public List<String> getResponsibles() {
        return new ArrayList<>(responsibles);
    }

    public void setResponsibles(List<String> responsibles) {
        this.responsibles = new ArrayList<>(responsibles);
    }

    public List<String> getMembers() {
        return new ArrayList<>(members);
    }

    public void setMembers(List<String> members) {
        this.members = new ArrayList<>(members);
    }

    public List<String> getCategories() {
        return new ArrayList<>(categories);
    }

    public void setCategories(List<String> categories) {
        this.categories = new ArrayList<>(categories);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupResponseDTO that = (GroupResponseDTO) o;
        return Objects.equals(groupDescription, that.groupDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupDescription);
    }
}