package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

/**
 * DTO Class that is used when responding to a GetPerson(s) request
 */
public class PersonsResponseDTO extends RepresentationModel<PersonsResponseDTO> {
    private String personID;
    private String name;
    private String birthdate;
    private String birthplace;
    private String mother;
    private String father;
    private String siblings;
    private String categories;

    public PersonsResponseDTO(String personID, String name, String birthdate, String birthplace, String mother, String father, String siblings, String categories) {
        this.personID = personID;
        this.name = name;
        this.birthdate = birthdate;
        this.birthplace = birthplace;
        this.mother = mother;
        this.father = father;
        this.siblings = siblings;
        this.categories = categories;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getSiblings() {
        return siblings;
    }

    public void setSiblings(String siblings) {
        this.siblings = siblings;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonsResponseDTO that = (PersonsResponseDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthdate, that.birthdate) &&
                Objects.equals(birthplace, that.birthplace) &&
                Objects.equals(mother, that.mother) &&
                Objects.equals(father, that.father) &&
                Objects.equals(siblings, that.siblings) &&
                Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), personID, name, birthdate, birthplace, mother, father, siblings, categories);
    }
}
