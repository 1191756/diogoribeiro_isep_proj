package project.dto;

import java.util.Objects;

public class DashboardPieDTO {
    String id;
    String label;
    int value;

    public DashboardPieDTO(String id, String label, int value) {
        this.id = id;
        this.label = label;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardPieDTO that = (DashboardPieDTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", label:'" + label + '\'' +
                ", value:" + value +
                '}';
    }
}
