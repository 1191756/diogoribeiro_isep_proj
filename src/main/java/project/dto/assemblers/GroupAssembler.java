package project.dto.assemblers;

import project.dto.GroupDTO;
import project.model.group.Group;

public class GroupAssembler {

    protected GroupAssembler() {
        throw new AssertionError();
    }

    public static GroupDTO mapToDTO(Group group) {
        GroupDTO groupDTO = new GroupDTO();

        groupDTO.setGroupID(group.getID());
        groupDTO.setResponsibles(group.getResponsibles());
        groupDTO.setMembers(group.getMembers());
        groupDTO.setCategories(group.getCategories());
        groupDTO.setCreationDate(group.getCreationDate());

        return groupDTO;
    }
}
