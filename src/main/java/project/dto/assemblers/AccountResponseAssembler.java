package project.dto.assemblers;

import project.dto.AccountDTO;
import project.dto.AccountResponseDTO;

public class AccountResponseAssembler {

    protected AccountResponseAssembler() {
        throw new AssertionError();
    }

    public static AccountResponseDTO mapToDTO(AccountDTO accountDTO) {
        return new AccountResponseDTO(accountDTO.getAccountID().getDenomination().toString(), accountDTO.getDescription().toString(), accountDTO.getAccountID().getOwnerID().toString());
    }
}
