package project.dto.assemblers;

import project.dto.AddCategoryToGroupRequestDTO;
import project.model.shared.*;

public class AddCategoryToGroupRequestAssembler {
    protected AddCategoryToGroupRequestAssembler() {
        throw new AssertionError();
    }

    /**
     * This will mapTo a RequestDTO with information from web and transforms it within CLI
     *
     * @param designation      String parameter from JSON
     * @param groupDescription String parameter from JSON
     * @param responsibleEmail String parameter from JSON
     * @return AddCategoryEntryDTO(adesignation, gpID, pID)
     */
    public static AddCategoryToGroupRequestDTO addCategoryMapToDTO(String designation, String groupDescription, String responsibleEmail) {
        Designation adesignation = new Designation(designation);
        GroupID gpID = new GroupID(new Description(groupDescription));
        PersonID pID = new PersonID(new Email(responsibleEmail));

        return new AddCategoryToGroupRequestDTO(adesignation, gpID, pID);
    }
}