package project.dto.assemblers;

import project.dto.IsSiblingResponseDTO;

public class IsSiblingResponseAssembler {

    protected IsSiblingResponseAssembler() {
        throw new AssertionError();
    }

    public static IsSiblingResponseDTO mapToDTO(String isSibling) {
        return new IsSiblingResponseDTO(isSibling);
    }
}
