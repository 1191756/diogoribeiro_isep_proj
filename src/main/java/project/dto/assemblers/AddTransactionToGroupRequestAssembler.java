package project.dto.assemblers;

import project.dto.AddTransactionToGroupRequestDTO;
import project.model.shared.*;

public class AddTransactionToGroupRequestAssembler {

    public static AddTransactionToGroupRequestDTO mapToDTO(String personID, String groupID, Double amount, String description, String category, String accountIDDebit, String accountIDCredit, String type) {
        AddTransactionToGroupRequestAssembler addTransactionToGroupRequestAssembler = new AddTransactionToGroupRequestAssembler();
        PersonID memberID = new PersonID(new Email(personID));
        GroupID groupDescription = new GroupID(new Description(groupID));
        Description transDescription = new Description(description);
        Category transCategory = new Category(new Designation(category));
        AccountID transDebit = new AccountID(new Denomination(accountIDDebit), groupDescription);
        AccountID transCredit = new AccountID(new Denomination(accountIDCredit), groupDescription);
        TransactionType transType = TransactionType.valueOf(type);

        return new AddTransactionToGroupRequestDTO(memberID, groupDescription, amount, transDescription, transCategory, transDebit, transCredit, transType);
    }
}
