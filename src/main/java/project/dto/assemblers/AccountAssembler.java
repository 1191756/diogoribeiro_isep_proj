package project.dto.assemblers;

import project.dto.AccountDTO;
import project.model.account.Account;

public class AccountAssembler {

    protected AccountAssembler() {
        throw new AssertionError();
    }

    public static AccountDTO mapToDTO(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountID(account.getAccountID());
        accountDTO.setDescription(account.getDescription());
        return accountDTO;
    }
}
