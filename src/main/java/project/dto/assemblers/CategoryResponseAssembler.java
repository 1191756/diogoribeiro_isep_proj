package project.dto.assemblers;

import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;

public class CategoryResponseAssembler {
    public static CategoryResponseDTO mapToDTO(CategoryDTO categoryDTO) {
        return new CategoryResponseDTO(categoryDTO.getDesignation().toString());
    }
}
