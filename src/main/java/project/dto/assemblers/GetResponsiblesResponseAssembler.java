package project.dto.assemblers;

import project.dto.GetResponsiblesResponseDTO;
import project.dto.PersonDTO;

/**
 * Assembler for GetResponsiblesResponseDTO
 */
public class GetResponsiblesResponseAssembler {

    protected GetResponsiblesResponseAssembler() {
        throw new AssertionError();
    }

    public static GetResponsiblesResponseDTO mapToDTO(PersonDTO personDTO) {
        return new GetResponsiblesResponseDTO(personDTO.getPersonID().getEmail().toString());
    }
}
