package project.dto;

import project.model.shared.AccountID;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.model.shared.TransactionDate;

import java.util.Objects;

public class GetGroupAccountTransactionsInPeriodRequestDTO {
    private PersonID personID;
    private GroupID groupID;
    private AccountID accountID;
    private TransactionDate initialDate;
    private TransactionDate endDate;

    public GetGroupAccountTransactionsInPeriodRequestDTO(PersonID personID, GroupID groupID, AccountID accountID, TransactionDate initialDate, TransactionDate endDate) {
        this.personID = personID;
        this.groupID = groupID;
        this.accountID = accountID;
        this.initialDate = initialDate;
        this.endDate = endDate;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(AccountID accountID) {
        this.accountID = accountID;
    }

    public TransactionDate getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(TransactionDate initialDate) {
        this.initialDate = initialDate;
    }

    public TransactionDate getEndDate() {
        return endDate;
    }

    public void setEndDate(TransactionDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetGroupAccountTransactionsInPeriodRequestDTO)) return false;
        GetGroupAccountTransactionsInPeriodRequestDTO that = (GetGroupAccountTransactionsInPeriodRequestDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(accountID, that.accountID) &&
                Objects.equals(initialDate, that.initialDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, groupID, accountID, initialDate, endDate);
    }
}
