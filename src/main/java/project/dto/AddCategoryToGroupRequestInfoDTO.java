package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize
public class AddCategoryToGroupRequestInfoDTO {
    private String designation;
    private String groupDescription;
    private String responsibleEmail;

    public AddCategoryToGroupRequestInfoDTO(String designation, String groupDescription, String responsibleEmail) {
        this.designation = designation;
        this.groupDescription = groupDescription;
        this.responsibleEmail = responsibleEmail;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getPersonID() {
        return responsibleEmail;
    }

    public void setPersonID(String personID) {
        this.responsibleEmail = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddCategoryToGroupRequestInfoDTO that = (AddCategoryToGroupRequestInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, groupDescription, responsibleEmail);
    }
}