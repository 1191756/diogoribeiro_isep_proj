package project.dto;

import project.model.shared.Designation;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddCategoryToPersonRequestDTO {
    private Designation designation;
    private PersonID personID;

    public AddCategoryToPersonRequestDTO(Designation designation, PersonID personID) {
        this.designation = designation;
        this.personID = personID;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryToPersonRequestDTO)) return false;
        AddCategoryToPersonRequestDTO that = (AddCategoryToPersonRequestDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, personID);
    }
}
