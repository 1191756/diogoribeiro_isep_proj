package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize
public class AddCategoryToPersonRequestInfoDTO {
    private String designation;
    private String personID;

    public AddCategoryToPersonRequestInfoDTO(String designation, String personID) {
        this.designation = designation;
        this.personID = personID;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddCategoryToPersonRequestInfoDTO that = (AddCategoryToPersonRequestInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, personID);
    }
}