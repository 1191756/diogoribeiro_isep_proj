package project.dto;

import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.Objects;
import java.util.Set;

public class AddMembersRequestDTO {
    GroupID groupID;
    Set<PersonID> personID;

    public AddMembersRequestDTO(GroupID groupID, Set<PersonID> personID) {
        this.groupID = groupID;
        this.personID = personID;
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    public Set<PersonID> getPersonIDs() {
        return personID;
    }

    public void setPersonIDs(Set<PersonID> personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddMembersRequestDTO that = (AddMembersRequestDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID, personID);
    }
}
