package project.dto;

import java.util.List;
import java.util.Objects;

public class AddMembersRequestInfoDTO {
    List<String> members;

    public AddMembersRequestInfoDTO(List<String> personIDs) {
        this.members = personIDs;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddMembersRequestInfoDTO that = (AddMembersRequestInfoDTO) o;
        return Objects.equals(members, that.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(members);
    }
}
