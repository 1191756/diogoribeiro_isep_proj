package project.dto;

import org.springframework.hateoas.RepresentationModel;
import project.model.shared.TransactionDate;
import project.model.shared.TransactionType;

public class AddTransactionResponseDTO extends RepresentationModel<AddTransactionResponseDTO> {
    private Double amount;
    private String description;
    private TransactionDate date;
    private String category;
    private String debit;
    private String credit;
    private TransactionType type;

    public AddTransactionResponseDTO(Double amount, String description, TransactionDate date, String category, String debit, String credit, TransactionType type) {
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
        this.type = type;

    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TransactionDate getDate() {
        return date;
    }

    public void setDate(TransactionDate date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }
}
