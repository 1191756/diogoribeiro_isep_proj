package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AccountDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class GetGroupAccountsService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    GroupRepository groupRepository;

    public Set<AccountDTO> getGroupAccounts(String groupID) {

        if (!groupRepository.findById(new GroupID(new Description(groupID))).isPresent()) {
            throw new NotFoundException("GroupID");
        } else {
            Iterable<Account> accountList = accountRepository.findAllByAccountID_OwnerID(new GroupID(new Description(groupID)));
            Set<AccountDTO> accountDTOSet = new HashSet<>();
            for (Account account : accountList) {
                accountDTOSet.add(AccountAssembler.mapToDTO(account));
            }
            return accountDTOSet;
        }
    }
}
