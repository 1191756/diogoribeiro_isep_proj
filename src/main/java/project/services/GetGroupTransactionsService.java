package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.LedgerID;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetGroupTransactionsService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    public Set<TransactionDTO> getGroupTransactions(String groupID) {
        Set<TransactionDTO> getGroupTransactions = new HashSet<>();
        Optional<Group> groupOptional = groupRepository.findById(new GroupID(new Description(groupID)));
        Group group;
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }
        else {
            group = groupOptional.get();
        }
        Optional<Ledger> ledgerOptional = ledgerRepository.findById(new LedgerID(group.getID()));
        if (!ledgerOptional.isPresent()) {
            throw new NotFoundException("LedgerID");
        }
        Ledger ledger = ledgerOptional.get();
        Set<Transaction> transactions = ledger.getTransactions();
        for (Transaction transaction : transactions) {
            getGroupTransactions.add(TransactionAssembler.mapToDTO(transaction));
        }
        return getGroupTransactions;
    }
}