package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddCategoryToPersonRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.Category;
import project.repositories.PersonRepository;

import java.util.Optional;

@Service
public class AddCategoryToPersonService {

    @Autowired
    private final PersonRepository personRepository;

    public AddCategoryToPersonService(PersonRepository personRepo) {
        this.personRepository = personRepo;
    }

    /**
     * As a person, I want to create a category.
     *
     * @param addCategoryToPersonRequestDTO Data Transfer Object
     * @return categoryDTO
     */
    public CategoryDTO addCategoryToPerson(AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTO) {

        Optional<Person> personOptional = personRepository.findById(addCategoryToPersonRequestDTO.getPersonID());
        if (!personOptional.isPresent()) {
            throw new NotFoundException("PersonID");
        }

        Person targetPerson = personOptional.get();

        Category category = new Category(addCategoryToPersonRequestDTO.getDesignation());
        CategoryDTO categoryDTO = CategoryAssembler.mapToDTO(category);
        targetPerson.addCategory(category);
        personRepository.save(targetPerson);
        return categoryDTO;
    }
}

