package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Category;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CopyGroupAsMemberServiceAV {

    @Autowired
    GroupRepository groupRepository;

    public CopyGroupAsMemberServiceAV(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupDTO copyGroupAsMemberServiceAV(AddGroupRequestDTO addGroupRequestDTO) {
        GroupID groupID = new GroupID(addGroupRequestDTO.getGroupDescription());

        Optional<Group> groupOptional = groupRepository.findById(groupID);
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group targetGroup = groupOptional.get();

        if (!(targetGroup.isMemberID(addGroupRequestDTO.getPersonID()))) {
            throw new NotFoundException("Member");
        }
        Set<PersonID> members = new HashSet<>(targetGroup.getMembers());
        Set<Category> categories = new HashSet<>(targetGroup.getCategories());
        Description copyGroupDescription = new Description("BeerBuddies");

        Group groupCopy = new Group(copyGroupDescription, addGroupRequestDTO.getPersonID());

        groupCopy.addMembers(members);

        for (Category category : categories) {
            groupCopy.addCategory(category);
        }
        groupRepository.save(groupCopy);
        return GroupAssembler.mapToDTO(groupCopy);
    }
}