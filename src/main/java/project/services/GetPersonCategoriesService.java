package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.Category;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetPersonCategoriesService {
    @Autowired
    PersonRepository personRepository;

    public Set<CategoryDTO> getPeronCategories(String personID) {

        Set<CategoryDTO> categoryDTOSet = new HashSet<>();

        Optional<Person> personOptional = personRepository.findById(new PersonID(new Email(personID)));
        if (!personOptional.isPresent()) {
            throw new NotFoundException("PersonID");
        }
        Person person = personOptional.get();
        Set<Category> categories = person.getCategories();
        for (Category category : categories) {
            categoryDTOSet.add(CategoryAssembler.mapToDTO(category));
        }
        return categoryDTOSet;
    }
}