package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Category;
import project.repositories.GroupRepository;

import java.util.Optional;

@Service
public class AddCategoryToGroupService {

    @Autowired
    private final GroupRepository groupRepository;

    public AddCategoryToGroupService(GroupRepository groupRepo) {
        this.groupRepository = groupRepo;
    }

    /**
     * As responsible of the group, I want to create a category for group.
     *
     * @param addCategoryToGroupRequestDTO Data Transfer Object
     * @return categoryDTO
     */
    public CategoryDTO addCategoryToGroup(AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO) {

        Optional<Group> groupOptional = groupRepository.findById(addCategoryToGroupRequestDTO.getGroupDescription());
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group targetGroup = groupOptional.get();
        if (!targetGroup.isResponsible(addCategoryToGroupRequestDTO.getResponsibleEmail())) {
            throw new NotFoundException("Responsible");
        }

        Category category = new Category(addCategoryToGroupRequestDTO.getDesignation());
        CategoryDTO categoryDTO = CategoryAssembler.mapToDTO(category);
        targetGroup.addCategory(category);
        groupRepository.save(targetGroup);
        return categoryDTO;
    }
}