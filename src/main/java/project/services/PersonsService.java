package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.PersonDTO;
import project.dto.assemblers.PersonAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PersonsService {
    @Autowired
    PersonRepository personRepository;

    public Set<PersonDTO> getPersons() {

        Iterable<Person> allPersons = personRepository.findAll();

        Set<PersonDTO> personDTOSet = new HashSet<>();

        for (Person person : allPersons) {
            personDTOSet.add(PersonAssembler.mapToDTO(person));
        }

        return personDTOSet;
    }

    public PersonDTO getPersonById(String personID) {

        Optional<Person> personOptional = personRepository.findById(new PersonID(new Email(personID)));

        if (!personOptional.isPresent()) {
            throw new NotFoundException("PersonID not found");
        }

        Person person = personOptional.get();
        PersonDTO personDTO = PersonAssembler.mapToDTO(person);

        return personDTO;
    }
}
