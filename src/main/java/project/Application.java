package project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/groups").allowedOrigins("http://localhost:3000");
				registry.addMapping("/groups/{groupDescription}/members").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/groups").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/accounts").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/groups/accounts").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/categories").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/accounts/{accountDenomination}/transactions/{initialDate}/{endDate}").allowedOrigins("http://localhost:3000");
				registry.addMapping("/groups/{groupID}/transactions").allowedOrigins("http://localhost:3000");
				registry.addMapping("/groups/{groupID}/transaction").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/transactions").allowedOrigins("http://localhost:3000");
				registry.addMapping("/groups/{groupID}/accounts").allowedOrigins("http://localhost:3000");
				registry.addMapping("/groups/{groupID}/categories").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons/{personID}/groups/categories").allowedOrigins("http://localhost:3000");
				registry.addMapping("/persons").allowedOrigins("http://localhost:3000");
			}
		};
	}
}