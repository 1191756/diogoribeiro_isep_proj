package project.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.GroupData;
import project.datamodel.GroupIDData;
import project.datamodel.MemberIDData;
import project.datamodel.assemblers.GroupDomainDataAssembler;
import project.model.framework.Repository;
import project.model.group.Group;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.jpa.GroupJPARepository;

import java.util.*;

@Component
public class GroupRepository implements Repository {
    @Autowired
    GroupJPARepository groupJPARepository;

    /**
     * Saves an entity either by creating it or updating it in the persistence
     * store. The reference is no
     * longer valid and use the returned object instead. e.g.,
     *
     * @param entity
     * @return the object reference to the persisted object.
     */
    @Override
    public Group save(Object entity) {
        Group newGroup = (Group) entity;
        GroupData groupData = GroupDomainDataAssembler.toData(newGroup);
        GroupData savedGroup = groupJPARepository.save(groupData);
        return GroupDomainDataAssembler.toDomain(savedGroup);
    }

    public Group updateMembers(Object entity, PersonID newMember) {
        Group newGroup = (Group) entity;
        GroupData groupData = GroupDomainDataAssembler.toData(newGroup);
        Optional<GroupData> originalGroup = groupJPARepository.findById(groupData.getId());

        if (!originalGroup.isPresent()) {
            throw new IllegalArgumentException();
        }

        GroupData updatedGroup = originalGroup.get();
        updatedGroup.setMember(new MemberIDData(newMember.getEmail().toString()));
        groupJPARepository.save(updatedGroup);
        return GroupDomainDataAssembler.toDomain(updatedGroup);
    }

    public Group saveMembers(Object entity, Set<PersonID> newMembers) {
        Group newGroup = (Group) entity;
        GroupData groupData = GroupDomainDataAssembler.toData(newGroup);
        Optional<GroupData> originalGroup = groupJPARepository.findById(groupData.getId());

        if (!originalGroup.isPresent()) {
            throw new IllegalArgumentException();
        }

        Set<MemberIDData> membersDataSet = new HashSet<>();
        for (PersonID personID : newMembers) {
            membersDataSet.add(new MemberIDData(personID.getEmail().toString()));
        }

        GroupData updatedGroup = originalGroup.get();
        updatedGroup.setMembers(membersDataSet);
        groupJPARepository.save(updatedGroup);
        return GroupDomainDataAssembler.toDomain(updatedGroup);
    }

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    @Override
    public Iterable<Group> findAll() {
        Iterable<GroupData> allGroupData = groupJPARepository.findAll();
        List<Group> domainGroups = new ArrayList<>();
        for (GroupData groupData : allGroupData) {
            domainGroups.add(GroupDomainDataAssembler.toDomain(groupData));
        }
        return domainGroups;
    }

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id
     * @return the entity with the specse calharified primary key
     */
    @Override
    public Optional<Group> findById(Object id) {
        GroupID groupIdToSearch = (GroupID) id;
        Optional<Group> foundGroup = Optional.empty();
        GroupIDData groupIDData = new GroupIDData(groupIdToSearch.toString());
        Optional<GroupData> jpaGroup = groupJPARepository.findById(groupIDData);

        if (jpaGroup.isPresent()) {
            GroupData groupData = jpaGroup.get();
            foundGroup = Optional.of(GroupDomainDataAssembler.toDomain(groupData));
        }

        return foundGroup;
    }

    public void deleteAll() {
        groupJPARepository.deleteAll();
    }
}