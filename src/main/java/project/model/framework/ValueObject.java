package project.model.framework;

public interface ValueObject<T> {

    boolean sameValueAs(T other);
}
