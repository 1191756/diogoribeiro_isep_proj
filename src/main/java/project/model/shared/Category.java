package project.model.shared;

import project.model.framework.ValueObject;

import java.util.Objects;

public class Category implements ValueObject<Category> {
    private final Designation designation;

    /**
     * US005 Constructor
     * Create a Category
     *
     * @param designation of the category
     */
    public Category(Designation designation) {
        if (designation == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }
        this.designation = designation;
    }

    public Designation getDesignation() {
        return designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        return designation.equals(category.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

    @Override
    public String toString() {
        return designation.getDesignation();
    }

    @Override
    public boolean sameValueAs(Category other) {
        return other != null && this.designation.equals(other.designation);
    }
}