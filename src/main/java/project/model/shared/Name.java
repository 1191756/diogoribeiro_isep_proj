package project.model.shared;

import project.exceptions.EmptyException;

import java.util.Objects;

public class Name {
    private String name;

    public Name(String name) {
        setIsNameValid(name);
    }

    /**
     * Verifies if provided name is null or empty
     *
     * @param name
     * @return
     */
    public void setIsNameValid(String name) {
        if (name == null) {
            throw new NullPointerException();
        }
        if (name.trim().isEmpty()) {
            throw new EmptyException();
        }

        this.name = name.trim();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Name aName = (Name) o;
        return aName.getName().equals(this.name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
