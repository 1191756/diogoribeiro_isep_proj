package project.model.shared;

import project.exceptions.EmptyException;
import project.model.framework.ValueObject;

import java.util.Objects;

public class Designation implements ValueObject<Designation> {
    private String designation;

    public Designation(String designation) {
        setIsDesignationValid(designation);
    }

    /**
     * Verifies if provided designation is null or empty
     *
     * @param designation
     */
    private void setIsDesignationValid(String designation) {
        if (designation == null) {
            throw new NullPointerException("Designation cannot be null");
        }
        if (designation.trim().isEmpty()) {
            throw new EmptyException("Designation cannot be empty");
        } else {
            this.designation = designation.trim();
        }
    }

    public String getDesignation() {
        return designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Designation that = (Designation) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

    @Override
    public boolean sameValueAs(Designation other) {
        return other != null && this.designation.equals(other.designation);
    }

    @Override
    public String toString() {
        return designation;
    }
}
