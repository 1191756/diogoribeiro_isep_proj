package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AddTransactionResponseDTO;
import project.dto.AddTransactionToGroupRequestDTO;
import project.dto.AddTransactionToGroupRequestInfoDTO;
import project.dto.TransactionDTO;
import project.dto.assemblers.AddTransactionResponseDTOAssembler;
import project.dto.assemblers.AddTransactionToGroupRequestAssembler;
import project.services.AddTransactionToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping
public class AddTransactionToGroupRestController {
    @Autowired
    private AddTransactionToGroupService service;

    @PostMapping("/groups/{groupID}/transaction")
    public ResponseEntity<Object> addTransactionToGroup(@PathVariable String groupID, @RequestBody AddTransactionToGroupRequestInfoDTO info) {
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO = AddTransactionToGroupRequestAssembler.mapToDTO(
                info.getPersonID(),
                groupID,
                info.getAmount(),
                info.getDescription(),
                info.getCategory(),
                info.getDebit(),
                info.getCredit(),
                info.getType());

        TransactionDTO transactionDTO = service.addTransactionToGroup(addTransactionToGroupRequestDTO);

        AddTransactionResponseDTO transactionResponseDTO = AddTransactionResponseDTOAssembler.mapToDTO(transactionDTO);

        Link selfLink = linkTo(AddTransactionToGroupRestController.class).slash("groups").slash(groupID).slash("transaction").slash(transactionResponseDTO.getDescription()).withSelfRel();
        transactionResponseDTO.add(selfLink);

        return new ResponseEntity<>(transactionResponseDTO, HttpStatus.CREATED);
    }
}