package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.dto.AddGroupRequestDTO;
import project.dto.AddGroupRequestInfoDTO;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.dto.assemblers.AddGroupRequestAssembler;
import project.dto.assemblers.GroupResponseAssembler;
import project.model.shared.Category;
import project.model.shared.PersonID;
import project.services.CopyGroupAsMemberServiceAV;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class CopyGroupAsMemberRestControllerAV {
    @Autowired
    private CopyGroupAsMemberServiceAV service;

    @PostMapping("/persons")
    public ResponseEntity<Object> copyGroupAsMember(@RequestBody AddGroupRequestInfoDTO info) {
        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(info.getGroupDescription(), info.getResponsibleEmail());

        GroupDTO groupDTO = service.copyGroupAsMemberServiceAV(addGroupRequestDTO);
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();

        for (PersonID responsible : groupDTO.getResponsibles()) {
            responsibles.add(responsible.getEmail().toString());
        }
        for (PersonID member : groupDTO.getMembers()) {
            members.add(member.getEmail().toString());
        }
        for (Category category : groupDTO.getCategories()) {
            categories.add(category.toString());
        }
        GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(
                groupDTO.getGroupID().getDescription().getDescription(), responsibles, members, categories);

        Link selfLink = linkTo(CopyGroupAsMemberRestControllerAV.class).slash("groups").slash(groupResponseDTO.getGroupDescription()).withSelfRel();
        groupResponseDTO.add(selfLink);

        return new ResponseEntity<>(groupResponseDTO, HttpStatus.CREATED);
    }
}