package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMembersRequestDTO;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.dto.assemblers.AddMembersRequestAssembler;
import project.dto.assemblers.GroupResponseAssembler;
import project.model.shared.Category;
import project.model.shared.PersonID;
import project.services.AddMemberToGroupService;
import project.utils.GetAllEmailsInString;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping
public class AddMemberToGroupRestController {
    @Autowired
    private AddMemberToGroupService service;

    /**
     * As a system manager, I want to add people to the group.
     *
     * @param
     * @return
     */
    @PostMapping("/groups/{groupDescription}/members")
    public ResponseEntity<Object> addMembersToGroup(@PathVariable String groupDescription, @RequestBody String info) {
        List<String> emails = GetAllEmailsInString.getEmailAddressesInString(info);
        AddMembersRequestDTO addMembersRequestDTO = AddMembersRequestAssembler.mapToDTO(groupDescription, emails);

        GroupDTO groupDTO = service.addMembers(addMembersRequestDTO);

        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        for (PersonID responsible : groupDTO.getResponsibles()) {
            responsibles.add(responsible.getEmail().toString());
        }
        for (PersonID member : groupDTO.getMembers()) {
            members.add(member.getEmail().toString());
        }
        for (Category category : groupDTO.getCategories()) {
            categories.add(category.getDesignation().toString());
        }
        GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(
                groupDTO.getGroupID().getDescription().getDescription(), responsibles, members, categories);

        Link selfLink = linkTo(AddMemberToGroupRestController.class).slash("groups").slash(groupDescription).slash("members").withSelfRel();
        groupResponseDTO.add(selfLink);

        return new ResponseEntity<>(groupResponseDTO, HttpStatus.CREATED);
    }

    //@PostMapping("/groups/{groupDescription}/members/{personEmail}")
    public ResponseEntity<Object> addMemberToGroup(@PathVariable String groupDescription, @PathVariable String personEmail) {
        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);

        GroupDTO groupDTO = service.addMember(addMemberRequestDTO);

        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        for (PersonID responsible : groupDTO.getResponsibles()) {
            responsibles.add(responsible.getEmail().toString());
        }
        for (PersonID member : groupDTO.getMembers()) {
            members.add(member.getEmail().toString());
        }
        for (Category category : groupDTO.getCategories()) {
            categories.add(category.getDesignation().toString());
        }
        GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(
                groupDTO.getGroupID().getDescription().getDescription(), responsibles, members, categories);

        Link selfLink = linkTo(AddMemberToGroupRestController.class).slash("groups").slash(groupDescription).slash("members").withSelfRel();
        groupResponseDTO.add(selfLink);

        return new ResponseEntity<>(groupResponseDTO, HttpStatus.CREATED);
    }
}
