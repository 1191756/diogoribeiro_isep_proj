package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.dto.assemblers.GroupResponseAssembler;
import project.model.shared.Category;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.services.GroupsService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GroupsRestController {
    @Autowired
    private GroupsService service;

    @GetMapping("/groups/{groupDescription}/")
    @ResponseBody
    public ResponseEntity<Object> getGroup(@PathVariable String groupDescription) {

        GroupDTO groupDTO = service.getGroup(groupDescription);

        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        for (PersonID responsible : groupDTO.getResponsibles()) {
            responsibles.add(responsible.getEmail().toString());
        }
        for (PersonID member : groupDTO.getMembers()) {
            members.add(member.getEmail().toString());
        }
        for (Category category : groupDTO.getCategories()) {
            categories.add(category.toString());
        }
        GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(
                groupDTO.getGroupID().getDescription().getDescription(), responsibles, members, categories);

        Link selfLink = linkTo(GroupsRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).withSelfRel();
        groupResponseDTO.add(selfLink);

        Link getMembersLink = linkTo(GetMembersRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("members").withRel("Get Members");
        groupResponseDTO.add(getMembersLink);

        Link getGroupCategoriesLink = linkTo(GetGroupCategoriesRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("Categories").withRel("Get GroupCategories");
        groupResponseDTO.add(getGroupCategoriesLink);

        Link getGroupAccountsLink = linkTo(GetGroupAccountsRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("accounts").withRel("Get GroupAccounts");
        groupResponseDTO.add(getGroupAccountsLink);

        Link getResponsiblesLink = linkTo(GetResponsiblesRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("responsibles").withRel("Get Responsibles");
        groupResponseDTO.add(getResponsiblesLink);

        Link addMemberLink = linkTo(AddMemberToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("members").withRel("Add Member");
        groupResponseDTO.add(addMemberLink);

        Link addTransactionLink = linkTo(AddTransactionToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("transaction").withRel("Add Transaction");
        groupResponseDTO.add(addTransactionLink);

        Link addAccountLink = linkTo(AddAccountToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("accounts").withRel("Add Account");
        groupResponseDTO.add(addAccountLink);

        Link addCategoriesLink = linkTo(AddCategoryToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("categories").withRel("Add Categories");
        groupResponseDTO.add(addCategoriesLink);

        return new ResponseEntity<>(groupResponseDTO, HttpStatus.OK);
    }

    @GetMapping("/persons/{personID}/groups")
    @ResponseBody
    public ResponseEntity<Object> getGroups(@PathVariable String personID) {

        Set<GroupDTO> groupsDTOSet = service.getGroups(new PersonID(new Email(personID)));

        Set<GroupResponseDTO> groupResponseDTOSet = new HashSet<>();

        for (GroupDTO groupDTO : groupsDTOSet) {

            List<String> responsibles = new ArrayList<>();
            List<String> members = new ArrayList<>();
            List<String> categories = new ArrayList<>();
            for (PersonID responsible : groupDTO.getResponsibles()) {
                responsibles.add(responsible.getEmail().toString());
            }
            for (PersonID member : groupDTO.getMembers()) {
                members.add(member.getEmail().toString());
            }
            for (Category category : groupDTO.getCategories()) {
                categories.add(category.toString());
            }
            GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(
                    groupDTO.getGroupID().getDescription().getDescription(), responsibles, members, categories);

            Link selfLink = linkTo(GroupsRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).withSelfRel();
            groupResponseDTO.add(selfLink);

            Link getMembersLink = linkTo(GetMembersRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("members").withRel("Get Members");
            groupResponseDTO.add(getMembersLink);

            Link getGroupCategoriesLink = linkTo(GetGroupCategoriesRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("Categories").withRel("Get GroupCategories");
            groupResponseDTO.add(getGroupCategoriesLink);

            Link getGroupAccountsLink = linkTo(GetGroupAccountsRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("accounts").withRel("Get GroupAccounts");
            groupResponseDTO.add(getGroupAccountsLink);

            Link getResponsiblesLink = linkTo(GetResponsiblesRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("responsibles").withRel("Get Responsibles");
            groupResponseDTO.add(getResponsiblesLink);

            Link addMemberLink = linkTo(AddMemberToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID().getDescription()).slash("members").withRel("Add Member");
            groupResponseDTO.add(addMemberLink);

            Link addTransactionLink = linkTo(AddTransactionToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("transaction").withRel("Add Transaction");
            groupResponseDTO.add(addTransactionLink);

            Link addAccountLink = linkTo(AddAccountToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("accounts").withRel("Add Account");
            groupResponseDTO.add(addAccountLink);

            Link addCategoriesLink = linkTo(AddCategoryToGroupRestController.class).slash("groups").slash(groupDTO.getGroupID()).slash("categories").withRel("Add Categories");
            groupResponseDTO.add(addCategoriesLink);

            groupResponseDTOSet.add(groupResponseDTO);
        }
        return new ResponseEntity<>(groupResponseDTOSet, HttpStatus.OK);
    }
}