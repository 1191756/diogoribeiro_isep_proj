package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.TransactionDTO;
import project.dto.TransactionResponseDTO;
import project.dto.assemblers.TransactionResponseDTOAssembler;
import project.services.GetGroupTransactionsService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetGroupTransactionsRestController {
    @Autowired
    private GetGroupTransactionsService service;

    /**
     * As a group member, I want to obtain the movements of a given account in a given period.
     *
     * @param groupID
     * @return
     */
    @GetMapping("/groups/{groupID}/transactions")
    public ResponseEntity<Object> getPersonTransactionInPeriod(@PathVariable String groupID) {

        Set<TransactionDTO> transactionsInPeriodDTO = service.getGroupTransactions(groupID);

        Set<TransactionResponseDTO> transactionsResponseDTO = new HashSet<>();

        for (TransactionDTO iterator : transactionsInPeriodDTO) {

            TransactionResponseDTO transactionResponseDTO = TransactionResponseDTOAssembler.mapToDTO(iterator);

            Link selfLink = linkTo(GetGroupTransactionsRestController.class).slash("groups").slash(groupID).slash("transactions").withSelfRel();

            transactionResponseDTO.add(selfLink);
            transactionsResponseDTO.add(transactionResponseDTO);
        }
        return new ResponseEntity<>(transactionsResponseDTO, HttpStatus.OK);
    }

}
