package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetResponsiblesResponseDTO;
import project.dto.PersonDTO;
import project.dto.assemblers.GetResponsiblesResponseAssembler;
import project.services.GetResponsiblesService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetResponsiblesRestController {
    @Autowired
    private GetResponsiblesService service;

    @GetMapping("/groups/{groupDescription}/responsibles/")
    @ResponseBody
    public ResponseEntity<Object> getResponsibles(@PathVariable String groupDescription) {


        Set<PersonDTO> responsiblesDTO = service.getResponsibles(groupDescription);

        Set<GetResponsiblesResponseDTO> getResponsiblesResponseDTO = new HashSet<>();

        for (PersonDTO responsibleDTO : responsiblesDTO) {
            GetResponsiblesResponseDTO responsiblesResponseDTO = GetResponsiblesResponseAssembler.mapToDTO(responsibleDTO);
            Link selfLink = linkTo(AddGroupRestController.class).slash("responsibles").slash(responsibleDTO.getPersonID().getEmail()).withSelfRel();
            responsiblesResponseDTO.add(selfLink);
            getResponsiblesResponseDTO.add(responsiblesResponseDTO);
        }
        return new ResponseEntity<>(getResponsiblesResponseDTO, HttpStatus.OK);

    }
}
