package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;
import project.dto.assemblers.AddCategoryResponseAssembler;
import project.services.GetPersonCategoriesService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetPersonCategoriesRestController {
    @Autowired
    private GetPersonCategoriesService service;

    @GetMapping("/persons/{personID}/categories")
    @ResponseBody
    public ResponseEntity<Object> getCategoriesCategory(@PathVariable String personID) {

        Set<CategoryDTO> categoryDTOSet = service.getPeronCategories(personID);

        Set<CategoryResponseDTO> categoriesResponseDTO = new HashSet<>();

        for (CategoryDTO categoryDTO : categoryDTOSet) {
            CategoryResponseDTO categoryResponseDTO = AddCategoryResponseAssembler.mapToCategoryResponseDTO(categoryDTO);
            Link selfLink = linkTo(GetGroupCategoriesRestController.class).slash("persons").slash(personID).withSelfRel();
            categoryResponseDTO.add(selfLink);
            categoriesResponseDTO.add(categoryResponseDTO);
        }

        return new ResponseEntity<>(categoriesResponseDTO, HttpStatus.OK);
    }
}
