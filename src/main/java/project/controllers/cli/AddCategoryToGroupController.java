package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.AddCategoryToGroupRequestAssembler;
import project.services.AddCategoryToGroupService;

@Component
public class AddCategoryToGroupController {
    @Autowired
    private AddCategoryToGroupService service;

    /**
     * Finds group on the repository and adds new category if person is responsible.
     *
     * @param groupDescription
     * @param responsibleEmail
     * @param designation
     * @return service.addCategoryToGroup(addCategoryRequestDTO)
     */
    public CategoryDTO addCategoryToGroup(String groupDescription, String responsibleEmail, String designation) {
        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO;
        addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation, groupDescription, responsibleEmail);

        return service.addCategoryToGroup(addCategoryToGroupRequestDTO);
    }
}