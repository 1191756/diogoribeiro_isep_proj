package project.datamodel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(PersonCategoryIDData.class)
@Table(name = "persons_categories")
public class PersonCategoryData implements Serializable {
    private static final long serialVersionUID = 91989003634571L;
    @Id
    private String id;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonData person;

    public PersonCategoryData() {
    }

    public PersonCategoryData(String id, PersonData person) {
        this.id = id;
        this.person = person;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonCategoryData category = (PersonCategoryData) o;
        return id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id;
    }
}
