package project.datamodel;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import project.model.shared.PersonID;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "persons")
public class PersonData {
    @EmbeddedId
    private PersonID personId;
    private String name;
    private String birthPlace;
    private String birthDate;
    private String mother;
    private String father;
    @ElementCollection
    @CollectionTable(name = "persons_siblings", joinColumns = @JoinColumn(name = "person_id"))
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<SiblingIDData> siblings;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    private List<PersonCategoryData> categories;

    public PersonData() {
    }

    public PersonData(PersonID id, String name, String birthPlace, String birthDate) {
        this.personId = id;
        this.name = name;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.siblings = new ArrayList<>();
        this.categories = new ArrayList<>();
    }

    public PersonID getId() {
        return personId;
    }

    public void setId(PersonID id) {
        this.personId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public List<SiblingIDData> getSiblings() {
        return new ArrayList<>(siblings);
    }

    public void setSiblings(List<SiblingIDData> siblings) {
        this.siblings = new ArrayList<>(siblings);
    }

    public void setSibling(SiblingIDData sibling) {
        this.siblings.add(sibling);
    }

    public List<PersonCategoryData> getCategories() {
        return new ArrayList<>(categories);
    }

    public void setCategories(List<PersonCategoryData> categories) {
        this.categories = new ArrayList<>(categories);
    }

    public void setCategory(PersonCategoryData category) {
        this.categories.add(category);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonData that = (PersonData) o;
        return Objects.equals(personId, that.personId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personId);
    }

    @Override
    public String toString() {
        return "PersonData{" +
                "personId=" + personId +
                ", name='" + name + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", mother='" + mother + '\'' +
                ", father='" + father + '\'' +
                ", siblings=" + siblings +
                ", categories=" + categories +
                '}';
    }
}