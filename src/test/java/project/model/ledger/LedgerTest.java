package project.model.ledger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class LedgerTest {
    Description description;
    Category category;
    AccountID credit;
    AccountID debit;
    GroupID gpID;
    TransactionType transType;
    LedgerID ledgerID;
    Ledger ledger;

    @BeforeEach()
    void Ledger() {
        double amount = 67.9;
        description = new Description("Market");
        category = new Category(new Designation("GroceryStore"));
        Description groupdescription = new Description("Music Group");
        gpID = new GroupID(groupdescription);
        debit = new AccountID(new Denomination("Purse"), gpID);
        credit = new AccountID(new Denomination("money"), gpID);
        transType = TransactionType.valueOf("DEBIT");
        ledgerID = new LedgerID(gpID);
        ledger = new Ledger(ledgerID);
    }

    /**
     * US008 - Create transaction in Ledger
     * US008.1 - As a member, Create transaction in Ledger
     */
    @DisplayName("addTransaction() - Happy Path")
    @Test
    void addTransactionHappyPath() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, TransactionType.DEBIT);
        Transaction expected = transaction;
        //Act
        Transaction result = ledger.addTransaction(transaction);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("Equals override - Null Object")
    @Test
    void testEqualsObjectIsNull() {
        //Arrange
        Ledger nullLedger = null;
        //act
        boolean result = ledger.equals(nullLedger);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Object is of other type")
    @Test
    void testEqualsObjectIsOfOtherType() {
        //Arrange
        Category groceries = new Category(new Designation("Groceries"));
        //
        boolean result = ledger.equals(groceries);
        //Act/Assert
        assertFalse(result);

    }

    @DisplayName("Equals override")
    @Test
    void testEqualsSet() {
        //Arrange //Act
        boolean result = ledger.equals(ledger);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;

        Ledger ledger01 = new Ledger(ledgerID);
        Ledger ledger02 = new Ledger(ledgerID);

        int expectedResult = ledger02.hashCode();
        //Act
        int result = ledger01.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("Hash Code - Fixed value")
    @Test
    void hashCodeFixedTest() {
        //Arrange
        Ledger ledger01 = new Ledger(ledgerID);

        int expected = 1412808960;
        //Act
        int result = ledger01.hashCode();
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("get LedgerID- Happy Path ")
    @Test
    void getAccountIDCredit() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        LedgerID expected = new LedgerID(gpID);
        //Act
        LedgerID result = ledger.getLedgerID();
        //Assert
        assertEquals(expected, result);
    }
}



