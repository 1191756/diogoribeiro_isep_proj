package project.model.group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {
    Email email;
    Description description;
    PersonID responsible;

    Name p1Name;
    Address p1BirthPlace;
    Date p1BirthDate;
    Email p1Email;
    Person p1;
    PersonID p1ID;

    Name p2Name;
    Address p2BirthPlace;
    Date p2BirthDate;
    Email p2Email;
    Person p2;
    PersonID p2ID;

    Description groupDescription;
    Group g1;
    GroupID g1ID;

    Description group2Description;
    Group g2;
    GroupID g2ID;

    @BeforeEach
    void addGroupBefore() {
        p1Name = new Name("Pedro");
        p1BirthPlace = new Address("Porto");
        p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        p1Email = new Email("pedro.1234@gmail.com");
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2Name = new Name("Maria");
        p2BirthPlace = new Address("Maia");
        p2BirthDate = new Date(LocalDateTime.of(1988, 10, 2, 0, 0));
        p2Email = new Email("maria.1234@gmail.com");
        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();


        groupDescription = new Description("Family");
        g1 = new Group(groupDescription, p1ID);
        g1ID = g1.getID();

        group2Description = new Description("Friends");
        g2 = new Group(group2Description, p2ID);
        g2ID = g2.getID();
    }

    @DisplayName("As a user, I want to create a group, becoming a group administrator -Happy path ")
    @Test
    void addGroupHappyPath() {
        //Arrange
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        description = new Description("Happy Group");

        //Act//Assert
        Group group = new Group(description, responsible);
    }

    @DisplayName("As a user, I want to create a group, becoming a group administrator -null PersonID ")
    @Test
    void addGroupNullPersonID() {
        //Arrange
        description = new Description("Happy Group");

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Group group = new Group(description, null);
        });
    }

    @DisplayName("As a user, I want to create a group, becoming a group administrator -null description ")
    @Test
    void addGroupNullDescription() {
        //Arrange
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Group group = new Group(null, responsible);
        });
    }

    @DisplayName("As a user, I want to create a group, becoming a group administrator - empty description  ")
    @Test
    void addGroupEmptyDescription() {
        //Arrange
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);

        //Act//Assert
        assertThrows(EmptyException.class, () -> {
            Group group = new Group(new Description(""), responsible);
        });
    }

    @DisplayName("Override - sameIdentityAs")
    @Test
    void sameIdentityAs() {
        //Arrange
        description = new Description("Happy Group");
        email = new Email("joana@gmail.com");
        responsible = new PersonID(email);

        Group group1 = new Group(description, responsible);
        Group group2 = new Group(description, responsible);

        //Act
        boolean result = group1.sameIdentityAs(group2);

        //Assert
        assertTrue(result);

    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrage
        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);

        Group group = new Group(description, responsible);
        Group groupSameAttributes = new Group(description, responsible);
        int expected = groupSameAttributes.hashCode();

        //Act
        int result = group.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("Equals override - Same Object")
    @Test
    void testEqualsSameObject() {
        //Arrange
        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        Group group = new Group(description, responsible);
        //Act
        boolean result = group.equals(group);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the Same attributes")
    @Test
    void testEqualsSameAttributesObject() {
        //Assert
        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        Description description1 = new Description("HappyGroup");
        Email email1 = new Email("maria@gmail.com");
        PersonID responsible1 = new PersonID(email1);

        Group group = new Group(description, responsible);
        Group sameAttributesGroup = new Group(description1, responsible1);
        //Act
        boolean result = group.equals(sameAttributesGroup);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDifferentObjectType() {
        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        Group group = new Group(description, responsible);

        Designation designation = new Designation("supermercado");
        Category category = new Category(designation);

        boolean result = group.equals(category);

        assertFalse(result);
    }

    @DisplayName("Equals override - Object Null")
    @Test
    void testEqualsObjectNull() {

        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        Group group = new Group(description, responsible);
        Group group1 = null;

        boolean result = group.equals(group1);

        assertFalse(result);
    }

    @DisplayName("Equals override - Different Objects with the Different attributes")
    @Test
    void testEqualsDifferentAttributes() {
        description = new Description("HappyGroup");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        Description description1 = new Description("Family");
        Email email1 = new Email("family@gmail.com");
        PersonID responsible1 = new PersonID(email1);
        //Assert
        Group group = new Group(description, responsible);
        Group sameAttributesGroup = new Group(description1, responsible1);
        //Act
        boolean result = group.equals(sameAttributesGroup);
        //Assert
        assertFalse(result);
    }

    @DisplayName("getMembers() - test if the members set is returned")
    @Test
    void getMembers_HappyPath() {
        //Arrange

        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        g1 = new Group(groupDescription, p1ID);
        g1.addMember(p1ID);
        g1.addMember(p2ID);

        Set<PersonID> expected = new HashSet<>();
        expected.add(p1ID);
        expected.add(p2ID);

        //Act
        Set<PersonID> result = g1.getMembers();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("addMember() - Adds given personID to members of the group")
    @Test
    void addMember_HappyPath() {
        //Arrange
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        g1 = new Group(groupDescription, p1ID);

        //Act
        g1.addMember(p2ID);
        boolean result = g1.getMembers().contains(p2ID);

        //Assert
        assertTrue(result);
    }

    @DisplayName("addMember() - Test if an exception is thrown when given personID is null")
    @Test
    void addMember_NullPersonID() {
        //Arrange
        g1 = new Group(groupDescription, p1ID);

        //Act/Assert
        Exception exception = assertThrows(NullPointerException.class, () -> {
            g1.addMember(null);
        });

        String expectedMessage = "personID cannot be null";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("isMember_Happy() - Happy Path")
    @Test
    void isMember_Happy() {

        //Arrange
        g1 = new Group(groupDescription, p1ID);
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        //Act
        g1.addMember(p1ID);
        boolean result = g1.isMemberID(p1ID);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isMember_EmptyGroup() - Just one person added")
    @Test
    void isMember_EmptyGroup() {

        //Arrange
        g1 = new Group(groupDescription, p1ID);
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();
        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        //Act
        g1.addMember(p1ID);
        boolean result = g1.isMemberID(p2ID);

        //Assert
        assertFalse(result);
    }

    @DisplayName("addResponsible() - Add a person to responsibles list")
    @Test
    void addResponsible_Person() {
        //Arrange
        g1 = new Group(groupDescription, p1ID);
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        //Act
        PersonID result = g1.addResponsible(p1ID);

        assertEquals(p1.getPersonID(), result);
    }

    @DisplayName("addResponsible() - Null personID")
    @Test
    void addResponsible_NullPersonID() {
        //Arrange
        PersonID nullPersonID = null;

        //Act
        assertThrows(NullPointerException.class, () -> {
            PersonID result = g1.addResponsible(nullPersonID);
        });
    }

    @DisplayName("toString() - Happy Path")
    @Test
    void toStringHappyPath() {
        Date date = g1.getCreationDate();
        String expected = "Group{groupID=Family, responsibles=pedro.1234@gmail.com, members=pedro.1234@gmail.com,, categories=[], creationDate=" + date.toString() + "}";

        String result = g1.toString();

        assertEquals(expected, result);
    }

    @DisplayName("addMembers() - Adds given members to the group")
    @Test
    void addMembers_HappyPath() {
        //Arrange
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        g1 = new Group(groupDescription, p1ID);

        Set<PersonID> members = new HashSet<>();

        members.add(p2ID);

        //Act
        g1.addMembers(members);
        boolean result = g1.getMembers().contains(p1ID);
        boolean result2 = g1.getMembers().contains(p2ID);

        //Assert
        assertTrue(result);
        assertTrue(result2);
    }

    @DisplayName("addMembers() - Adds given members to the group")
    @Test
    void addMembers_CheckMembers() {
        //Arrange
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        g1 = new Group(groupDescription, p1ID);

        Set<PersonID> members = new HashSet<>();
        members.add(p2ID);

        Set<PersonID> expectedMembers = new HashSet<>();
        expectedMembers.add(p1ID);
        expectedMembers.add(p2ID);

        //Act
        Set<PersonID> result = g1.addMembers(members);

        //Assert
        assertTrue(result.containsAll(expectedMembers));
    }
}