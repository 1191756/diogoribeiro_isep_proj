package project.model.person;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    Person father;
    Person mother;
    Person son;
    Person sibling;
    Person notSibling;
    Person anotherFather;
    Person anotherMother;
    Person sameMotherSon;
    Person sameMotherSibling;
    Person sameFatherSon;
    Person sameFatherSibling;
    Person listSon;
    Person listSibling;
    Person otherListSibling;

    @BeforeEach
    void setUp() {
        // Family with two sons
        Address birthAddress = new Address("Porto, Portugal");

        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        // No family ties person
        Name mafalda = new Name("Mafalda");
        Email mafaldaEmail = new Email("mafalda@family.com");
        notSibling = new Person(mafalda, birthAddress, personsBirthdate, mafaldaEmail, null, null);

        //Extra Mother and Father
        Name ana = new Name("Ana");
        Email anaEmail = new Email("ana@family.com");
        anotherMother = new Person(ana, birthAddress, parentsBirthdate, anaEmail, null, null);
        Name luis = new Name("Luis");
        Email luisEmail = new Email("luis@family.com");
        anotherFather = new Person(luis, birthAddress, parentsBirthdate, luisEmail, null, null);

        //Same mother siblings
        Name diogo = new Name("Diogo");
        Email diogoEmail = new Email("diogo@family.com");
        sameMotherSon = new Person(diogo, birthAddress, personsBirthdate, diogoEmail, anotherMother.getPersonID(), anotherFather.getPersonID());
        Name alexandre = new Name("Alexandre");
        Email alexandreEmail = new Email("alexandre@family.com");
        sameMotherSibling = new Person(alexandre, birthAddress, personsBirthdate, alexandreEmail, anotherMother.getPersonID(), father.getPersonID());

        //Same father siblings
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@family.com");
        sameFatherSon = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, anotherMother.getPersonID(), anotherFather.getPersonID());
        Name rui = new Name("Rui");
        Email ruiEmail = new Email("rui@family.com");
        sameFatherSibling = new Person(rui, birthAddress, personsBirthdate, ruiEmail, mother.getPersonID(), anotherFather.getPersonID());

        //Persons with the sibling in the siblings list
        Name antonio = new Name("António");
        Email antonioEmail = new Email("antonio@family.com");
        listSon = new Person(antonio, birthAddress, personsBirthdate, antonioEmail, null, null);
        Name frederico = new Name("Frederico");
        Email fredericoEmail = new Email("frederico@family.com");
        listSibling = new Person(frederico, birthAddress, personsBirthdate, fredericoEmail, null, null);

        Name rogerio = new Name("Rogerio");
        Email rogerioEmail = new Email("rogerio@family.com");
        otherListSibling = new Person(frederico, birthAddress, personsBirthdate, fredericoEmail, null, null);

        listSon.addSibling(listSibling.getPersonID());
        listSibling.addSibling(listSon.getPersonID());

        otherListSibling.addSibling(listSon.getPersonID());
    }

    /**
     * User Story 1 (US1):
     * Como gestor de sistema, quero saber se determinada pessoa é irmão/irmã de outra. É irmão/irmã de outra se tem
     * a mesma mãe ou o mesmo pai, ou se na lista de irmãos está incluída a outra. A relação de irmão é bidirecional,
     * i.e. se A é irmão de B, então B é irmão de A.
     */
    @DisplayName("isSibling() - Verifies if other Person has same parents, or if it is located inside sibling's Set")
    @Test
    void isSiblingTrueSameMotherAndFather() {
        //Act
        boolean result = son.isSibling(sibling);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same mother")
    @Test
    void isSiblingTrueSameMother() {
        //Act
        boolean result = sameMotherSon.isSibling(sameMotherSibling);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same father")
    @Test
    void isSiblingTrueSameFather() {
        //Act
        boolean result = sameFatherSon.isSibling(sameFatherSibling);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person is inside sibling's list")
    @Test
    void isSiblingTrueSiblings() {
        //Act
        boolean result = listSon.isSibling(listSibling);

        //Assert
        assertTrue(result);

        //Act
        result = listSibling.isSibling(listSon);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if Person is inside other Person sibling's list")
    @Test
    void isSiblingTrueInOtherPersonSiblingsList() {
        //Act
        boolean result = listSon.isSibling(otherListSibling);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - False if other Person is null ")
    @Test
    void isSiblingTrueSiblingsNull() {
        //Arrange
        Person nullPerson = null;

        //Act
        assertThrows(NullPointerException.class, () -> {
            son.isSibling(nullPerson);
        });
    }

    @DisplayName("isSibling() - False if other Person is not sibling")
    @Test
    void isSiblingFalse() {
        //Act
        boolean result = son.isSibling(notSibling);

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - Main person mother is null")
    @Test
    void isSiblingMainPersonMotherIsNull() {
        //Act
        boolean result = mother.isSibling(son);

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - Other person mother is null")
    @Test
    void isSiblingOtherPersonMotherIsNull() {
        //Act
        boolean result = son.isSibling(father);

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - Main person father is null")
    @Test
    void isSiblingMainPersonFatherIsNull() {
        //Act
        boolean result = mother.isSibling(son);

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - Other person father is null")
    @Test
    void isSiblingOtherPersonFatherIsNull() {
        //Act
        boolean result = son.isSibling(father);

        //Assert
        assertFalse(result);
    }

    /**
     * Métodos de teste para o método addSibling()
     */
    @DisplayName("addSibling() - Test if added Person is inside sibling's Set ")
    @Test
    void addSiblingTrue() {
        //Arrange//Act
        Set<PersonID> result = listSon.getSiblings();

        //Assert
        assertTrue(result.contains(listSibling.getPersonID()));
    }

    @DisplayName("addSibling() - Test if given Person is null, it is not added to Set ")
    @Test
    void addSiblingNull() {
        //Arrange
        PersonID nullPersonId = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            listSon.addSibling(nullPersonId);
        });
    }

    @DisplayName("getFather() - Happy Path")
    @Test
    void getFatherHappyPath() {
        //Arrange
        PersonID expected = new PersonID(new Email("jose@family.com"));

        //Act
        PersonID result = son.getFather();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("getMother() - Happy Path")
    @Test
    void getMotherHappyPath() {
        //Arrange
        PersonID expected = new PersonID(new Email("maria@family.com"));

        //Act
        PersonID result = son.getMother();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Check same object")
    @Test
    void equalsSameObject() {
        //Act
        boolean result = father.equals(father);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Check same attributes object")
    @Test
    void equalsSameAttributesObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Act
        boolean result = father.equals(testPerson);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() -  Different person id in object")
    @Test
    void equalsDifferentPersonIdObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose2@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Act
        boolean result = father.equals(testPerson);

        // Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Object is null")
    @Test
    void equalsNullObject() {
        //Arrange
        Person nullPerson = null;

        //Act
        boolean result = father.equals(nullPerson);

        // Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = father.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - HappyPath")
    @Test
    void hashcodeHappyPath() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        int expected = -1163019811;

        //Act
        int result = testPerson.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    @DisplayName("sameIdentityAs() - HappyPath")
    @Test
    void sameIdentityAsHappyPath() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Act
        boolean result = father.sameIdentityAs(testPerson);

        // Assert
        assertTrue(result);
    }

    @DisplayName("toString() - HappyPath")
    @Test
    void toStringHappyPath() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        String expected = "Person{personID=jose@family.com, name=José, birthPlace=Porto, Portugal, birthDate=1956-02-23, mother=null, father=null, siblings=[], categories=[]}";

        //Act
        String result = testPerson.toString();

        // Assert
        assertEquals(expected, result);
    }

    @DisplayName("sameValueAs - Different Emails")
    @Test
    void sameValueAsDifferentObjects() {
        //Arrange
        Email emailOne = new Email("1234@email.com");
        PersonID p1ID = new PersonID(emailOne);
        Email emailTwo = new Email("4321@email.com");
        PersonID p2ID = new PersonID(emailTwo);

        //Act
        boolean result = p1ID.sameValueAs(p2ID);

        //Assert
        assertFalse(result);
    }

    @DisplayName("getCategory() - Category null ")
    @Test
    void addCategoryNull() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person testPerson = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Category category = null;

        //Act
        assertThrows(NullPointerException.class, () -> {
            testPerson.addCategory(category);
        });
    }

    @DisplayName("getName - Happy Path")
    @Test
    void getNameHappyPath() {
        //Act
        String result = mother.getName().toString();

        //Assert
        assertEquals("Maria", result);
    }

    @DisplayName("getBirthPlace - Happy Path")
    @Test
    void getBirthPlaceHappyPath() {
        //Act
        String result = mother.getBirthPlace().toString();

        //Assert
        assertEquals("Porto, Portugal", result);
    }

    @DisplayName("getBirthDate - Happy Path")
    @Test
    void getBirthDateHappyPath() {
        //Act
        String result = mother.getBirthDate().toString();

        //Assert
        assertEquals("1956-02-23", result);
    }

    @DisplayName("getCategories - Happy Path")
    @Test
    void getCategoriesHappyPath() {
        //Act
        Set<Category> result = mother.getCategories();

        //Assert
        assertEquals(0, result.size());
    }
}