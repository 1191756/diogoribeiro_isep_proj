package project.model.account;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {
    Denomination denomination;
    Description description;
    Email email;
    OwnerID ownerID;


    @BeforeEach()
    void Account() {
        denomination = new Denomination("Main Bank");
        description = new Description("Paycheck account");
        email = new Email("joana@gmail.com");
        ownerID = new PersonID(email);
    }


    @DisplayName("Equals override - Same Object")
    @Test
    void testEqualsSameObject() {
        Account account = new Account(denomination, description, ownerID);

        boolean result = account.equals(account);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the same attributes")
    @Test
    void testEqualsSamesameAttriburesObject() {
        Denomination denomination2 = new Denomination("Main Bank");
        Description description2 = new Description("Paycheck account");
        Account account = new Account(denomination, description, ownerID);
        Account sameAttriburesAccount = new Account(denomination2, description2, ownerID);

        boolean result = account.equals(sameAttriburesAccount);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the different attributes")
    @Test
    void testEqualsDifferentAttribes() {
        Denomination denomination2 = new Denomination("Main Bank");
        Description description2 = new Description("Paycheck account");
        PersonID ownerID2 = new PersonID(new Email("marco@gmail.com"));
        Account account = new Account(denomination, description, ownerID);
        Account sameAttriburesAccount = new Account(denomination2, description2, ownerID2);

        boolean result = account.equals(sameAttriburesAccount);

        assertFalse(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDifferentObjectType() {
        Account account = new Account(denomination, description, ownerID);
        Designation designation = new Designation("supermercado");
        Category category = new Category(designation);

        boolean result = account.equals(category);

        assertFalse(result);
    }

    @DisplayName("Equals override - Null Object")
    @Test
    void testEqualsNullObject() {
        Account account = new Account(denomination, description, ownerID);
        Account nullAccount = null;

        boolean result = account.equals(nullAccount);

        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        Account account = new Account(denomination, description, ownerID);
        Account accountSameAttributes = new Account(denomination, description, ownerID);

        int expectedResult = accountSameAttributes.hashCode();

        int result = account.hashCode();

        assertEquals(expectedResult, result);
    }

    @DisplayName("Hash Code Fixed value")
    @Test
    void hashCodeFixedTest() {
        Account account = new Account(denomination, description, ownerID);

        int expected = 1409993490;

        int result = account.hashCode();

        assertEquals(expected, result);
    }

    @DisplayName("Equals override - diferent description")
    @Test
    void testEqualsAttriburesDiferentDescription() {
        //Arrrange
        Description description2 = new Description("2");
        Account account1 = new Account(denomination, description, ownerID);
        Account account2 = new Account(denomination, description2, ownerID);
        //Act
        boolean result = account1.equals(account2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Get IDAccount happy path")
    @Test
    void getAccountIDHappyPath() {
        //Arrange
        Account resultAccount = new Account(denomination, description, ownerID);
        Account expectedAccount = new Account(denomination, description, ownerID);
        AccountID expected = expectedAccount.getAccountID();

        //Act
        AccountID result = resultAccount.getAccountID();
        //Asset
        assertEquals(expected, result);

    }

    @DisplayName("Same Identity As - Happy Path")
    @Test
    void sameIdentityAs() {
        //Arrange
        Account account1 = new Account(denomination, description, ownerID);
        Account account2 = new Account(denomination, description, ownerID);

        //Act
        boolean result = account1.sameIdentityAs(account2);

        //Asset
        assertTrue(result);

    }

    @DisplayName("Same Identity As - Different Parameters")
    @Test
    void sameIdentityAsDifferentParameters() {
        //Arrange
        PersonID ownerID2 = new PersonID(new Email("marco@gmail.com"));
        Denomination denomination2 = new Denomination("Main Bank");
        Description description2 = new Description("Paycheck account");
        Account account1 = new Account(denomination, description, ownerID);
        Account account2 = new Account(denomination2, description2, ownerID2);

        //Act
        boolean result = account1.sameIdentityAs(account2);

        //Asset
        assertFalse(result);

    }

    @DisplayName("Same Identity As - Null")
    @Test
    void sameIdentityNull() {
        //Arrange
        Account account1 = new Account(denomination, description, ownerID);
        Account account2 = null;

        //Act
        boolean result = account1.sameIdentityAs(account2);

        //Asset
        assertFalse(result);

    }

    @DisplayName("toString")
    @Test
    void toStringHappyPath() {
        Account account1 = new Account(denomination, description, ownerID);

        String expected = "Account{description=Paycheck account, accountID=Main Bank}";
        String result = account1.toString();

        assertEquals(expected, result);
    }
}

