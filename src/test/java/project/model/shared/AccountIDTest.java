package project.model.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountIDTest {
    Denomination denomination;
    AccountID accountId;
    Email email;
    PersonID personId;

    @BeforeEach
    void setUp() {
        denomination = new Denomination("Denomination");
        email = new Email("joana@gmail.com");
        personId = new PersonID(email);
        accountId = new AccountID(denomination, personId);
    }

    @Test
    void setDenomination() {
        accountId.setDenomination(accountId.getDenomination());
    }

    @Test
    void setOwner() {
        accountId.setOwnerID(accountId.getOwnerID());
    }

    @DisplayName("create accountID - Happy Path")
    @Test
    void setAccountIDHappyPath() {
        //Assert
        AccountID expectedResult = new AccountID(denomination, personId);

        // Act
        AccountID result = accountId;

        // Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("setAccountID - Null Denomination")
    @Test
    void setAccountIDNullDenomination() {
        assertThrows(NullPointerException.class, () -> {
            new AccountID(null, personId);
        });
    }

    @DisplayName("setAccountID - Null OwnerID")
    @Test
    void setAccountIDNulOwnerID() {
        assertThrows(NullPointerException.class, () -> {
            new AccountID(denomination, null);
        });
    }

    @DisplayName("Equals override - Same Object")
    @Test
    void testEqualsSameObject() {
        AccountID accountID = new AccountID(denomination, personId);

        boolean result = accountID.equals(accountID);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the same attributes")
    @Test
    void testEqualsSameAttributesObject() {

        AccountID accountID = new AccountID(denomination, personId);
        AccountID sameAttributesAccount = new AccountID(denomination, personId);

        boolean result = accountID.equals(sameAttributesAccount);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDifferentObjectType() {
        AccountID accountID = new AccountID(denomination, personId);
        Designation designation = new Designation("supermercado");
        Category category = new Category(designation);

        boolean result = accountID.equals(category);

        assertFalse(result);
    }

    @DisplayName("Equals override - Null Object")
    @Test
    void testEqualsNullObject() {
        AccountID accountID = new AccountID(denomination, personId);
        AccountID nullAccount = null;

        boolean result = accountID.equals(nullAccount);

        assertFalse(result);
    }

    @DisplayName("Equals override - Different Parameters")
    @Test
    void testEqualsDifferentParameters() {
        PersonID personIdDifferent = new PersonID(new Email("marco@gmail.com"));
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountID1 = new AccountID(denomination, personIdDifferent);

        boolean result = accountID.equals(accountID1);

        assertFalse(result);
    }

    @DisplayName("Equals override - Different denominations")
    @Test
    void testEqualsDifferentDenominations() {
        Denomination denomination1 = new Denomination("Debit");
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountID1 = new AccountID(denomination1, personId);

        boolean result = accountID.equals(accountID1);

        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination, personId);

        int expectedResult = accountIDSameAttributes.hashCode();

        int result = accountID.hashCode();

        assertEquals(expectedResult, result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTestDifferent() {
        Denomination denomination1 = new Denomination("Debit");
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination1, personId);

        int expectedResult = accountIDSameAttributes.hashCode();

        int result = accountID.hashCode();

        assertNotEquals(expectedResult, result);
    }

    @DisplayName("SameValueAs Override Test - Happy Path")
    @Test
    void sameValueAsTestHappyPath() {
        AccountID accountID = new AccountID(denomination, personId);

        boolean result = accountID.sameValueAs(accountID);

        assertTrue(result);
    }

    @DisplayName("SameValueAs Override Test")
    @Test
    void sameValueAsTest() {
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination, personId);

        boolean result = accountID.sameValueAs(accountIDSameAttributes);

        assertTrue(result);
    }

    @DisplayName("SameValueAs Override Test - False")
    @Test
    void sameValueAsTestFalse() {
        Denomination denomination2 = new Denomination("Credit");
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination2, personId);

        boolean result = accountID.sameValueAs(accountIDSameAttributes);

        assertFalse(result);
    }

    @DisplayName("SameValueAs Override Test - DifferentPersonID ")
    @Test
    void sameValueAsTestFalseDifferentPersonID() {
        PersonID personIdDifferent = new PersonID(new Email("marco@gmail.com"));
        Denomination denomination2 = new Denomination("Credit");
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination2, personIdDifferent);

        boolean result = accountID.sameValueAs(accountIDSameAttributes);

        assertFalse(result);
    }

    @DisplayName("SameValueAs Override Test -Null ")
    @Test
    void sameValueAsTestNull() {

        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = null;

        boolean result = accountID.sameValueAs(accountIDSameAttributes);

        assertFalse(result);
    }

    @DisplayName("SameValueAs Override Test - Different PersonID")
    @Test
    void sameValueAsTestDifferentPersonID() {
        PersonID personIdDifferent = new PersonID(new Email("marco@gmail.com"));
        AccountID accountID = new AccountID(denomination, personId);
        AccountID accountIDSameAttributes = new AccountID(denomination, personIdDifferent);

        boolean result = accountID.sameValueAs(accountIDSameAttributes);

        assertFalse(result);
    }

    @DisplayName("toString() - HappyPath")
    @Test
    void toStringHappyPath() {

        AccountID accountID = new AccountID(denomination, personId);
        String expected = "Denomination";
        String result = accountID.toString();

        assertEquals(expected, result);
    }

}