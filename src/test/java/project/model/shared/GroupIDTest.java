package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupIDTest {
    @DisplayName("create groupID - Happy Path")
    @Test
    void setgroupIDHappyPath() {
        //Arrange
        GroupID noArgsGroups = new GroupID();
        Description description = new Description("Family");

        //Act//Assert
        new GroupID(description);
    }

    @DisplayName("setGroupID - NullDescription")
    @Test
    void setGroupIDNullDescription() {
        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new GroupID(null);
        });
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        //Arrange
        Description description = new Description("Family");
        GroupID testGroupId = new GroupID(description);

        //Act
        boolean result = testGroupId.equals(testGroupId);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Null Object")
    @Test
    void equalsNullObject() {
        //Arrange
        Description description = new Description("Family");
        GroupID testGroupId = new GroupID(description);
        GroupID nullGroup = null;

        //Act
        boolean result = testGroupId.equals(nullGroup);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different Class")
    @Test
    void equalsDifferentClass() {
        //Arrange
        Description description = new Description("Family");
        GroupID testGroupId = new GroupID(description);
        Name testName = new Name("Nuno");

        //Act
        boolean result = testGroupId.equals(testName);

        //Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Same descripition of two different objects")
    @Test
    void sameValueAsHappyPath() {
        //Arrange
        Description description = new Description("Family");
        Description descriptionAlternate = new Description("Family");

        GroupID testGroupId = new GroupID(description);
        GroupID testAlternateGroupId = new GroupID(descriptionAlternate);

        //Act
        boolean result = testGroupId.sameValueAs(testAlternateGroupId);

        //Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Null object to compare")
    @Test
    void sameValueAsNullCompare() {
        //Arrange
        Description description = new Description("Family");

        GroupID testGroupId = new GroupID(description);
        GroupID testAlternateGroupId = null;

        //Act
        boolean result = testGroupId.sameValueAs(testAlternateGroupId);

        //Assert
        assertFalse(result);
    }
}