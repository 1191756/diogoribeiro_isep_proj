package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TransactionDateTest {
    @DisplayName("create TransactionDate - Happy Path")
    @Test
    void setIsTransactionValidHappyPath() {
        //Arrange//Act//Assert
        LocalDateTime.of(1999, 03, 10, 22, 10, 30);
    }

    @DisplayName("create TransactionDate - convert from string")
    @Test
    void setIsDateValidSameValueAsWithString() {
        //Arrange
        String date = LocalDateTime.of(1999, 03, 10, 22, 10, 30).toString();
        TransactionDate transactionDateOne = new TransactionDate(date);
        TransactionDate transactionDateTwo = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.sameValueAs(transactionDateTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create TransactionDate - sameValueAs")
    @Test
    void setIsDateValidSameValueAs() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));
        TransactionDate transactionDateTwo = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.sameValueAs(transactionDateTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create TransactionDate - sameValueAsFalse")
    @Test
    void setIsDateValidSameValueAsFalse() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));
        TransactionDate transactionDateTwo = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.sameValueAs(transactionDateTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create transactionDate - sameValueAsNull")
    @Test
    void setIsDateValidSameValueAsNull() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.sameValueAs(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create transactionDate - Override Equals False")
    @Test
    void setIsTransactionDateValidOverrideEqualsFalse() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));
        TransactionDate transactionDateTwo = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.equals(transactionDateTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create transactionDate - Override Equals Null")
    @Test
    void setIsTransactionDateValidOverrideEqualsNull() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Create transactionDate - Null LocalDateTime")
    @Test
    void setIsTransactionDateValidNullLocalDateTime() {
        //Arrange
        LocalDateTime dateTest = null;

        //Act
        assertThrows(NullPointerException.class, () -> {
            TransactionDate transactionDate = new TransactionDate(dateTest);
        });
    }

    @DisplayName("create transactionDate - Override Equals Same Object")
    @Test
    void setIsTransactionDateeValidOverrideEqualsSameObject() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = transactionDateOne.equals(transactionDateOne);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create transactionDate - Override Equals Different Class")
    @Test
    void setIsDateValidOverrideEqualsDifferentClass() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));
        Name name = new Name("Ludovina");
        //Act
        boolean result = transactionDateOne.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        int result = transactionDateOne.hashCode();

        //Assert
        assertEquals(-554388369, result);
    }

    @DisplayName("toString() - Happy Path")
    @Test
    void toStringHappyPath() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        String result = transactionDateOne.toString();

        //Assert
        assertEquals("1988-03-10T22:10:30", result);
    }

    @DisplayName("getDate() - Happy Path")
    @Test
    void getDateHappyPath() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        LocalDateTime result = transactionDateOne.getDate();
        LocalDateTime expected = LocalDateTime.of(1988, 03, 10, 22, 10, 30);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("convertFromString")
    @Test
    void testConvertFromString() {
        //Arrange
        TransactionDate transactionDateOne = new TransactionDate(LocalDateTime.of(1999, 03, 10, 22, 10, 30, 213123));
        String expected = "1999-03-10T22:10:30";

        //Act
        TransactionDate result = new TransactionDate("1999-03-10T22:10:30.000213123");

        //Assert
        assertEquals(expected, result.toString());
    }
}