package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AmountTest {
    @DisplayName("create amount Happy Path")
    @Test
    void setAmountIfValidHappyPath() {
        //Arrange//Act//Assert
        new Amount(123.89);
    }

    @DisplayName("getAmount() - Happy Path")
    @Test
    void getAmountHappyPath() {
        //Arrange
        Amount testAmount = new Amount(13.3);
        Double expected = 13.3;

        //Act
        Double result = testAmount.getAmount();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Same Amount Object")
    @Test
    void getAmountSameObject() {
        //Arrange
        Amount testAmount = new Amount(13.3);

        //Act
        boolean result = testAmount.equals(testAmount);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Different objects same values")
    @Test
    void getAmountDifferentObjectSameValues() {
        //Arrange
        Amount testAmount = new Amount(144.0);
        Amount testAmount2 = new Amount(144.0);

        //Act
        boolean result = testAmount.equals(testAmount2);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Different class object")
    @Test
    void getAmountDifferentClassObject() {
        //Arrange
        Amount testAmount = new Amount(144.6);

        Name nuno = new Name("Nuno");

        //Act
        boolean result = testAmount.equals(nuno);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - null object")
    @Test
    void getAmountNullObject() {
        //Arrange
        Amount testAmount = new Amount(144.6);

        //Act
        boolean result = testAmount.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Different Amount values")
    @Test
    void sameValueAsDifferentAmountValues() {
        //Arrange
        Amount testAmount = new Amount(144.6);
        Amount compareAmount = new Amount(136.78);

        //Act
        boolean result = testAmount.sameValueAs(compareAmount);

        //Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Amount to compare is null")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        Amount testAmount = new Amount(144.6);
        Amount compareAmount = null;

        //Act
        boolean result = testAmount.sameValueAs(compareAmount);

        //Assert
        assertFalse(result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        //Arrange
        Amount amount = new Amount(123.7);
        Amount amountSameAttributes = new Amount(123.7);

        int expectedResult = amountSameAttributes.hashCode();

        int result = amount.hashCode();

        assertEquals(expectedResult, result);
    }

    @DisplayName("hashCode() - Fixed value")
    @Test
    void hashCodeFixed() {
        //Arrange
        Amount amount = new Amount(123.7);

        int expected = -1936580576;

        int result = amount.hashCode();

        assertEquals(expected, result);
    }
}



