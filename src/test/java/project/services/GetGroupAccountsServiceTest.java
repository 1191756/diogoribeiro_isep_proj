package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.Date;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class GetGroupAccountsServiceTest {
    @Autowired
    GetGroupAccountsService getGroupAccountsService;
    @Autowired
    AccountRepository accountMockRepository;
    @Autowired
    GroupRepository groupMockRepository;

    @DisplayName("getGroupAccounts - Happy Path")
    @Test
    void getGroupAccounts_HappyPath() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariaconfitada@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisiogreat@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunohombrero@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        // GROUPS
        Description familyDescription = new Description("Family4");
        Group familyGroup = new Group(familyDescription, father.getPersonID());

        familyGroup.addMember(son.getPersonID());
        familyGroup.addMember(mother.getPersonID());
        familyGroup.addMember(sibling.getPersonID());

        //ACCOUNTS
        Account account1 = new Account(new Denomination("first account"),new Description("Created to test service"),familyGroup.getID());

        Set<AccountDTO> accountsDTO = new HashSet<>();

        AccountDTO accountDTO1 = new AccountDTO();
        accountDTO1.setAccountID(account1.getAccountID());
        accountDTO1.setDescription(account1.getDescription());

        accountsDTO.add(accountDTO1);

        List<Account> accounts = new ArrayList<Account>() {};
        accounts.add(account1);

        Set<AccountDTO> expectedDTOSet = new HashSet<>();
        expectedDTOSet.add(AccountAssembler.mapToDTO(account1));

        Mockito.when(groupMockRepository.findById(familyGroup.getID())).thenReturn(Optional.of(familyGroup));
        Mockito.when(accountMockRepository.findAllByAccountID_OwnerID(familyGroup.getID())).thenReturn(accounts);

        //Act
        Set<AccountDTO> resultDTOSet = getGroupAccountsService.getGroupAccounts(familyGroup.getID().toString());

        for(AccountDTO iterator : expectedDTOSet){
            assertTrue(resultDTOSet.contains(iterator));
        }
    }

    @DisplayName("getGroupAccounts - Happy Path")
    @Test
    void getGroupAccounts_NonExistingGroup() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        // GROUPS
        Description familyDescription = new Description("Family");
        Group familyGroup = new Group(familyDescription, father.getPersonID());


        Mockito.when(groupMockRepository.findById(familyGroup.getID())).thenReturn(Optional.empty());

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            getGroupAccountsService.getGroupAccounts(familyDescription.getDescription());
        });

        String expectedMessage = "GroupID";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}