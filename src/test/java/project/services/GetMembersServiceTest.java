package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.PersonDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class GetMembersServiceTest {
    @Autowired
    GetMembersService getMembersService;
    @Autowired
    PersonRepository personMockRepository;
    @Autowired
    GroupRepository groupMockRepository;

    @DisplayName("getMembers - members found")
    @Test
    void getMembersHappyPath() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name jose = new Name("José");
        Email joseEmail = new Email("josebides@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariabides@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);

        Mockito.when(personMockRepository.findById(father.getPersonID())).thenReturn(java.util.Optional.of(father));
        Mockito.when(personMockRepository.findById(mother.getPersonID())).thenReturn(java.util.Optional.of(mother));

        // GROUPS
        Description membersDescription = new Description("Members' Group");
        Group membersGroup = new Group(membersDescription, father.getPersonID());
        Set<Person> membersCollection = new HashSet<>();
        membersGroup.addMember(mother.getPersonID());
        membersCollection.add(mother);

        Mockito.when(groupMockRepository.findById(membersGroup.getID())).thenReturn(java.util.Optional.of(membersGroup));

        //Act
        Set<PersonDTO> result = getMembersService.getMembers("Members' Group");

        assertEquals(2, result.size());
    }
}

