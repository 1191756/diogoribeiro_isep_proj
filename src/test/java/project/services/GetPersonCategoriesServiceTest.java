package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoryDTO;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class GetPersonCategoriesServiceTest {
    @Autowired
    private GetPersonCategoriesService getPersonCategoriesService;
    @Autowired
    private PersonRepository personMockRepository;

    @DisplayName("Get categories of a Group - Happy path ")
    @Test
    void getCategoriesGroupHappyPath() {
        //Arrange
        //Person
        LocalDateTime personBirthDate = LocalDateTime.of(1913, Month.FEBRUARY, 05, 23, 5, 6);
        Email personEmail = new Email("josejoao@gmail.com");
        PersonID personID = new PersonID(personEmail);
        Person person = new Person(new Name("Jose Joao"), new Address("Porto"), new Date(personBirthDate), personEmail, null, null);

        //Category
        Designation designationCategory1 = new Designation("Flores");
        Designation designationCategory2 = new Designation("Vegetables");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        person.addCategory(category1);
        person.addCategory(category2);

        CategoryDTO categoryDTOExp1 = new CategoryDTO();
        CategoryDTO categoryDTOExp2 = new CategoryDTO();
        categoryDTOExp1.setDesignation(new Designation("Flores"));
        categoryDTOExp2.setDesignation(new Designation("Vegetables"));
        Set<CategoryDTO> expected = new HashSet<>();
        expected.add(categoryDTOExp1);
        expected.add(categoryDTOExp2);

        Mockito.when(personMockRepository.findById(personID)).thenReturn(Optional.of(person));

        //Act
        Set<CategoryDTO> result = getPersonCategoriesService.getPeronCategories("josejoao@gmail.com");

        //Assert
        for (CategoryDTO iterator : expected) {
            assertTrue(result.contains(iterator));
        }
    }

    @DisplayName("getCategories - Person Not Found ")
    @Test
    void getCategoriesPersonNotFound() {
        //Arrange
        //PersonId
        Email personEmail = new Email("pedro@gmail.com");
        PersonID personID = new PersonID(personEmail);

        Mockito.when(personMockRepository.findById(personID)).thenThrow(new NotFoundException("PersonID"));

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            getPersonCategoriesService.getPeronCategories("pedro@gmail.com");
        });

        String expectedMessage = "PersonID";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }

}