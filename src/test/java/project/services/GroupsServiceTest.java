package project.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class GroupsServiceTest {
    @Autowired
    GroupsService groupsService;
    @Autowired
    GroupRepository groupMockRepository;
    @Autowired
    AddGroupService addGroupService;

    PersonID chosen;
    Description description1;
    Email email1;
    PersonID responsible1;
    Group ACDCFans;
    Set<Group> expectedGroups;
    GroupDTO ACDCFansDTO;
    Description description2;
    Email email;
    PersonID responsible2;
    Group QotSAFans;
    GroupDTO QotSAFansDTO;
    Description description0;

    @BeforeEach
    void setUpForTests() {
        description0 = new Description("NickelbackFanClub");
        chosen = new PersonID(new Email("chosen@gmail.com"));
        description1 = new Description("ACDCFansClub");
        email1 = new Email("brianJohnson@gmail.com");
        responsible1 = new PersonID(email1);
        ACDCFans = new Group(description1, responsible1);
        ACDCFans.addMember(chosen);
        expectedGroups = new HashSet<>();
        ACDCFansDTO = GroupAssembler.mapToDTO(ACDCFans);
        expectedGroups.add(ACDCFans);

        description2 = new Description("QotSAFansClub");
        email = new Email("joshHomme@gmail.com");
        responsible2 = new PersonID(email);
        QotSAFans = new Group(description2, responsible2);
        QotSAFans.addMember(chosen);
        QotSAFansDTO = GroupAssembler.mapToDTO(QotSAFans);
        expectedGroups.add(QotSAFans);
    }

    @DisplayName("getGroups - HappyPath")
    @Test
    void getGroupsHappyPath() {
        //Arrange
        groupMockRepository.save(ACDCFans);
        groupMockRepository.save(QotSAFans);

        Mockito.when(groupMockRepository.findAll()).thenReturn(expectedGroups);

        //Act
        Set<GroupDTO> allGroups = groupsService.getGroups(chosen);

        //Assert
        assertTrue(allGroups.contains(QotSAFansDTO) && allGroups.contains(ACDCFansDTO));
    }

    @DisplayName("getGroup - Happy Path")
    @Test
    void getGroupHappyPath() {

        groupMockRepository.save(ACDCFans);

        Mockito.when(groupMockRepository.findById(new GroupID(description1))).thenReturn(java.util.Optional.ofNullable(ACDCFans));

        //Act
        GroupDTO groupDTO = groupsService.getGroup("ACDCFansClub");

        //Assert
        assertEquals(groupDTO, ACDCFansDTO);
    }

    @DisplayName("getGroup - NotFoundException - Group")
    @Test
    void getGroupsNotFoundException() {
        //Arrange
        groupMockRepository.save(ACDCFans);
        groupMockRepository.save(QotSAFans);

        Mockito.when(groupMockRepository.findById(new GroupID(description0))).thenThrow(new NotFoundException("Group Not Found"));

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            groupsService.getGroup(description0.toString());
        });

        String expectedMessage = "Group Not Found";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}