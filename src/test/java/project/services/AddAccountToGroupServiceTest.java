package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.assemblers.AddAccountToGroupRequestAssembler;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.group.Group;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class AddAccountToGroupServiceTest {
    @Autowired
    private AccountRepository accountMockRepository;
    @Autowired
    private GroupRepository groupMockRepository;
    @Autowired
    private AddAccountToGroupService addAccountToGroupService;

    @DisplayName("addAccount() - Happy Path")
    @Test
    void addAccountHappyPath() {
        //Arrange
        Denomination denomination = new Denomination("Sardines");
        Description groupDescription = new Description("Fishing Group");
        GroupID groupID = new GroupID(groupDescription);
        Email email = new Email("pedro@gmail.com");
        PersonID personID = new PersonID(email);
        Denomination accountDenomination = new Denomination("Fish");
        Description accountDescription = new Description("Big");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = new AddAccountToGroupRequestDTO(accountDenomination, accountDescription, personID, groupID);

        Group targetGroup = new Group(groupDescription, personID);
        groupMockRepository.save(targetGroup);

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, groupID));

        Mockito.when(groupMockRepository.findById(new GroupID(groupDescription))).thenReturn(Optional.of(targetGroup));
        Mockito.when(accountMockRepository.findById(new AccountID(accountDenomination, groupID))).thenReturn(Optional.empty());

        Account account = new Account(accountDenomination, accountDescription, groupID);
        Account savedAccount = new Account(accountDenomination, accountDescription, groupID);
        Mockito.when(accountMockRepository.save(account)).thenReturn(savedAccount);

        //Act
        AccountDTO result = addAccountToGroupService.addAccountToGroup(addAccountToGroupRequestDTO);

        //Assert
        assertEquals(expected.getAccountID(), result.getAccountID());
    }

    @DisplayName("addAccount() - Test if exception is thrown when GroupID is not found in GroupRepository")
    @Test
    void addAccountTestIfGroupNotFound() {
        //Arrange
        String groupDescription = "Surf and wind Group";
        String responsibleEmail = "miguel@gmail.com";
        String accountDenomination = "White";
        String accountDescription = "surf board";
        GroupID groupID = new GroupID(new Description(groupDescription));

        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(accountDenomination, accountDescription, responsibleEmail, groupDescription);

        Mockito.when(groupMockRepository.findById(groupID)).thenReturn(Optional.empty());

        //Act/Assert
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addAccountToGroupService.addAccountToGroup(addAccountToGroupRequestDTO);
        });

        String expectedMessage = "GroupID";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addAccount() - Test if exception is thrown when the account to add already exist in the repository")
    @Test
    void addAccountTestIfAccountAlreadyExists() {
        //Arrange
        //Arrange
        Denomination denomination = new Denomination("Sardines");
        Description groupDescription = new Description("Fishing Group");
        GroupID groupID = new GroupID(groupDescription);
        Email email = new Email("pedro@gmail.com");
        PersonID personID = new PersonID(email);
        Denomination accountDenomination = new Denomination("Fish");
        Description accountDescription = new Description("Big");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = new AddAccountToGroupRequestDTO(accountDenomination, accountDescription, personID, groupID);

        Group targetGroup = new Group(groupDescription, personID);
        groupMockRepository.save(targetGroup);

        Account account = new Account(accountDenomination, accountDescription, groupID);
        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, groupID));
        Mockito.when(groupMockRepository.findById(new GroupID(groupDescription))).thenReturn(Optional.of(targetGroup));
        Mockito.when(accountMockRepository.findById(new AccountID(accountDenomination, groupID))).thenReturn(Optional.of(account));

        //Act/Assert
        Exception exception = assertThrows(AlreadyExistsException.class, () -> {
            addAccountToGroupService.addAccountToGroup(addAccountToGroupRequestDTO);
        });

        String expectedMessage = "That account is already in the repository";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }
}
