package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.PersonDTO;
import project.dto.assemblers.PersonAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class PersonsServiceTest {
    @Autowired
    PersonsService personsService;
    @Autowired
    PersonRepository personMockRepository;

    @DisplayName("getPersons - HappyPath")
    @Test
    void getPersons() {
        //Arrange
        //PersonsBirthDate
        LocalDateTime personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        Email motherEmail = new Email("cacildadomingues@gmail.com");
        PersonID motherID = new PersonID(motherEmail);
        Person mother = new Person(new Name("Cacilda Domingues"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        Email fatherEmail = new Email("diogodesousa@gmail.com");
        PersonID fatherID = new PersonID(fatherEmail);
        Person father = new Person(new Name("Diogo de Sousa"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        Email brotherEmail = new Email("ruidamura@gmail.com");
        PersonID brotherID = new PersonID(brotherEmail);
        Person brother = new Person(new Name("Rui de Sousa"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Person
        Email personEmail = new Email("valentina@gmail.com");
        Person person = new Person(new Name("Valentina de Sousa"), new Address("Guimarães"), new Date(personBirthDate), personEmail, motherID, fatherID);
        person.addSibling(brotherID);

        personMockRepository.save(mother);
        personMockRepository.save(father);
        personMockRepository.save(brother);
        personMockRepository.save(person);

        List<Person> repoPersons = new ArrayList<>();
        repoPersons.add(mother);
        repoPersons.add(father);
        repoPersons.add(brother);
        repoPersons.add(person);

        Mockito.when(personMockRepository.findAll()).thenReturn(repoPersons);

        //Act
        Set<PersonDTO> allPersons = personsService.getPersons();

        //Assert
        assertTrue(allPersons.contains(PersonAssembler.mapToDTO(person)) && allPersons.contains(PersonAssembler.mapToDTO(brother)));
    }

    @DisplayName("getPersonById - HappyPath")
    @Test
    void getPersonById() {
        //Arrange
        //PersonsBirthDate
        LocalDateTime personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        Email motherEmail = new Email("cacildadomingues@gmail.com");
        PersonID motherID = new PersonID(motherEmail);
        Person mother = new Person(new Name("Cacilda Domingues"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        Email fatherEmail = new Email("diogodesousa@gmail.com");
        PersonID fatherID = new PersonID(fatherEmail);
        Person father = new Person(new Name("Diogo de Sousa"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        Email brotherEmail = new Email("ruidamura@gmail.com");
        PersonID brotherID = new PersonID(brotherEmail);
        Person brother = new Person(new Name("Raul de Sousa"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Person
        Email personEmail = new Email("amanda@gmail.com");
        Person person = new Person(new Name("Amanda de Sousa"), new Address("Guimarães"), new Date(personBirthDate), personEmail, motherID, fatherID);
        person.addSibling(brotherID);

        personMockRepository.save(mother);
        personMockRepository.save(father);
        personMockRepository.save(brother);
        personMockRepository.save(person);

        Mockito.when(personMockRepository.findById(new PersonID(personEmail))).thenReturn(java.util.Optional.of(person));

        //Act
        PersonDTO personById = personsService.getPersonById("amanda@gmail.com");

        //Assert
        assertTrue(personById.getPersonID().sameValueAs(person.getPersonID()));
    }

    @DisplayName("getPersonById - PersonNotFound")
    @Test
    void getPersonByIdPersonNotFound() {

        //Arrange
        //PersonsBirthDate
        LocalDateTime personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        Email motherEmail = new Email("cacildadomingues@gmail.com");
        PersonID motherID = new PersonID(motherEmail);
        Person mother = new Person(new Name("Cacilda Domingues"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        Email fatherEmail = new Email("diogodesousa@gmail.com");
        PersonID fatherID = new PersonID(fatherEmail);
        Person father = new Person(new Name("Diogo de Sousa"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        Email brotherEmail = new Email("ruidamura@gmail.com");
        PersonID brotherID = new PersonID(brotherEmail);
        Person brother = new Person(new Name("Raul de Sousa"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Person
        Email personEmail = new Email("amanda@gmail.com");
        Person person = new Person(new Name("Amanda de Sousa"), new Address("Guimarães"), new Date(personBirthDate), personEmail, motherID, fatherID);
        person.addSibling(brotherID);

        personMockRepository.save(mother);
        personMockRepository.save(father);
        personMockRepository.save(brother);
        personMockRepository.save(person);

        Mockito.when(personMockRepository.findById(new PersonID(personEmail))).thenThrow(new NotFoundException("Person Not Found"));

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            personsService.getPersonById("luisa@gmail.com");
        });

        String expectedMessage = "PersonID not found";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage,actualMessage);
    }
}