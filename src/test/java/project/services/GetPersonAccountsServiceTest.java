package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.person.Person;
import project.model.shared.Date;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class GetPersonAccountsServiceTest {
    @Autowired
    GetPersonAccountsService getPersonAccountsService;
    @Autowired
    AccountRepository accountMockRepository;
    @Autowired
    PersonRepository personMockRepository;

    @DisplayName("getPersonAccounts - Happy Path")
    @Test
    void getPersonAccounts_HappyPath() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name name = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisiogreat@family.com");
        Person tarcisio = new Person(name, birthAddress, personsBirthdate, tarcisioEmail, null,null);

        //ACCOUNTS
        Account account1 = new Account(new Denomination("first account"),new Description("Created to test service"),tarcisio.getPersonID());

        Set<AccountDTO> accountsDTO = new HashSet<>();

        AccountDTO accountDTO1 = new AccountDTO();
        accountDTO1.setAccountID(account1.getAccountID());
        accountDTO1.setDescription(account1.getDescription());

        accountsDTO.add(accountDTO1);

        List<Account> accounts = new ArrayList<Account>() {};
        accounts.add(account1);

        Set<AccountDTO> expectedDTOSet = new HashSet<>();
        expectedDTOSet.add(AccountAssembler.mapToDTO(account1));

        Mockito.when(personMockRepository.findById(tarcisio.getPersonID())).thenReturn(Optional.of(tarcisio));
        Mockito.when(accountMockRepository.findAllByAccountID_OwnerID(tarcisio.getPersonID())).thenReturn(accounts);

        //Act
        Set<AccountDTO> resultDTOSet = getPersonAccountsService.getPersonAccounts(tarcisio.getPersonID().toString());

        for(AccountDTO iterator : expectedDTOSet){
            assertTrue(resultDTOSet.contains(iterator));
        }
    }

    @DisplayName("getGroupAccounts - Happy Path")
    @Test
    void getGroupAccounts_NonExistingPerson() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);


        Mockito.when(personMockRepository.findById(father.getPersonID())).thenReturn(Optional.empty());

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            getPersonAccountsService.getPersonAccounts(father.getPersonID().getEmail().toString());
        });

        String expectedMessage = "PersonID";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}