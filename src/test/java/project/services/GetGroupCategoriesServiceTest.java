package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoryDTO;
import project.model.group.Group;
import project.model.shared.*;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class GetGroupCategoriesServiceTest {

    @Autowired
    private GetGroupCategoriesService getGroupCategoriesService;
    @Autowired
    private GroupRepository groupMockRepository;

    @DisplayName("Get categories of a Group - Happy path ")
    @Test
    void getCategoriesGroupHappyPath() {
        //Arrange

        //Group
        Description groupDescription = new Description("Farmer Group");
        Email email = new Email("toninho@gmail.com");
        PersonID personID = new PersonID(email);
        Group group = new Group(groupDescription, personID);
        GroupID groupID = new GroupID(groupDescription);

        //Category
        Designation designationCategory1 = new Designation("Flores");
        Designation designationCategory2 = new Designation("Vegetables");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        group.addCategory(category1);
        group.addCategory(category2);

        CategoryDTO categoryDTOExp1 = new CategoryDTO();
        CategoryDTO categoryDTOExp2 = new CategoryDTO();
        categoryDTOExp1.setDesignation(new Designation("Flores"));
        categoryDTOExp2.setDesignation(new Designation("Vegetables"));
        Set<CategoryDTO> expected = new HashSet<>();
        expected.add(categoryDTOExp1);
        expected.add(categoryDTOExp2);

        Mockito.when(groupMockRepository.findById(groupID)).thenReturn(Optional.of(group));

        //Act
        Set<CategoryDTO> result = getGroupCategoriesService.getGroupCategories("Farmer Group");

        //Assert
        for (CategoryDTO iterator : expected) {
            assertTrue(result.contains(iterator));
        }
    }
}