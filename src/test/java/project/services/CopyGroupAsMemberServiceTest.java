package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddGroupRequestAssembler;
import project.model.group.Group;
import project.model.shared.*;
import project.repositories.GroupRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test2")//VER MELHOR
@ExtendWith(SpringExtension.class)
public class CopyGroupAsMemberServiceTest {

    @Autowired
    GroupRepository groupMockRepository;
    @Autowired
    CopyGroupAsMemberServiceAV copyGroupAsMemberServiceAV;

    Email email;
    Description groupDescription;
    PersonID member;

    @DisplayName("As a member, I want to copy a group, but only its members and categories. - Happy path ")
    @Test
    void CopyGroupAsMemberHappyPath() {
        //Arrange
        Denomination denomination = new Denomination("Sardines");
        Description groupDescription = new Description("Fishing Group");
        GroupID groupID = new GroupID(groupDescription);
        Email email = new Email("pedro@gmail.com");
        PersonID personID = new PersonID(email);
        Denomination accountDenomination = new Denomination("Fish");
        Description accountDescription = new Description("Big");
        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(groupDescription.getDescription(), email.getEmail());

        Group targetGroup = new Group(groupDescription, personID);
        groupMockRepository.save(targetGroup);

        GroupDTO expected = new GroupDTO();
        expected.setGroupID(new GroupID(groupDescription));

        Mockito.when(groupMockRepository.findById(new GroupID(groupDescription))).thenReturn(Optional.of(targetGroup));

        //Act
        GroupDTO result = copyGroupAsMemberServiceAV.copyGroupAsMemberServiceAV(addGroupRequestDTO);

        //Assert
        assertEquals(result.getGroupID(), expected.getGroupID());
    }
}