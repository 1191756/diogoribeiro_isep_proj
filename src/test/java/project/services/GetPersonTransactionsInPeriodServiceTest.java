package project.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;
import project.utils.ClockUtil;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class GetPersonTransactionsInPeriodServiceTest {
    private final static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2020, 6, 2, 13, 0, 0, 0);
    @Autowired
    GetPersonTransactionsInPeriodService getPersonTransactionsInPeriodService;
    @Autowired
    GroupRepository groupMockRepository;
    @Autowired
    LedgerRepository ledgerMockRepository;
    @Autowired
    PersonRepository personMockRepository;
    @Autowired
    ClockUtil clockUtilMock;
    Clock clock;
    Clock fixedClock;

    //BeforeEach
    Person person1;
    Person person2;
    Category category1;
    Category category2;
    Group group1;
    Group group2;
    Ledger ledger1;
    AccountID debit1;
    AccountID debit2;
    AccountID credit1;
    AccountID credit2;
    Transaction transaction1;
    Transaction transaction2;
    Transaction transaction3;
    Transaction transaction4;
    Transaction transaction5;
    Transaction transaction6;
    Description description1;
    Description description2;
    Address birthAddress1;
    Address birthAddress2;
    Date birthdate1;
    Date birthdate2;
    Name name1;
    Name name2;
    Email email1;
    Email email2;

    @BeforeEach
    void setUpForTests() {
        clock = Clock.systemDefaultZone();
        fixedClock = Clock.fixed(LOCAL_DATE_TIME.toInstant(ZoneOffset.UTC), ZoneId.systemDefault());

        //Person1
        birthAddress1 = new Address("Maia, Portugal");
        birthdate1 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name1 = new Name("Ricardo Carvalho");
        email1 = new Email("ricardo@family.com");
        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);

        //Person2
        birthAddress2 = new Address("Maia, Portugal");
        birthdate2 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name2 = new Name("Vitor Carvalho");
        email2 = new Email("vitor@family.com");
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);

        personMockRepository.save(person1);
        personMockRepository.save(person2);

        //Categories
        category1 = new Category(new Designation("Shopping"));
        category2 = new Category(new Designation("Services"));

        //Group1
        description1 = new Description("FantasticGroup");

        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);

        //Group2
        description2 = new Description("AmazingGroup");

        group2 = new Group(description2, person2.getPersonID());

        group2.addCategory(category1);
        group2.addCategory(category2);

        //Ledger1
        ledger1 = new Ledger(new LedgerID(person1.getPersonID()));

        //Accounts
        debit1 = new AccountID(new Denomination("Primary account"), person1.getPersonID());
        credit1 = new AccountID(new Denomination("Secondary account"), person1.getPersonID());

        debit2 = new AccountID(new Denomination("home account"), person1.getPersonID());
        credit2 = new AccountID(new Denomination("business account"), person1.getPersonID());

        //Transaction1
        transaction1 = new Transaction(100.0,
                new Description("simulated transaction"),
                new TransactionDate(LocalDateTime.of(2020, 06, 12, 8, 0)),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction2
        transaction2 = new Transaction(5.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.of(2020, 06, 12, 8, 0)),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        //Transaction3
        transaction3 = new Transaction(380.5,
                new Description("big transaction"),
                new TransactionDate(LocalDateTime.of(2020, 04, 12, 8, 0)),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction4
        transaction4 = new Transaction(3.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.of(2020, 06, 12, 8, 0)),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        //Transaction5
        transaction5 = new Transaction(3.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.of(2020, 5, 5, 7, 30)),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        //Transaction6
        transaction6 = new Transaction(3.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.of(2020, 6, 15, 7, 30)),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        ledger1.addTransaction(transaction1);
        ledger1.addTransaction(transaction2);
        ledger1.addTransaction(transaction3);
        ledger1.addTransaction(transaction4);
        ledger1.addTransaction(transaction5);
        ledger1.addTransaction(transaction6);
    }

    @DisplayName("GetPersonTransactionsInPeriodService - Happy path")
    @Test
    public void addTransactionToGroup_HappyPath() {
        //Arrange
        String personID = "ricardo@family.com";
        String accountDenomination = "Primary account";
        String initialDate = "2020-05-05 07:30";
        String endDate = "2020-06-15 07:30";

        Mockito.when(personMockRepository.findById(person1.getPersonID())).thenReturn(Optional.of(person1));
        Mockito.when(ledgerMockRepository.findById(ledger1.getLedgerID())).thenReturn(Optional.of(ledger1));

        //Act
        Set<TransactionDTO> expected = new HashSet<>();
        expected.add(TransactionAssembler.mapToDTO(transaction1));
        expected.add(TransactionAssembler.mapToDTO(transaction2));
        expected.add(TransactionAssembler.mapToDTO(transaction4));

        Set<TransactionDTO> resultDTO = getPersonTransactionsInPeriodService.getPersonTransactionsInPeriod(personID, accountDenomination, initialDate, endDate);

        //Assert
        for (TransactionDTO iterator : expected) {
            assertTrue(resultDTO.contains(iterator));
        }
    }

    @DisplayName("GetPersonTransactionsInPeriodService - Non Existing Person")
    @Test
    public void addTransactionToGroup_NonExistingPerson() {
        //Arrange
        String personID2 = "joao@family.com";
        String accountDenomination = "Primary account";
        String initialDate = "2020-05-05 07:30";
        String endDate = "2020-06-15 07:30";

        Mockito.when(personMockRepository.findById(person1.getPersonID())).thenReturn(Optional.of(person1));
        Mockito.when(ledgerMockRepository.findById(ledger1.getLedgerID())).thenReturn(Optional.of(ledger1));

        //Act
        Set<TransactionDTO> expected = new HashSet<>();
        expected.add(TransactionAssembler.mapToDTO(transaction1));
        expected.add(TransactionAssembler.mapToDTO(transaction2));
        expected.add(TransactionAssembler.mapToDTO(transaction4));
        expected.add(TransactionAssembler.mapToDTO(transaction5));
        expected.add(TransactionAssembler.mapToDTO(transaction6));

        Exception exception = assertThrows(NotFoundException.class, () -> {
            Set<TransactionDTO> resultDTO = getPersonTransactionsInPeriodService.getPersonTransactionsInPeriod(personID2, accountDenomination, initialDate, endDate);
        });

        String expectedMessage = "PersonID";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}
