package project.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.model.person.Person;
import project.model.shared.*;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class PersonRepositoryTest {
    @Autowired
    PersonRepository personRepository;
    Description description;
    Name name;
    Address birthPlace;
    Email email;
    PersonID responsible;
    Date birthDate;

    @BeforeEach()
    void setUp() {
        description = new Description("Sports for the Family");
        email = new Email("joana@gmail.com");
        responsible = new PersonID(email);
        name = new Name("Joana");
        birthPlace = new Address("Porto, Portugal");
        birthDate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
    }

    @DisplayName("save() - Happy Path")
    @Test
    void saveHappyPath() {
        //Arrange
        Person testPerson = new Person(name, birthPlace, birthDate, email, null, null);

        //Act
        Person result = personRepository.save(testPerson);
        System.out.println(result.getPersonID());
        //Assert
        assertEquals(testPerson, result);
    }

    @DisplayName("save() - Null Person")
    @Test
    void saveNullPerson() {
        //Arrange
        Person testPerson = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Person result = personRepository.save(testPerson);
        });
    }

    @Transactional
    @DisplayName("findAll() - Happy Path")
    @Test
    void findAll() {
        //Arrange
        final Person[] result = {null};
        Person testPerson = new Person(name, birthPlace, birthDate, email, null, null);
        personRepository.save(testPerson);

        Person expected = new Person(name, birthPlace, birthDate, email, null, null);

        //Act
        Iterable<Person> persons = personRepository.findAll();

        persons.forEach(group -> {
            result[0] = group;
        });

        //Assert
        assertEquals(testPerson, result[0]);
    }

    @Transactional
    @DisplayName("findById() - Happy Path")
    @Test
    void findByIdHappyPath() {
        //Assert
        PersonID personID = new PersonID(email);
        Person testPerson = new Person(name, birthPlace, birthDate, email, null, null);
        Person resultPerson = null;
        personRepository.save(testPerson);

        //Act
        if (personRepository.findById(personID).isPresent()) {
            resultPerson = personRepository.findById(personID).get();
        }

        //Assert
        assertEquals(testPerson, resultPerson);
    }

    @DisplayName("findById() - No persons found")
    @Test
    void findById() {
        personRepository.deleteAll();
        //Assert
        PersonID personID = new PersonID(email);
        boolean result = true;

        //Act
        if (!personRepository.findById(personID).isPresent()) {
            result = false;
        }

        //Assert
        assertFalse(result);
    }
}