package project.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.model.ledger.Ledger;
import project.model.shared.*;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@Transactional
class LedgerRepositoryTest {

    @Autowired
    LedgerRepository ledgerRepository;
    LedgerID ledgerID;
    GroupID groupID;
    double amount;
    Description description;
    Category category;
    AccountID debit;
    AccountID credit;
    TransactionType type;
    TransactionDate date;

    @BeforeEach
    void setUp() {
        // Arrange
        ledgerID = new LedgerID(groupID);
        groupID = new GroupID(new Description("High School"));
        amount = 50.0;
        description = new Description("Teenagers");
        category = new Category(new Designation("Classes"));
        debit = new AccountID(new Denomination("Banc"), groupID);
        credit = new AccountID(new Denomination("coins"), groupID);
        type = TransactionType.DEBIT;

        date = new TransactionDate(LocalDateTime.of(2000, Month.JANUARY, 4, 21, 30, 45));
    }


    @DisplayName("save() - Null Ledger")
    @Test
    void saveNullPerson() {
        //Arrange
        Ledger testledger = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Ledger result = ledgerRepository.save(testledger);
        });
    }


    @DisplayName("findById() - No ledger found")
    @Test
    void findById() {
        //Arrange
        boolean result = true;

        //Act
        if (!ledgerRepository.findById(ledgerID).isPresent()) {
            result = false;
        }

        //Assert
        assertFalse(result);
    }

    @DisplayName("findAll()")
    @Test
    void findByAll() {
        //Arrange
        //Act
        List<Ledger> result = ledgerRepository.findAll();

        //Assert
        assertTrue(result.size() >= 0);
    }
}