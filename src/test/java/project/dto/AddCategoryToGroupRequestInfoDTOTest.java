package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

public class AddCategoryToGroupRequestInfoDTOTest {

    String designation;
    String groupDescription;
    String responsibleEmail;
    AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO;

    @BeforeEach
    void setUp() {
        designation = "equipment";
        groupDescription = "Soccer club";
        responsibleEmail = "Rui Frederico";
        addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);
    }

    @Test
    void getDesignationTest() {
        addCategoryToGroupRequestInfoDTO.getDesignation();
    }

    @Test
    void setDesignationTest() {
        addCategoryToGroupRequestInfoDTO.setDesignation(designation);
    }

    @Test
    void getGroupDescriptionTest() {
        addCategoryToGroupRequestInfoDTO.getGroupDescription();
    }

    @Test
    void setGroupDescriptionTest() {
        addCategoryToGroupRequestInfoDTO.setGroupDescription(groupDescription);
    }

    @Test
    void getPersonIDTest() {
        addCategoryToGroupRequestInfoDTO.getGroupDescription();
    }

    @Test
    void setPersonIDTest() {
        addCategoryToGroupRequestInfoDTO.setPersonID(responsibleEmail);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Testing Equals")
    @Test
    void AddCategoryRequestInfoDTOEqualsTrue() {
        //Act
        AddCategoryToGroupRequestInfoDTO expected = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Same Exact Object")
    @Test
    void AddCategoryRequestInfoDTOSameExactObject() {
        //Act
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertEquals(result, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Different Objects")
    @Test
    void AddCategoryRequestInfoDTODiffObjects() {
        //Act
        String designationDif = "Trips";
        String groupDescriptionDiff = "Cheerleaders";
        String responsibleEmailDiff = "tito@mail.com";
        AddCategoryToGroupRequestInfoDTO expected = new AddCategoryToGroupRequestInfoDTO(designationDif, groupDescriptionDiff, responsibleEmailDiff);
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Different type of Object")
    @Test
    void AddCategoryRequestInfoDTODifferentTypeOFObject() {
        Name name = new Name("Name");

        //Act
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertNotEquals(name, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Null Object")
    @Test
    void AddCategoryRequestInfoDTONullObject() {
        //Act
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertFalse(result.equals(null));
    }

    @DisplayName("Equals not same designation")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseDesignation() {
        //Act
        designation = "Ball";
        AddCategoryToGroupRequestInfoDTO expected = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("Equals not same groupDescription")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseGroupDesc() {
        //Act
        groupDescription = "Dancers";
        AddCategoryToGroupRequestInfoDTO expected = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("Equals not same responsibleEmail")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseEmail() {
        //Act
        responsibleEmail = "lala@mail.com";
        AddCategoryToGroupRequestInfoDTO expected = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestInfoDTO result = addCategoryToGroupRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        int expected = 1516386563;
        int result = addCategoryToGroupRequestInfoDTO.hashCode();

        assertEquals(expected, result);
    }
}