package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

public class AddCategoryToGroupRequestDTOTest {
    Designation designation;
    GroupID groupDescription;
    Description description;
    PersonID responsibleEmail;
    Email email;
    AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO;

    @BeforeEach
    void setUp() {
        designation = new Designation("Equipamentos");
        description = new Description("Futebol Clube");
        groupDescription = new GroupID(description);
        email = new Email("ruifrederico@duasdaasneira.com");
        responsibleEmail = new PersonID(email);
        addCategoryToGroupRequestDTO = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
    }

    @Test
    void getDesignationTest() {
        addCategoryToGroupRequestDTO.getDesignation();
    }

    @Test
    void setDesignationTest() {
        addCategoryToGroupRequestDTO.setDesignation(designation);
    }

    @Test
    void getGroupDescriptionTest() {
        addCategoryToGroupRequestDTO.getGroupDescription();
    }

    @Test
    void setGroupDescriptionTest() {
        addCategoryToGroupRequestDTO.setGroupDescription(groupDescription);
    }

    @Test
    void getPersonIDTest() {
        addCategoryToGroupRequestDTO.getGroupDescription();
    }

    @Test
    void setPersonIDTest() {
        addCategoryToGroupRequestDTO.setResponsibleEmail(responsibleEmail);
    }

    @DisplayName("AddCategoryRequestDTOEqualsTrue - Testing Equals")
    @Test
    void TestEqualsTrue() {
        //Act
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestDTOEqualsTrue - Exact same object")
    @Test
    void TestEqualsExactSameObject() {
        //Act
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;

        //Assert
        assertEquals(result, result);
    }

    @Test
    void TestEqualsFalse() {
        //Act
        designation = new Designation("Caça");
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentPersonID() {
        //Arrange
        email = new Email("marcoantonio@romano.com");
        responsibleEmail = new PersonID(email);
        //Act
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentDesignation() {
        //Arrange
        designation = new Designation("Chuteiras");
        //Act
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentGroupID() {
        //Arrange
        description = new Description("Basquetebol Clube");
        groupDescription = new GroupID(description);
        //Act
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = addCategoryToGroupRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsNullObjects() {
        assertNotNull(addCategoryToGroupRequestDTO);
    }

    @Test
    void testEqualsDifferentObjects() {
        description = new Description("Dance School");

        assertNotEquals(addCategoryToGroupRequestDTO, description);
    }

    @Test
    void testHashCode() {
        AddCategoryToGroupRequestDTO expected = new AddCategoryToGroupRequestDTO(designation, groupDescription, responsibleEmail);

        int result = addCategoryToGroupRequestDTO.hashCode();
        int expectedHash = expected.hashCode();

        assertEquals(result, expectedHash);
    }

    @Test
    void testHashCodeFixedValue() {
        int expectedHash = 1429641820;

        int result = addCategoryToGroupRequestDTO.hashCode();

        assertEquals(result, expectedHash);
    }
}