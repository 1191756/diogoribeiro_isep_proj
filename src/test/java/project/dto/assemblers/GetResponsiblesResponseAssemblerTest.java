package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.GetResponsiblesResponseDTO;
import project.dto.PersonDTO;
import project.dto.TransactionDTO;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GetResponsiblesResponseAssemblerTest {
    TransactionDTO transactionDTO;

    Person mario;
    Email marioEmail;
    Address marioBirthPlace;
    Date marioBirthDate;


    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
    }

    @Test
    void mapToDTO() {

        PersonDTO responsibleDTO = PersonAssembler.mapToDTO(mario);

        GetResponsiblesResponseDTO result = GetResponsiblesResponseAssembler.mapToDTO(responsibleDTO);

        GetResponsiblesResponseDTO expected = new GetResponsiblesResponseDTO("mario@family.com");

        assertEquals(expected, result);
    }

    @Test
    void GetFamiliesResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GetResponsiblesResponseAssembler getResponsiblesResponseAssembler = new GetResponsiblesResponseAssembler();
        });
    }

}