package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.AddCategoryToPersonRequestDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddCategoryToPersonRequestAssemblerTest {
    String designation;
    String personEmail;

    @BeforeEach
    void setUp() {
        designation = "Diamonds";
        personEmail = "isabel@angola.com";
    }

    @Test
    void mapToDTO() {
        //Act
        AddCategoryToPersonRequestDTO result = AddCategoryToPersonRequestAssembler.mapToDTO(designation, personEmail);

        //Assert
        assertEquals("Diamonds", result.getDesignation().toString());
        assertEquals("isabel@angola.com", result.getPersonID().toString());
    }
}