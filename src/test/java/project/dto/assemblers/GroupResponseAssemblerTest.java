package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.GroupResponseDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GroupResponseAssemblerTest {
    @DisplayName("GroupResponseAssembler - mapToDTO")
    @Test
    void GroupResponseAssembler_MapToDTO() {
        //Assert
        Group group = new Group(new Description("TestGroup"), new PersonID(new Email("tarcisio@gmail.com")));
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO expectedDTO = new GroupResponseDTO(group.getID().getDescription().getDescription(), responsibles, members, categories);

        GroupResponseDTO result = GroupResponseAssembler.mapToDTO(group.getID().getDescription().getDescription(), responsibles, members, categories);
        assertEquals(expectedDTO, result);
    }

    @Test
    void GroupResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GroupResponseAssembler groupResponseAssembler = new GroupResponseAssembler();
        });
    }
}
