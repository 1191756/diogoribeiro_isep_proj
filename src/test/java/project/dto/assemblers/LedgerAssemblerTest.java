package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.TransactionDTO;
import project.model.ledger.Transaction;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LedgerAssemblerTest {
    Transaction transaction;

    GroupID groupID;
    double amount;
    Description description;
    Category category;
    AccountID debit;
    AccountID credit;
    TransactionType type;
    TransactionDate date;

    @BeforeEach
    void setUp() {
        // Arrange
        groupID = new GroupID(new Description("High School"));
        amount = 50.0;
        description = new Description("Teenagers");
        category = new Category(new Designation("Classes"));
        debit = new AccountID(new Denomination("Banc"), groupID);
        credit = new AccountID(new Denomination("coins"), groupID);
        type = TransactionType.DEBIT;
        date = new TransactionDate(LocalDateTime.of(2000, Month.JANUARY, 4, 21, 30, 45));
    }

    @DisplayName("Leger Assember - exception")
    @Test
    void LedgerAssembler() throws Exception {

        assertThrows(AssertionError.class, () -> {
            LedgerAssembler ledgerAssembler = new LedgerAssembler();
        });
    }

    @DisplayName("Happy path")
    @Test
    void LedgerAssemblerHappyCase() {
        //Arrange
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, type);
        TransactionDTO expected = new TransactionDTO(amount, description, date, category, debit, credit, type);

        //Act
        TransactionDTO result = LedgerAssembler.mapToDTO(transaction);

        //Assert
        assertEquals(expected, result);
    }
}
