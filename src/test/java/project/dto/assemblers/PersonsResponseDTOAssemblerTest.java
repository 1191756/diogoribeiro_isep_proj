package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.PersonDTO;
import project.dto.PersonsResponseDTO;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PersonsResponseDTOAssemblerTest {

    // Arrange
    //PersonsBirthDate
    LocalDateTime personBirthDate;

    //Mother
    Email motherEmail;
    PersonID motherID;
    Person mother;

    //Father
    Email fatherEmail;
    PersonID fatherID;
    Person father;

    //Brother
    Email brotherEmail;
    PersonID brotherID;
    Person brother;

    //Valentina
    Email valentinaEmail;
    PersonID valentinaID;
    Person valentina;
    PersonDTO valentinaDTO;

    //Amanda
    Email amandaEmail;
    Person amanda;
    PersonDTO amandaDTO;

    @BeforeEach
    void setUp() {
        // Arrange
        //PersonsBirthDate
        personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        motherEmail = new Email("cacilda@gmail.com");
        motherID = new PersonID(motherEmail);
        mother = new Person(new Name("Cacilda Maria"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        fatherEmail = new Email("zedamura@gmail.com");
        fatherID = new PersonID(fatherEmail);
        father = new Person(new Name("Jose de Sousa"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        brotherEmail = new Email("ruidamura@gmail.com");
        brotherID = new PersonID(brotherEmail);
        brother = new Person(new Name("Rui de Sousa"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Valentina
        valentinaEmail = new Email("valentina@gmail.com");
        valentinaID = new PersonID(valentinaEmail);
        valentina = new Person(new Name("Valentina de Sousa"), new Address("Guimarães"), new Date(personBirthDate), valentinaEmail, motherID, fatherID);

        valentinaDTO = PersonAssembler.mapToDTO(valentina);

        amandaEmail = new Email("amanda@gmail.com");
        amanda = new Person(new Name("Amanda de Sousa"), new Address("Guimarães"), new Date(personBirthDate), amandaEmail, null, null);
        amanda.addSibling(brotherID);

        amandaDTO = PersonAssembler.mapToDTO(amanda);

    }

    @DisplayName("mapToDTO - Happy Path")
    @Test
    void mapToDTO() {

        PersonsResponseDTO expected = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);

        PersonsResponseDTO result = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);

        assertTrue(result.equals(expected));
    }

    @DisplayName("mapToDTO - NullParents")
    @Test
    void mapToDTONullParents() {

        PersonsResponseDTO expected = PersonsResponseDTOAssembler.mapToDTO(amandaDTO);

        PersonsResponseDTO result = PersonsResponseDTOAssembler.mapToDTO(amandaDTO);

        assertTrue(result.equals(expected));
    }

    @DisplayName("PersonsResponseDTOAssembler - ErrorException")
    @Test
    void PersonsResponseDTOAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            PersonsResponseDTOAssembler personsResponseDTOAssembler = new PersonsResponseDTOAssembler();
        });

    }
}