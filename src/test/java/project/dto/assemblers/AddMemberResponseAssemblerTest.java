package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddMemberResponseAssemblerTest {
    Description groupDescription;
    Email email;
    PersonID responsibleId;
    Group switchGroup;
    GroupDTO groupDTO;

    @BeforeEach
    void setUp() {
        groupDescription = new Description("Family");
        email = new Email("student@gmail.com");
        responsibleId = new PersonID(email);

        switchGroup = new Group(groupDescription, responsibleId);

        groupDTO = GroupAssembler.mapToDTO(switchGroup);
    }


    @Test
    void mapToGroupResponseDTO() {
        //Arrange
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        AddMemberResponseAssembler addMemberResponseAssembler = new AddMemberResponseAssembler();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO("Family", responsibles, members, categories);

        //Act
        GroupResponseDTO result = AddMemberResponseAssembler.mapToDTO("Family", responsibles, members, categories);

        //Assert
        assertEquals(groupResponseDTO, result);
    }
}