package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.PersonDTO;
import project.dto.TransactionDTO;
import project.dto.TransactionResponseDTO;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionResponseDTOAssemblerTest {
    // Arrange
    //PersonsBirthDate
    LocalDateTime birthDate;

    //Valentina
    Email valentinaEmail;
    PersonID valentinaID;
    Person valentina;
    PersonDTO valentinaDTO;

    //Transaction
    double amount;
    Description description;
    Category category;
    AccountID debit;
    AccountID credit;
    TransactionType transactionType;
    TransactionDate transactionDate;
    TransactionDTO transactionDTO;

    @BeforeEach
    void setUp() {
        // Arrange
        //Person Valentina
        birthDate = LocalDateTime.of(1988, Month.DECEMBER, 12, 18, 30, 6);
        valentinaEmail = new Email("valentina@gmail.com");
        valentinaID = new PersonID(valentinaEmail);
        valentina = new Person(new Name("Valentina de Sousa"), new Address("Guimarães"), new Date(birthDate), valentinaEmail, null, null);
        valentinaDTO = PersonAssembler.mapToDTO(valentina);

        //Transaction
        amount = 55.0;
        description = new Description("Dance School");
        category = new Category(new Designation("Dance Class"));
        debit = new AccountID(new Denomination("Wallet"), valentinaID);
        credit = new AccountID(new Denomination("School"), valentinaID);
        transactionType = TransactionType.DEBIT;
        transactionDate = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 4, 21, 30, 45));
        transactionDTO = new TransactionDTO(amount, description, transactionDate, category, debit, credit, transactionType);
    }

    @DisplayName("TransactionResponseDTOAssembler - exception")
    @Test
    void TransactionResponseDTOAssembler() throws Exception {

        assertThrows(AssertionError.class, () -> {
            TransactionResponseDTOAssembler transactionResponseDTO = new TransactionResponseDTOAssembler();
        });
    }

    @DisplayName("TransactionResponseDTOAssembler - Happy Path")
    @Test
    void TransactionResponseDTOAssemblerHappyPath() {
        //Arrange
        TransactionResponseDTO expectedResponseDTO = new TransactionResponseDTO(amount, description, transactionDate, category, debit, credit, transactionType);

        //Act
        TransactionResponseDTO resultResponseDTO = TransactionResponseDTOAssembler.mapToDTO(transactionDTO);

        //Assert
        assertTrue(resultResponseDTO.equals(expectedResponseDTO));
    }
}