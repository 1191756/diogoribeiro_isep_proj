package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.PersonDTO;
import project.dto.TransactionDTO;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionAssemblerTest {
    // Arrange
    //PersonsBirthDate
    LocalDateTime birthDate;

    //Valentina
    Email valentinaEmail;
    PersonID valentinaID;
    Person valentina;
    PersonDTO valentinaDTO;

    //Transaction
    double amount;
    Description description;
    Category category;
    AccountID debit;
    AccountID credit;
    TransactionType transactionType;
    TransactionDate transactionDate;
    Transaction transaction;

    @BeforeEach
    void setUp() {
        // Arrange
        //Person Valentina
        birthDate = LocalDateTime.of(1988, Month.DECEMBER, 12, 18, 30, 6);
        valentinaEmail = new Email("valentina@gmail.com");
        valentinaID = new PersonID(valentinaEmail);
        valentina = new Person(new Name("Valentina de Sousa"), new Address("Guimarães"), new Date(birthDate), valentinaEmail, null, null);
        valentinaDTO = PersonAssembler.mapToDTO(valentina);

        //Transaction
        amount = 55.0;
        description = new Description("Dance School");
        category = new Category(new Designation("Dance Class"));
        debit = new AccountID(new Denomination("Wallet"), valentinaID);
        credit = new AccountID(new Denomination("School"), valentinaID);
        transactionType = TransactionType.DEBIT;
        transactionDate = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 4, 21, 30, 45));
        transaction = new Transaction(amount, description, transactionDate, category, debit, credit, transactionType);
    }

    @DisplayName("TransactionAssembler - exception")
    @Test
    void TransactionResponseDTOAssembler() throws Exception {

        assertThrows(AssertionError.class, () -> {
            TransactionAssembler transactionResponseDTO = new TransactionAssembler();
        });
    }

    @DisplayName("TransactionAssembler - Happy Path")
    @Test
    void TransactionResponseDTOAssemblerHappyPath() {
        //Arrange
        TransactionDTO expectedDTO = new TransactionDTO(amount, description, transactionDate, category, debit, credit, transactionType);

        //Act
        TransactionDTO resultDTO = TransactionAssembler.mapToDTO(transaction);

        //Assert
        assertTrue(resultDTO.equals(expectedDTO));
    }
}