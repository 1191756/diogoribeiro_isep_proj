package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.AccountDTO;
import project.model.account.Account;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AccountAssemblerTest {

    @Test
    void mapToDTO() {
        AccountDTO expected = new AccountDTO();
        AccountID accountID = new AccountID(new Denomination("Groceries"), new PersonID(new Email("lia@gmail.com")));
        expected.setAccountID(accountID);
        expected.setDescription(new Description("Fruit"));

        Denomination denomination = new Denomination("Groceries");
        Description description = new Description("Fruit");
        PersonID personID = new PersonID(new Email("lia@gmail.com"));

        Account account = new Account(denomination, description, personID);
        AccountDTO result = AccountAssembler.mapToDTO(account);

        assertEquals(expected.getAccountID().getOwnerID(), result.getAccountID().getOwnerID());
        assertEquals(expected.getAccountID().getDenomination(), result.getAccountID().getDenomination());
    }

    @Test
    void accountAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AccountAssembler accountAssembler = new AccountAssembler();
        });
    }
}