package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AccountAssembler;
import project.model.account.Account;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountDTOTest {
    Denomination accountDenomination;
    Description accountDescription;
    Email personEmail;
    PersonID personId;
    Account account;
    AccountDTO accountDTO;

    @BeforeEach
    void setUp() {
        //Arrange
        accountDenomination = new Denomination("Main Bank Account");
        accountDescription = new Description("Generic account for all transactions");

        personEmail = new Email("person@gmail.com");
        personId = new PersonID(personEmail);

        account = new Account(accountDenomination, accountDescription, personId);

        accountDTO = AccountAssembler.mapToDTO(account);
    }

    @Test
    void getDescription() {
        Description result = accountDTO.getDescription();
    }

    @Test
    void setDescription() {
        accountDTO.setDescription(accountDescription);
    }

    @Test
    void getAccountID() {
        AccountID result = accountDTO.getAccountID();
    }

    @Test
    void setAccountID() {
        accountDTO.setAccountID(account.getAccountID());
    }


    @DisplayName("Override equals - AccountDTO to compare is null")
    @Test
    void equalsAmountDTOIsNull() {
        //Arrange
        AccountDTO accountDTO1 = null;

        //Act
        boolean result = accountDTO.equals(accountDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override equals - object has different class")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        Name name = new Name("Maria");

        //Act
        boolean result = accountDTO.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different description")
    @Test
    void testEqualsDifferentDescription() {
        //Arrange

        Description description2 = new Description("another account");
        Account account2 = new Account(accountDenomination, description2, personId);
        AccountDTO accountDTO2 = AccountAssembler.mapToDTO(account2);

        //Act
        boolean result = accountDTO.equals(accountDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID")
    @Test
    void testEqualsDifferentAccountID() {
        //Arrange

        PersonID personID2 = new PersonID(new Email("joanita@gmail.com"));
        Account account2 = new Account(accountDenomination, accountDescription, personID2);
        AccountDTO accountDTO2 = AccountAssembler.mapToDTO(account2);

        //Act
        boolean result = accountDTO.equals(accountDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override equals - AccountDTO same object")
    @Test
    void equalsAmountSameObject() {
        //Arrange //Act
        boolean result = accountDTO.equals(accountDTO);

        //Assert
        assertTrue(result);
    }
}
