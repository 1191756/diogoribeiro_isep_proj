package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.CategoryAssembler;
import project.model.shared.Category;
import project.model.shared.Designation;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

class CategoryDTOTest {
    Designation designation;
    Category category;
    CategoryDTO categoryDTO;

    @BeforeEach
    void setUp() {
        //Arrange
        designation = new Designation("Groceries");
        category = new Category(designation);
        categoryDTO = new CategoryDTO();

        categoryDTO = CategoryAssembler.mapToDTO(category);
    }

    @Test
    void getDesignation() {
        Designation result = categoryDTO.getDesignation();
    }

    @Test
    void setdesignation() {
        categoryDTO.setDesignation(categoryDTO.getDesignation());
    }

    @Test
    void categoryDTO() {
        CategoryDTO categoryDTO = CategoryAssembler.mapToDTO(category);
    }

    @DisplayName("Override Equals Same Object")
    @Test
    void setDesignationValidOverrideEqualsSameObject() {

        //Act
        boolean result = categoryDTO.equals(categoryDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentObjects() {
        //Arrange
        Name name = new Name("Nuno");

        //Act
        boolean result = categoryDTO.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        Designation designation1 = new Designation("Groceries");
        Category category1 = new Category(designation1);
        CategoryDTO categoryDTO1 = CategoryAssembler.mapToDTO(category1);

        int expectedResult = categoryDTO.hashCode();
        //Act
        int result = category1.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }


    @DisplayName("sameValueAs() - Category to compare is null")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        CategoryDTO categoryDTO1 = null;

        //Act
        boolean result = categoryDTO.equals(categoryDTO1);

        //Assert
        assertFalse(result);
    }
}