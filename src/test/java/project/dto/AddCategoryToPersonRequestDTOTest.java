package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Designation;
import project.model.shared.Email;
import project.model.shared.Name;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class AddCategoryToPersonRequestDTOTest {
    Designation designation;
    Email email;
    PersonID personID;
    AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTO;

    @BeforeEach
    private void setUp() {
        personID = new PersonID(new Email("robertinha@family.com"));
        designation = new Designation("Roberta's Category");
        addCategoryToPersonRequestDTO = new AddCategoryToPersonRequestDTO(designation, personID);
    }

    @Test
    void getPersonID() {
        //Act
        PersonID result = addCategoryToPersonRequestDTO.getPersonID();

        //Assert
        assertEquals(personID, result);
    }

    @Test
    void setPersonID() {
        //Arrange
        Email expectedEmail = new Email("robertafuentes@gmail.com");
        PersonID expected = new PersonID(expectedEmail);

        //Act
        addCategoryToPersonRequestDTO.setPersonID(expected);
        PersonID result = addCategoryToPersonRequestDTO.getPersonID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getDesignation() {
        //Arrange
        Designation expected = new Designation("Roberta's Category");

        //Act
        Designation result = addCategoryToPersonRequestDTO.getDesignation();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setDesignation() {
        //Arrange
        Designation expected = new Designation("Best by Roberta");

        //Act
        addCategoryToPersonRequestDTO.setDesignation(expected);
        Designation result = addCategoryToPersonRequestDTO.getDesignation();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(addCategoryToPersonRequestDTO, addCategoryToPersonRequestDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTOOther = new AddCategoryToPersonRequestDTO(designation, personID);

        //Act
        boolean result = addCategoryToPersonRequestDTO.equals(addCategoryToPersonRequestDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = addCategoryToPersonRequestDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTOOther = new AddCategoryToPersonRequestDTO(designation, personID);

        // Assert
        assertEquals(addCategoryToPersonRequestDTO.hashCode(), addCategoryToPersonRequestDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = -1168201788;
        int result = addCategoryToPersonRequestDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }
}