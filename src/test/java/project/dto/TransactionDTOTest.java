package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class TransactionDTOTest {
    TransactionDTO transactionDTO;

    Person mario;
    Email marioEmail;
    Address marioBirthPlace;
    Date marioBirthDate;
    Group group;
    PersonID personID;
    PersonID personID2;
    GroupID groupID;
    GroupID groupID2;
    double amount;
    double amount2;
    Description description;
    Description description2;
    Category category;
    Category category2;
    AccountID debit;
    AccountID debit2;
    AccountID credit;
    AccountID credit2;
    TransactionType type;
    TransactionType type2;
    TransactionDate date;
    TransactionDate date2;

    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
        personID = mario.getPersonID();
        personID2 = new PersonID(new Email("mario.family@family.com"));
        group = new Group(new Description("Family"), mario.getPersonID());
        groupID = group.getID();
        groupID2 = new GroupID(new Description("Work"));
        amount = 20.0;
        amount2 = 35.0;
        description = new Description("Lunch");
        description2 = new Description("tool");
        category = new Category(new Designation("Funny time"));
        category2 = new Category(new Designation("Pocket"));
        debit = new AccountID(new Denomination("Pocket"), groupID);
        debit2 = new AccountID(new Denomination("wallet"), groupID);
        credit = new AccountID(new Denomination("Restaurante Pinheiro"), groupID);
        credit2 = new AccountID(new Denomination("market"), groupID);
        type = TransactionType.DEBIT;
        type2 = TransactionType.CREDIT;
        date = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0));
        date2 = new TransactionDate(LocalDateTime.of(2020, Month.FEBRUARY, 1, 0, 0, 0));

        transactionDTO = new TransactionDTO(amount, description, date, category, debit, credit, type);
    }

    @DisplayName("Get Amount")
    @Test
    void getAmount() {
        double result = transactionDTO.getAmount();
    }

    @DisplayName("Set Amount")
    @Test
    void setAmount() {
        transactionDTO.setAmount(amount2);
    }

    @DisplayName("Get Description")
    @Test
    void getDescription() {
        Description result = transactionDTO.getDescription();
    }

    @DisplayName("Set Description")
    @Test
    void setDescription() {
        transactionDTO.setDescription(description2);
    }

    @DisplayName("Get Category")
    @Test
    void getCategory() {
        Category resut = transactionDTO.getCategory();
    }

    @DisplayName("Set Category")
    @Test
    void setCategory() {
        transactionDTO.setCategory(category2);
    }

    @DisplayName("Get Debit")
    @Test
    void getDebit() {
        AccountID result = transactionDTO.getDebit();
    }

    @DisplayName("Set Debit")
    @Test
    void setDebit() {
        transactionDTO.setDebit(debit2);
    }

    @DisplayName("Get Credit")
    @Test
    void getCredit() {
        AccountID result = transactionDTO.getCredit();
    }

    @DisplayName("Set Credit")
    @Test
    void setCredit() {
        transactionDTO.setCredit(credit2);
    }

    @DisplayName("Get Type")
    @Test
    void getType() {
        TransactionType result = transactionDTO.getType();
    }

    @DisplayName("Set type")
    @Test
    void setType() {
        transactionDTO.setType(type2);
    }


    @Test
    void getDate() {
        TransactionDate result = transactionDTO.getDate();
    }

    @Test
    void setDate() {
        transactionDTO.setDate(date2);
    }


    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date, category, debit, credit, type);

        int expectedResult = transactionDTO.hashCode();
        //Act
        int result = transactionDTO1.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentObjects() {
        //Arrange
        Name name = new Name("Zézinho");

        //Act
        boolean result = transactionDTO.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange

        TransactionDTO transactionDTO3;
        double amount3 = 20.0;
        Description description3 = new Description("Lunch");
        TransactionDate date3 = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0));
        Category category3 = new Category(new Designation("Funny time"));
        AccountID debit = new AccountID(new Denomination("Pocket"), groupID);
        AccountID credit = new AccountID(new Denomination("Restaurante Pinheiro"), groupID);
        TransactionType type = TransactionType.DEBIT;
        transactionDTO3 = new TransactionDTO(amount3, description3, date3, category3, debit, credit, type);

        //Act
        boolean result = transactionDTO.equals(transactionDTO3);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - same object")
    @Test
    void equalsSameObject() {
        //Arrange //Act
        boolean result = transactionDTO.equals(transactionDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Transaction to compare is null")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        TransactionDTO transactionDTO1 = null;

        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente amount")
    @Test
    void testEqualsDifferentAmount() {
        //Arrange

        TransactionDTO transactionDTO1 = new TransactionDTO(amount2, description, date, category, debit, credit, type);

        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente Description")
    @Test
    void testEqualsDifferentDescription() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description2, date, category, debit, credit, type);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente date")
    @Test
    void testEqualsDifferentDate() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date2, category, debit, credit, type);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente Category")
    @Test
    void testEqualsDifferentCategory() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date, category2, debit, credit, type);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente AccountID- debit")
    @Test
    void testEqualsDifferentAccountIDDebit() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date, category, debit2, credit, type);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente AccountID- credit")
    @Test
    void testEqualsDifferentAccountIDCredit() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date, category, debit, credit2, type);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - diferente type")
    @Test
    void testEqualsDifferentType() {
        //Arrange
        TransactionDTO transactionDTO1 = new TransactionDTO(amount, description, date, category, debit, credit, type2);
        //Act
        boolean result = transactionDTO.equals(transactionDTO1);

        //Assert
        assertFalse(result);
    }
}