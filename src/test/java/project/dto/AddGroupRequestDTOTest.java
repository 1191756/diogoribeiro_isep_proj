package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

public class AddGroupRequestDTOTest {

    @DisplayName("AddGroupRequestDTO - Coverage")
    @Test
    void AddGroupRequestDTO_CoverageTests() {
        AddGroupRequestDTO result = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        result.getPersonID();
        result.setPersonID(new PersonID(new Email("testemail@gmail.com")));
        result.getGroupDescription();
        result.setGroupDescription(new Description("TestGroup"));
        result.hashCode();
    }

    @DisplayName("AddGroupRequestDTO - Equals - same object")
    @Test
    void AddGroupRequestDTO_Equals_same_object() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        assertTrue(addGroupRequestDTO.equals(addGroupRequestDTO));
    }

    @DisplayName("AddGroupRequestDTO - Equals - Equal object")
    @Test
    void AddGroupRequestDTO_Equals_equal_object() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        assertTrue(addGroupRequestDTO.equals(addGroupRequestDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object has different class")
    @Test
    void AddGroupRequestDTO_Equals_different_class() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        Object o = new Object();
        assertFalse(addGroupRequestDTO.equals(o));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void AddGroupRequestDTO_Equals_different() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(new Description("Other Group"), new PersonID(new Email("testemail@gmail.com")));
        assertFalse(addGroupRequestDTO.equals(addGroupRequestDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void AddGroupRequestDTO_Equals_different2() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail2@gmail.com")));
        assertFalse(addGroupRequestDTO.equals(addGroupRequestDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is null")
    @Test
    void AddGroupRequestDTO_Equals_null() {
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        assertFalse(addGroupRequestDTO.equals(null));
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(new Description("Test Group2"), new PersonID(new Email("testemail@gmail.com")));
        //Assert
        assertNotEquals(addGroupRequestDTO.hashCode(), addGroupRequestDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));
        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(new Description("Test Group"), new PersonID(new Email("testemail@gmail.com")));

        //Assert
        assertEquals(addGroupRequestDTO.hashCode(), addGroupRequestDTO2.hashCode());
    }

}
