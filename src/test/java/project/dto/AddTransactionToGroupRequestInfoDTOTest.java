package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddTransactionToGroupRequestInfoDTOTest {
    AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO;

    String personID;
    String personID2;
    String groupID;
    String groupID2;
    double amount;
    double amount2;
    String description;
    String description2;
    String category;
    String category2;
    String debit;
    String debit2;
    String credit;
    String credit2;
    String type;
    String type2;

    @BeforeEach
    void setUp() {
        // Arrange
        personID = "mario@family.com";
        personID2 = "mario.family@family.com";
        groupID = "Family";
        groupID2 = "Work";
        amount = 20.0;
        amount2 = 35.0;
        description = "Lunch";
        description2 = "tool";
        category = "Funny time";
        category2 = "Pocket";
        debit = "Pocket";
        debit2 = "wallet";
        credit = "Restaurante Pinheiro";
        credit2 = "market";
        type = "DEBIT";
        type2 = "CREDIT";

        addTransactionToGroupRequestInfoDTO = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);
    }

    @DisplayName("Get PersonID")
    @Test
    void getPersonID() {
        String result = addTransactionToGroupRequestInfoDTO.getPersonID();
    }

    @DisplayName("Set PersonID")
    @Test
    void setPersonID() {
        addTransactionToGroupRequestInfoDTO.setPersonID(personID2);
    }

    @DisplayName("Get GroupID")
    @Test
    void getGroupID() {
        String result = addTransactionToGroupRequestInfoDTO.getGroupID();
    }

    @DisplayName("Set GroupID")
    @Test
    void setGroupID() {
        addTransactionToGroupRequestInfoDTO.setGroupID(groupID2);
    }

    @DisplayName("Get Amount")
    @Test
    void getAmount() {
        double result = addTransactionToGroupRequestInfoDTO.getAmount();
    }

    @DisplayName("Set Amount")
    @Test
    void setAmount() {
        addTransactionToGroupRequestInfoDTO.setAmount(amount2);
    }

    @DisplayName("Get Description")
    @Test
    void getDescription() {
        String result = addTransactionToGroupRequestInfoDTO.getDescription();
    }

    @DisplayName("Set Description")
    @Test
    void setDescription() {
        addTransactionToGroupRequestInfoDTO.setDescription(description2);
    }

    @DisplayName("Get Category")
    @Test
    void getCategory() {
        String resut = addTransactionToGroupRequestInfoDTO.getCategory();
    }

    @DisplayName("Set Category")
    @Test
    void setCategory() {
        addTransactionToGroupRequestInfoDTO.setCategory(category2);
    }

    @DisplayName("Get Debit")
    @Test
    void getDebit() {
        String result = addTransactionToGroupRequestInfoDTO.getDebit();
    }

    @DisplayName("Set Debit")
    @Test
    void setDebit() {
        addTransactionToGroupRequestInfoDTO.setDebit(debit2);
    }

    @DisplayName("Get Credit")
    @Test
    void getCredit() {
        String result = addTransactionToGroupRequestInfoDTO.getCredit();
    }

    @DisplayName("Set Credit")
    @Test
    void setCredit() {
        addTransactionToGroupRequestInfoDTO.setCredit(credit2);
    }

    @DisplayName("Get Type")
    @Test
    void getType() {
        String result = addTransactionToGroupRequestInfoDTO.getType();
    }

    @DisplayName("Set type")
    @Test
    void setType() {
        addTransactionToGroupRequestInfoDTO.setType(type2);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEquals() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO1.equals(addTransactionToGroupRequestInfoDTO2);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Same Exact Object")
    @Test
    void testEqualsSameExactObject() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO1.equals(addTransactionToGroupRequestInfoDTO1);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Different")
    @Test
    void testEqualsDifferent() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID2, groupID2, amount2, description2, category2, debit2, credit2, type2);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO1.equals(addTransactionToGroupRequestInfoDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalse() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO1.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalseAnotherObject() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO1.equals(personID);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode1() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID2, groupID2, amount2, description2, category2, debit2, credit2, type2);

        //Assert
        assertNotEquals(addTransactionToGroupRequestInfoDTO1.hashCode(), addTransactionToGroupRequestInfoDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO1 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Assert
        assertEquals(addTransactionToGroupRequestInfoDTO1.hashCode(), addTransactionToGroupRequestInfoDTO2.hashCode());
    }

    @DisplayName("Override Equals - False - different GroupID")
    @Test
    void testEqualsDifferentGroupID() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID2, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Amount")
    @Test
    void testEqualsDifferentAmount() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount2, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Description")
    @Test
    void testEqualsDifferentDescription() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description2, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Category")
    @Test
    void testEqualsDifferentCategory() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category2, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID debit")
    @Test
    void testEqualsDifferentAcoountIDDebit() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit2, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID credit")
    @Test
    void testEqualsDifferentAcoountIDCredit() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit2, type);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different type")
    @Test
    void testEqualsDifferentType() {
        //Arrange
        AddTransactionToGroupRequestInfoDTO addTransactionToGroupRequestInfoDTO2 = new AddTransactionToGroupRequestInfoDTO(personID, groupID, amount, description, category, debit, credit, type2);

        //Act
        boolean result = addTransactionToGroupRequestInfoDTO.equals(addTransactionToGroupRequestInfoDTO2);

        //
        assertFalse(result);
    }
}