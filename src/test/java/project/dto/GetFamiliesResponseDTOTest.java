package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class GetFamiliesResponseDTOTest {
    GetFamiliesResponseDTO getFamiliesResponseDTO;

    @BeforeEach
    void setUp() {
        getFamiliesResponseDTO = new GetFamiliesResponseDTO("Family");
    }

    @Test
    void getFamily() {
        String family = getFamiliesResponseDTO.getGroup();
    }

    @Test
    void setFamily() {
        getFamiliesResponseDTO.setGroup("Family");
    }

    @Test
    void testEquals() {
        GetFamiliesResponseDTO getFamiliesResponseTestDTO;
        getFamiliesResponseTestDTO = new GetFamiliesResponseDTO("Family");

        assertEquals(getFamiliesResponseDTO, getFamiliesResponseTestDTO);
    }

    @Test
    void testEqualsSameObject() {
        assertEquals(getFamiliesResponseDTO, getFamiliesResponseDTO);
    }

    @Test
    void testEqualsDifferentClassObject() {
        Description description = new Description("Beautiful thing");
        //Act
        boolean result = getFamiliesResponseDTO.equals(description);

        // Assert
        assertFalse(result);
    }

    @Test
    void testEqualsNullObject() {
        //Act
        boolean result = getFamiliesResponseDTO.equals(null);

        // Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        GetFamiliesResponseDTO getFamiliesResponseTestDTO;
        getFamiliesResponseTestDTO = new GetFamiliesResponseDTO("Family");

        assertEquals(getFamiliesResponseDTO.hashCode(), getFamiliesResponseTestDTO.hashCode());
    }

    @Test
    void testToString() {
        assertEquals("Family", getFamiliesResponseDTO.toString());
    }
}