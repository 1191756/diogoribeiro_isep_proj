package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import static org.junit.jupiter.api.Assertions.*;

class AddAccountToGroupRequestInfoDTOTest {
    String denomination;
    String description;
    String groupDescription;
    String responsibleEmail;
    AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO;

    @BeforeEach
    void setUp() {
        denomination = "Groceries";
        description = "Fruit";
        groupDescription = "Shopping";
        responsibleEmail = "lia@gmail.com";
        addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);
    }

    @Test
    void getDenomination() {
        String denomination = addAccountToGroupRequestInfoDTO.getDenomination();
    }

    @Test
    void setDenomination() {
        addAccountToGroupRequestInfoDTO.setDenomination(addAccountToGroupRequestInfoDTO.getDenomination());
    }

    @Test
    void getDescription() {
        String description = addAccountToGroupRequestInfoDTO.getDescription();
    }

    @Test
    void setDescription() {
        addAccountToGroupRequestInfoDTO.setDescription(addAccountToGroupRequestInfoDTO.getDescription());
    }

    @Test
    void getResponsibleEmail() {
        String responsibleEmail = addAccountToGroupRequestInfoDTO.getResponsibleEmail();
    }

    @Test
    void setResponsibleEmail() {
        addAccountToGroupRequestInfoDTO.setResponsibleEmail(addAccountToGroupRequestInfoDTO.getResponsibleEmail());
    }

    @Test
    void testEquals() {
        //Arrange
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO("Groceries", "lia@gmail.com", "Fruit");
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTOOther = new AddAccountToGroupRequestInfoDTO("Groceries", "lia@gmail.com", "Fruit");
        // Act
        boolean result = addAccountToGroupRequestInfoDTO.equals(addAccountToGroupRequestInfoDTOOther);
        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNull() {
        //Arrage
        AddAccountToGroupRequestInfoDTO nullAccountToGroupRequestInfoDTO = null;
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO("Groceries", "lia@gmail.com", "Fruit");
        // Act
        boolean result = addAccountToGroupRequestInfoDTO.equals(nullAccountToGroupRequestInfoDTO);
        //Assert
        assertFalse(result);
    }

    @DisplayName(" test Equals - diferent objects")
    @Test
    void testEqualsDifferentObject() {
        //Arrage
        Description otherAccountToGroupRequestInfoDTO = new Description("Shoes");
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO("Groceries", "lia@gmail.com", "Fruit");
        // Act
        boolean result = addAccountToGroupRequestInfoDTO.equals(otherAccountToGroupRequestInfoDTO);
        //Assert
        assertFalse(result);
    }

    @DisplayName("test for HashCode")
    @Test
    void testHashCode() {

        AddAccountToGroupRequestInfoDTO expect = new AddAccountToGroupRequestInfoDTO("Groceries", "Fruit", "lia@gmail.com");

        int expectHash = expect.hashCode();
        int resultHash = addAccountToGroupRequestInfoDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @DisplayName("test for HashCode with fixed value")
    @Test
    void testHashCodeWithFixedValue() {
        int expectHash = -1708495381;

        int resultHash = addAccountToGroupRequestInfoDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @DisplayName("test Equals - denomination")
    @Test
    void testEqualsDifferentObjectsDenomination() {
        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String groupDescription = "Girls Club";
        String responsibleEmail = "marta@gmail.com";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO1 = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);
        //Act
        String denomination2 = "Cub";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO2 = new AddAccountToGroupRequestInfoDTO(denomination2, description, responsibleEmail);

        //Assert
        assertFalse(addAccountToGroupRequestInfoDTO1.equals(addAccountToGroupRequestInfoDTO2));
    }

    @DisplayName("test Equals - description")
    @Test
    void testEqualsDifferentObjectsDescription() {

        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String responsibleEmail = "marta@gmail.com";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO1 = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);
        //Act
        String description2 = "Shoes";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO2 = new AddAccountToGroupRequestInfoDTO(denomination, description2, responsibleEmail);

        //Assert
        assertFalse(addAccountToGroupRequestInfoDTO1.equals(addAccountToGroupRequestInfoDTO2));
    }

    @DisplayName("test Equals - responsible Email")
    @Test
    void testEqualsDifferentObjectsResponsibleEmail() {

        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String responsibleEmail = "marta@gmail.com";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO1 = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);
        //Act
        String responsibleEmail2 = "manuel@gmail.com";
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO2 = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail2);

        //Assert
        assertFalse(addAccountToGroupRequestInfoDTO1.equals(addAccountToGroupRequestInfoDTO2));
    }

    @Test
    void testEqualsSameObject() {
        //Arrange
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO("Groceries", "lia@gmail.com", "Fruit");
        // Act
        boolean result = addAccountToGroupRequestInfoDTO.equals(addAccountToGroupRequestInfoDTO);
        //Assert
        assertTrue(result);
    }
}