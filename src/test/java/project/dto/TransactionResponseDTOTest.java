package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class TransactionResponseDTOTest {

    TransactionResponseDTO transactionResponseDTO;

    GroupID groupID1;
    GroupID groupID2;
    double amount1;
    double amount2;
    Description description1;
    Description description2;
    Category category1;
    Category category2;
    AccountID debit1;
    AccountID debit2;
    AccountID credit1;
    AccountID credit2;
    TransactionType type1;
    TransactionType type2;
    TransactionDate date1;
    TransactionDate date2;


    @BeforeEach
    void setUp() {
        // Arrange
        groupID1 = new GroupID(new Description("High School"));
        groupID2 = new GroupID(new Description("University"));
        amount1 = 50.0;
        amount2 = 190.0;
        description1 = new Description("Teenagers");
        description2 = new Description("Adults");
        category1 = new Category(new Designation("Classes"));
        category2 = new Category(new Designation("Sport"));
        debit1 = new AccountID(new Denomination("Banc"), groupID1);
        debit2 = new AccountID(new Denomination("wallet"), groupID2);
        credit1 = new AccountID(new Denomination("coins"), groupID1);
        credit2 = new AccountID(new Denomination("notes "), groupID2);
        type1 = TransactionType.DEBIT;
        type2 = TransactionType.CREDIT;
        date1 = new TransactionDate(LocalDateTime.of(2000, Month.JANUARY, 4, 21, 30, 45));
        date2 = new TransactionDate(LocalDateTime.of(2020, Month.FEBRUARY, 7, 8, 15, 6));

        transactionResponseDTO = new TransactionResponseDTO(amount1, description1, date1, category1, debit1, credit1, type1);
    }

    @Test
    void getAmount() {
        Double result = transactionResponseDTO.getAmount();

        assertEquals(amount1, result);
    }

    @Test
    void setAmount() {
        transactionResponseDTO.setAmount(amount1);
    }

    @Test
    void getDescription() {
        String result = transactionResponseDTO.getDescription();

        assertEquals("Teenagers", result);
    }

    @Test
    void setDescription() {
        transactionResponseDTO.setDescription(description1);
    }

    @Test
    void getDate() {
        String result = transactionResponseDTO.getDate();

        assertEquals("2000-01-04", result.substring(0, 10));
    }

    @Test
    void getCategory() {
        String result = transactionResponseDTO.getCategory();

        assertEquals("Classes", result);
    }

    @Test
    void setCategory() {
        transactionResponseDTO.setCategory(category1);
    }

    @Test
    void getAccountIDDebit() {
        String result = transactionResponseDTO.getDebit();

        assertEquals("Banc", result);
    }

    @Test
    void setAccountIDDebit() {
        transactionResponseDTO.setDebit(debit1);
    }

    @Test
    void getAccountIDCredit() {
        String result = transactionResponseDTO.getCredit();

        assertEquals("coins", result);
    }

    @Test
    void setAccountIDCredit() {
        transactionResponseDTO.setCredit(credit1);
    }

    @Test
    void getType() {
        String result = transactionResponseDTO.getType();

        assertEquals("DEBIT", result);
    }

    @Test
    void setType() {
        transactionResponseDTO.setType(type1);
    }

    @Test
    void toStringTest() {
        String result = transactionResponseDTO.toString();

        assertEquals("TransactionResponseDTO{amount=50.0, description=Teenagers, date=2000-01-04T21:30:45, category=Classes, debit=Banc, credit=coins, type=DEBIT}", result);
    }

    @DisplayName("sameValueAs() - TransactionResponseDTO to compare is null")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO3 = null;

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO3);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentObjects() {
        //Arrange
        Name name = new Name("Zézinho");

        //Act
        boolean result = transactionResponseDTO.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Hash Code Override test - Not Equals")
    @Test
    void hashCodeTestNotEquals() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount2, description2, date2, category2, debit2, credit2, type2);
        int expected = transactionResponseDTO2.hashCode();

        //Act
        int result = transactionResponseDTO.hashCode();

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("Equals - Exact same object")
    @Test
    void TestEqualsExactSameObject() {
        //Assert
        assertTrue(transactionResponseDTO.equals(transactionResponseDTO));
    }

    @DisplayName("Override Equals - False - different amount")
    @Test
    void testEqualsDifferentAmount() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount2, description1, date1, category1, debit1, credit1, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Description")
    @Test
    void testEqualsDifferentDescription() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description2, date1, category1, debit1, credit1, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different date")
    @Test
    void testEqualsDifferentDate() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description1, date2, category1, debit1, credit1, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different category")
    @Test
    void testEqualsDifferentCategory() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description1, date1, category2, debit1, credit1, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID Debit")
    @Test
    void testEqualsDifferentAccountIDDebit() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description1, date1, category1, debit2, credit1, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID Credit")
    @Test
    void testEqualsDifferentAccountIDCredit() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description1, date1, category1, debit1, credit2, type1);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Type")
    @Test
    void testEqualsDifferentType() {
        //Arrange
        TransactionResponseDTO transactionResponseDTO2 = new TransactionResponseDTO(amount1, description1, date1, category1, debit1, credit1, type2);

        //Act
        boolean result = transactionResponseDTO.equals(transactionResponseDTO2);

        //Assert
        assertFalse(result);
    }
}
