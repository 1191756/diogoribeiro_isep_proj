package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.GroupAssembler;
import project.model.group.Group;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GroupDTOTest {
    Description groupDescription;
    Email email;
    PersonID responsibleId;
    Group switchGroup;
    GroupDTO groupDTO;
    GroupDTO groupDTO2;

    @BeforeEach
    void setUp() {
        groupDescription = new Description("Switch Group");
        email = new Email("student@gmail.com");
        responsibleId = new PersonID(email);

        switchGroup = new Group(groupDescription, responsibleId);

        groupDTO = GroupAssembler.mapToDTO(switchGroup);
        groupDTO2 = GroupAssembler.mapToDTO(switchGroup);
    }

    @Test
    void getGroupID() {
        groupDTO.getGroupID();
    }

    @Test
    void setGroupID() {
        groupDTO.setGroupID(groupDTO.getGroupID());
    }

    @Test
    void getResponsibles() {
        groupDTO.getResponsibles();
    }

    @Test
    void setResponsibles() {
        groupDTO.setResponsibles(groupDTO.getResponsibles());
    }

    @Test
    void getMembers() {
        groupDTO.getMembers();
    }

    @Test
    void setMembers() {
        groupDTO.setMembers(groupDTO.getMembers());
    }

    @Test
    void getCategories() {
        groupDTO.getCategories();
    }

    @Test
    void setCategories() {
        groupDTO.setCategories(groupDTO.getCategories());
    }

    @Test
    void getCreationDate() {
        Date result = groupDTO.getCreationDate();

        assertEquals(result.toString(), new Date(LocalDateTime.now()).toString());
    }

    @Test
    void setCreationDate() {
        groupDTO.setCreationDate(groupDTO.getCreationDate());
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEquals() {
        //Act
        boolean result = groupDTO.equals(groupDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Different")
    @Test
    void testEqualsDifferent() {
        //Arrange
        Description groupDescription2 = new Description("ABC Group");
        Email email2 = new Email("abc@gmail.com");
        PersonID responsibleId2 = new PersonID(email2);

        Group group = new Group(groupDescription2, responsibleId2);

        GroupDTO groupDTO2 = GroupAssembler.mapToDTO(group);

        //Act
        boolean result = groupDTO.equals(groupDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - Different type")
    @Test
    void testEqualsDifferentType() {
        //Arrange
        Description groupDescription2 = new Description("ABC Group");
        Email email2 = new Email("abc@gmail.com");
        PersonID responsibleId2 = new PersonID(email2);

        Group group = new Group(groupDescription2, responsibleId2);

        GroupDTO groupDTO2 = GroupAssembler.mapToDTO(group);

        //Act
        boolean result = groupDTO.equals(email2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalse() {
        //Act
        boolean result = groupDTO.equals(null);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalseAnotherObject() {
        //Act
        boolean result = groupDTO.equals(groupDescription);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEqualsAttributes() {
        //Act
        boolean result = groupDTO.equals(groupDTO2);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsDifferentAttributes() {
        //Arrange
        Email email = new Email("pedro@gmail.com");
        PersonID responsibleId = new PersonID(email);
        Set<PersonID> responsible = new HashSet<>();
        responsible.add(responsibleId);
        Set<PersonID> members = new HashSet<>();
        members.add(responsibleId);
        groupDTO2.setResponsibles(responsible);
        groupDTO2.setMembers(members);

        //Act
        boolean result = groupDTO.equals(groupDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - Different Categories False")
    @Test
    void testEqualsDifferentAttributesCategoy() {
        //Arrange
        Category category = new Category(new Designation("Mercado"));
        Set<Category> categories = new HashSet<>();
        categories.add(category);
        groupDTO2.setCategories(categories);
        Category category2 = new Category(new Designation("Farmácia"));
        Set<Category> categories2 = new HashSet<>();
        categories2.add(category2);
        groupDTO.setCategories(categories2);

        //Act
        boolean result = groupDTO.equals(groupDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsDifferentAttributesDate() {
        //Arrange
        groupDTO2.setCreationDate(new Date(LocalDateTime.of(2019, Month.JANUARY, 1, 0, 0, 0)));
        //Act
        boolean result = groupDTO.equals(groupDTO2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - Different Members False")
    @Test
    void testEqualsDifferentMembersCategoy() {
        //Arrange
        Email email = new Email("rafaelbergundi@gmail.com");
        PersonID responsibleId = new PersonID(email);
        Set<PersonID> members = new HashSet<>();
        members.add(responsibleId);
        groupDTO2.setMembers(members);

        //Act
        boolean result = groupDTO.equals(groupDTO2);

        //Assert
        assertFalse(result);
    }
}