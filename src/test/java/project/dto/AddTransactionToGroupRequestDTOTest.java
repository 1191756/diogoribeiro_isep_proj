package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class AddTransactionToGroupRequestDTOTest {
    AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO;
    Person mario;
    Email marioEmail;
    Address marioBirthPlace;
    Date marioBirthDate;
    Group group;
    PersonID personID;
    PersonID personID2;
    GroupID groupID;
    GroupID groupID2;
    double amount;
    double amount2;
    Description description;
    Description description2;
    Category category;
    Category category2;
    AccountID debit;
    AccountID debit2;
    AccountID credit;
    AccountID credit2;
    TransactionType type;
    TransactionType type2;

    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
        personID = mario.getPersonID();
        personID2 = new PersonID(new Email("mario.family@family.com"));
        group = new Group(new Description("Family"), mario.getPersonID());
        groupID = group.getID();
        groupID2 = new GroupID(new Description("Work"));
        amount = 20.0;
        amount2 = 35.0;
        description = new Description("Lunch");
        description2 = new Description("tool");
        category = new Category(new Designation("Funny time"));
        category2 = new Category(new Designation("Pocket"));
        debit = new AccountID(new Denomination("Pocket"), groupID);
        debit2 = new AccountID(new Denomination("wallet"), groupID);
        credit = new AccountID(new Denomination("Restaurante Pinheiro"), groupID);
        credit2 = new AccountID(new Denomination("market"), groupID);
        type = TransactionType.DEBIT;
        type2 = TransactionType.CREDIT;

        addTransactionToGroupRequestDTO = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

    }

    @DisplayName("Get PersonID")
    @Test
    void getPersonID() {
        PersonID result = addTransactionToGroupRequestDTO.getPersonID();
    }

    @DisplayName("Set PersonID")
    @Test
    void setPersonID() {
        addTransactionToGroupRequestDTO.setPersonID(personID2);
    }

    @DisplayName("Get GroupID")
    @Test
    void getGroupID() {
        GroupID result = addTransactionToGroupRequestDTO.getGroupID();
    }

    @DisplayName("Set GroupID")
    @Test
    void setGroupID() {
        addTransactionToGroupRequestDTO.setGroupID(groupID2);
    }

    @DisplayName("Get Amount")
    @Test
    void getAmount() {
        double result = addTransactionToGroupRequestDTO.getAmount();
    }

    @DisplayName("Set Amount")
    @Test
    void setAmount() {
        addTransactionToGroupRequestDTO.setAmount(amount2);
    }

    @DisplayName("Get Description")
    @Test
    void getDescription() {
        Description result = addTransactionToGroupRequestDTO.getDescription();
    }

    @DisplayName("Set Description")
    @Test
    void setDescription() {
        addTransactionToGroupRequestDTO.setDescription(description2);
    }

    @DisplayName("Get Category")
    @Test
    void getCategory() {
        Category resut = addTransactionToGroupRequestDTO.getCategory();
    }

    @DisplayName("Set Category")
    @Test
    void setCategory() {
        addTransactionToGroupRequestDTO.setCategory(category2);
    }

    @DisplayName("Get Debit")
    @Test
    void getDebit() {
        AccountID result = addTransactionToGroupRequestDTO.getDebit();
    }

    @DisplayName("Set Debit")
    @Test
    void setDebit() {
        addTransactionToGroupRequestDTO.setDebit(debit2);
    }

    @DisplayName("Get Credit")
    @Test
    void getCredit() {
        AccountID result = addTransactionToGroupRequestDTO.getCredit();
    }

    @DisplayName("Set Credit")
    @Test
    void setCredit() {
        addTransactionToGroupRequestDTO.setCredit(credit2);
    }

    @DisplayName("Get Type")
    @Test
    void getType() {
        TransactionType result = addTransactionToGroupRequestDTO.getType();
    }

    @DisplayName("Set type")
    @Test
    void setType() {
        addTransactionToGroupRequestDTO.setType(type2);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEquals() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO1.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Same Exact Object")
    @Test
    void testEqualsSameExactObject() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO1.equals(addTransactionToGroupRequestDTO1);

        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Different")
    @Test
    void testEqualsDifferent() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID2, groupID2, amount2, description2, category2, debit2, credit2, type2);

        //Act
        boolean result = addTransactionToGroupRequestDTO1.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalse() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO1.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalseAnotherObject() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO1.equals(personID);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode1() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID2, groupID2, amount2, description2, category2, debit2, credit2, type2);

        //Assert
        assertNotEquals(addTransactionToGroupRequestDTO1.hashCode(), addTransactionToGroupRequestDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO1 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type);

        //Assert
        assertEquals(addTransactionToGroupRequestDTO1.hashCode(), addTransactionToGroupRequestDTO2.hashCode());
    }

    @DisplayName("Override Equals - False - different GroupID")
    @Test
    void testEqualsDifferentGroupID() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID2, amount, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different amount")
    @Test
    void testEqualsDifferentAmount() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount2, description, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different description")
    @Test
    void testEqualsDifferentDescription() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description2, category, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different Category")
    @Test
    void testEqualsDifferentCategory() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category2, debit, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID Debit")
    @Test
    void testEqualsDifferentAccountIDDebit() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit2, credit, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different AccountID Credit")
    @Test
    void testEqualsDifferentAccountIDCredit() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit2, type);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False - different type")
    @Test
    void testEqualsDifferentType() {
        //Arrange
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO2 = new AddTransactionToGroupRequestDTO(personID, groupID, amount, description, category, debit, credit, type2);

        //Act
        boolean result = addTransactionToGroupRequestDTO.equals(addTransactionToGroupRequestDTO2);

        //Assert
        assertFalse(result);
    }
}
