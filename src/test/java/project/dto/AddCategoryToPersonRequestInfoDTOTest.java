package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

class AddCategoryToPersonRequestInfoDTOTest {
    String designation;
    String personID;
    AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTO;

    @BeforeEach
    private void setUp() {
        personID = "robertinha@family.com";
        designation = "Roberta's Category";
        addCategoryToPersonRequestInfoDTO = new AddCategoryToPersonRequestInfoDTO(designation, personID);
    }

    @Test
    void getPersonID() {
        //Act
        String result = addCategoryToPersonRequestInfoDTO.getPersonID();

        //Assert
        assertEquals(personID, result);
    }

    @Test
    void setPersonID() {
        //Arrange
        String expected = "robertafuentes@gmail.com";

        //Act
        addCategoryToPersonRequestInfoDTO.setPersonID(expected);
        String result = addCategoryToPersonRequestInfoDTO.getPersonID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getDesignation() {
        //Arrange
        String expected = "Roberta's Category";

        //Act
        String result = addCategoryToPersonRequestInfoDTO.getDesignation();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setDesignation() {
        //Arrange
        String expected = "Best by Roberta";

        //Act
        addCategoryToPersonRequestInfoDTO.setDesignation(expected);
        String result = addCategoryToPersonRequestInfoDTO.getDesignation();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(addCategoryToPersonRequestInfoDTO, addCategoryToPersonRequestInfoDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTOOther = new AddCategoryToPersonRequestInfoDTO(designation, personID);

        //Act
        boolean result = addCategoryToPersonRequestInfoDTO.equals(addCategoryToPersonRequestInfoDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = addCategoryToPersonRequestInfoDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTOOther = new AddCategoryToPersonRequestInfoDTO(designation, personID);

        // Assert
        assertEquals(addCategoryToPersonRequestInfoDTO.hashCode(), addCategoryToPersonRequestInfoDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = -1168202811;
        int result = addCategoryToPersonRequestInfoDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }
}