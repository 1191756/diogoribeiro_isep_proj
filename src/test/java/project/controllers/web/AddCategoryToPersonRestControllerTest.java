package project.controllers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddCategoryToPersonRequestDTO;
import project.dto.AddCategoryToPersonRequestInfoDTO;
import project.dto.assemblers.AddCategoryToPersonRequestAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.PersonRepository;
import project.services.AddCategoryToPersonService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddCategoryToPersonRestControllerTest {
    Designation designation;
    Person p1;
    LocalDateTime birthDate;
    @Autowired
    private AddCategoryToPersonRestController addCategoryToPersonRestController;
    @Autowired
    private AddCategoryToPersonService addCategoryToPersonMockService;
    @Autowired
    private PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        designation = new Designation("Bike repairs");
        birthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        p1 = new Person(new Name("Rui Machado"), new Address("Porto"), new Date(birthDate), new Email("ruimachado@gmail.com"), null, null);
        personRepository.save(p1);
    }

    @DisplayName("addCategoryToPerson - Person ID not found.")
    @Test
    void addCategoryToPersonNoGroupIDFound() {

        //Arrange
        Designation designation = new Designation("Football Items");
        Description groupDescription = new Description("Sport Department");
        Email responsibleEmail = new Email("antonio@sportsdirect.com");

        AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTO = AddCategoryToPersonRequestAssembler.mapToDTO(designation.getDesignation(), "antonio@sportsdirect.com");

        Mockito.when(addCategoryToPersonMockService.addCategoryToPerson(addCategoryToPersonRequestDTO)).thenThrow(new NotFoundException("PersonID does not exist"));

        //Act
        AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTO = new AddCategoryToPersonRequestInfoDTO("Football Items", "antonio@sportsdirect.com");
        Exception exception = assertThrows(NotFoundException.class, () -> addCategoryToPersonRestController.addCategoryToPerson(addCategoryToPersonRequestInfoDTO, "antonio@sportsdirect.com"));

        //Assert
        assertEquals("PersonID does not exist", exception.getMessage());
    }
}