package project.controllers.web.persistence;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.datamodel.GroupIDData;
import project.dto.AddGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.repositories.jpa.GroupJPARepository;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddGroupRestControllerInt extends AbstractTest {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupJPARepository groupJPARepository;
    @DisplayName("As a user, I want to create a group, becoming a group administrator -add Group - Happy path ")
    @Test
    void addGroupHappyPath() throws Exception {

        //Arrange
        final String uri = "/groups";
        final String groupDescription = "Mega Test Group";
        final String responsibleEmail = "mariafanecas@gmail.com";
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);

        //Act
        assertFalse(groupRepository.findById(new GroupID(new Description("Mega Test Group"))).isPresent());
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(entryJson);
        String groupDescription1 = design.read("$.groupDescription");

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        assertEquals("Mega Test Group", groupDescription1);

        //Find
        assertTrue(groupRepository.findById(new GroupID(new Description("Mega Test Group"))).isPresent());

        //Delete
        GroupIDData groupIDData = new GroupIDData(new GroupID(new Description("Mega Test Group")).toString());
        groupJPARepository.deleteById(groupIDData);
        assertFalse(groupRepository.findById(new GroupID(new Description("Mega Test Group"))).isPresent());

    }
    @DisplayName("/groups - Assert that a specific exception is thrown when the group already exists")
    @Test
    void addGroup_ExistingGroup() throws Exception {

        //Arrange
        Description description = new Description("Group2");
        Email email = new Email("mariafanecas@gmail.com");
        PersonID responsible = new PersonID(email);
        Group group2 = new Group(description, responsible);
        groupRepository.save(group2);
        final String uri = "/groups";
        final String groupDescription = "Group2";
        final String responsibleEmail = "mariafanecas@gmail.com";
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);
        assertEquals("Please select an nonexistent Group", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}