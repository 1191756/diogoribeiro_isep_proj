package project.controllers.web.persistence;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.datamodel.assemblers.GroupDomainDataAssembler;
import project.dto.AddAccountToGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.jpa.AccountJPARepository;
import project.repositories.jpa.GroupJPARepository;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddAccountToGroupRestControllerInt extends AbstractTest {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountJPARepository accountJPARepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupJPARepository groupJPARepository;

    //BeforeEach
    AddAccountToGroupRequestInfoDTO infoDTO;
    Person person;
    PersonID responsibleID;
    Group group;
    Description groupDescription;
    Email email;

    @BeforeEach
    void setUpForTests() {

        //Person1
        email = new Email("maria@family.com");
        responsibleID = new PersonID(email);


        //Group
        groupDescription = new Description("SportGroup");
        group = new Group(groupDescription, responsibleID);
    }

    @DisplayName("addAccountToGroup - Happy path ")
    @Test
    void addAccountToGroupHappyPath() throws Exception {
        //Arrange
        final String uri = "/groups/SportGroup/accounts";
        final String accountDenomination = "Tennis Account";
        final String accountDescription = "racket";
        final String responsibleID = "maria@family.com";

        assertFalse(groupRepository.findById(group.getID()).isPresent());
        groupRepository.save(group);
        assertFalse(accountRepository.findById(new AccountID(new Denomination(accountDenomination), group.getID())).isPresent());

        //New Account
        //Account newAccount = new Account(new Denomination(accountDenomination), new Description(accountDescription), group.getID());

        infoDTO = new AddAccountToGroupRequestInfoDTO(accountDenomination, accountDescription, responsibleID);

        //Act
        String entryJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String denomination = design.read("$.denomination");
        String description = design.read("$.description");
        String ownerID = design.read("$.ownerID");

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        assertEquals("Tennis Account", denomination);
        assertEquals("racket", description);
        assertEquals("SportGroup", ownerID);

        //Assert persistence
        assertTrue(accountRepository.findById(new AccountID(new Denomination("Tennis Account"), group.getID())).isPresent());

        //Delete saved entities
        AccountID accountID = new AccountID(new Denomination("Tennis Account"), group.getID());
        accountJPARepository.deleteById(accountID);

        groupJPARepository.delete(GroupDomainDataAssembler.toData(group));
        assertFalse(groupRepository.findById(group.getID()).isPresent());
    }
}