package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddGroupRequestDTO;
import project.dto.AddGroupRequestInfoDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.model.group.Group;
import project.model.shared.*;
import project.services.AddGroupService;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddGroupRestControllerTest {

    Email email;
    Description description;
    PersonID responsible;
    @Autowired
    private AddGroupService addGroupMockService;
    @Autowired
    private AddGroupRestController addGroupRestController;

    @DisplayName("addGroup() - Happy path")
    @Test
    void addGroupHappyPath() {
        //Arrange
        description = new Description("Happy Group2");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        // Categories
        Category categoryAlface = new Category(new Designation("Alface"));
        Category categoryTomate = new Category(new Designation("Tomate"));

        Group group1 = new Group(description, responsible);
        group1.addCategory(categoryAlface);
        group1.addCategory(categoryTomate);

        GroupDTO expected = GroupAssembler.mapToDTO(group1);

        AddGroupRequestDTO addGroupRequestDTO = new AddGroupRequestDTO(description, responsible);
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(description.getDescription(), email.getEmail());

        Mockito.when(addGroupMockService.addGroup(addGroupRequestDTO)).thenReturn(expected);
        //Act
        ResponseEntity<Object> result = addGroupRestController.addGroup(addGroupRequestInfoDTO);

        //Assert
        assertEquals(201, result.getStatusCodeValue());
    }
}
