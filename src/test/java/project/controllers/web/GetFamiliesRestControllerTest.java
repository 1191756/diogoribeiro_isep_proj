package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetFamiliesService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetFamiliesRestControllerTest {
    @Autowired
    private GetFamiliesService getFamiliesMockService;
    @Autowired
    private GetFamiliesRestController getFamiliesRestController;

    @DisplayName("getFamilies - Happy Path")
    @Test
    public void getFamiliesHappyPath() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariagarrida@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("josecalame@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisioborges@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunoagostini@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        // GROUPS
        Description familyDescription = new Description("Family of Mafas");
        Group familyGroup = new Group(familyDescription, mother.getPersonID());
        familyGroup.addMember(father.getPersonID());
        familyGroup.addMember(son.getPersonID());
        familyGroup.addMember(sibling.getPersonID());

        GroupDTO familyDTO = GroupAssembler.mapToDTO(familyGroup);
        Set<GroupDTO> familiesDTOCollection = new HashSet<>();
        familiesDTOCollection.add(familyDTO);

        Mockito.when(getFamiliesMockService.getFamilies()).thenReturn(familiesDTOCollection);

        //Act
        ResponseEntity<Object> result = getFamiliesRestController.getFamilies("type=families");

        assertEquals(200, result.getStatusCodeValue());
    }
}