package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GroupDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GroupsService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GroupsRestControllerTest {
    @Autowired
    private GroupsService groupsMockService;
    @Autowired
    private GroupsRestController groupsRestController;

    @DisplayName("getGroups - Get a person's groups")
    @Test
    void getAllGroups() {
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Date familyDate = new Date(LocalDateTime.of(2018, 1, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        PersonID mariaID = new PersonID(mariaEmail);
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        PersonID joseID = new PersonID(joseEmail);
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        PersonID tarcisioID = new PersonID(tarcisioEmail);
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());
        son.addSibling(sibling.getPersonID());

        // GROUP
        Description groupDescription = new Description("Family3");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();

        // Categories
        Category categoryAlface = new Category(new Designation("Alface"));
        Category categoryTomate = new Category(new Designation("Tomate"));

        //Arrange
        Set<PersonID> responsibles = new HashSet<>();
        responsibles.add(joseID);

        Set<PersonID> members = new HashSet<>();
        members.add(joseID);
        members.add(tarcisioID);
        members.add(mariaID);

        Set<Category> categories = new HashSet<>();
        categories.add(categoryAlface);
        categories.add(categoryTomate);

        GroupDTO familyDTO = new GroupDTO();
        familyDTO.setGroupID(familyGroupID);
        familyDTO.setResponsibles(responsibles);
        familyDTO.setMembers(members);
        familyDTO.setCategories(categories);
        familyDTO.setCreationDate(familyDate);

        Set<GroupDTO> groupsSetDTO = new HashSet<>();
        groupsSetDTO.add(familyDTO);

        Mockito.when(groupsMockService.getGroups(tarcisioID)).thenReturn(groupsSetDTO);

        //Act
        ResponseEntity<Object> result = groupsRestController.getGroups(tarcisioID.toString());

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }

    @DisplayName("getGroups - Get a group")
    @Test
    void getGroup() {
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Date familyDate = new Date(LocalDateTime.of(2018, 1, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        PersonID mariaID = new PersonID(mariaEmail);
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        PersonID joseID = new PersonID(joseEmail);
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        PersonID tarcisioID = new PersonID(tarcisioEmail);
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());
        son.addSibling(sibling.getPersonID());

        // GROUP
        Description groupDescription = new Description("Family3");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();

        // Categories
        Category categoryAlface = new Category(new Designation("Alface"));
        Category categoryTomate = new Category(new Designation("Tomate"));

        //Arrange
        Set<PersonID> responsibles = new HashSet<>();
        responsibles.add(joseID);
        responsibles.add(tarcisioID);

        Set<PersonID> members = new HashSet<>();
        members.add(joseID);
        members.add(tarcisioID);
        members.add(mariaID);

        Set<Category> categories = new HashSet<>();
        categories.add(categoryAlface);
        categories.add(categoryTomate);

        GroupDTO familyDTO = new GroupDTO();
        familyDTO.setGroupID(familyGroupID);
        familyDTO.setResponsibles(responsibles);
        familyDTO.setMembers(members);
        familyDTO.setCategories(categories);
        familyDTO.setCreationDate(familyDate);

        Mockito.when(groupsMockService.getGroup("Family3")).thenReturn(familyDTO);

        //Act
        ResponseEntity<Object> result = groupsRestController.getGroup("Family3");

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }
}