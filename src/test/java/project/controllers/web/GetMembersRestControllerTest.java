package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetMembersService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetMembersRestControllerTest {
    @Autowired
    private GetMembersService getMembersMockService;
    @Autowired
    private GetMembersRestController getMembersRestController;

    @DisplayName("getMembers - Happy Path")
    @Test
    public void getMembersHappyPath() {
        //Arrange
        //Members
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Group with two members
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisioborges@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, null, null);
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunoagostini@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, null, null);

        // GROUP
        Description membersDescription = new Description("Members Group");
        Group membersGroup = new Group(membersDescription, son.getPersonID());
        membersGroup.addMember(sibling.getPersonID());

        GroupDTO membersDTO = GroupAssembler.mapToDTO(membersGroup);
        Set<GroupDTO> membersDTOCollection = new HashSet<>();
        membersDTOCollection.add(membersDTO);

        Mockito.when(getMembersMockService.getMembers("Members Group")).thenReturn(new HashSet<>());

        //Act
        ResponseEntity<Object> result = getMembersRestController.getMembers("Members Group");

        assertEquals(200, result.getStatusCodeValue());
    }
}