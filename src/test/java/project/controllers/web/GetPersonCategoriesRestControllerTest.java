package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetPersonCategoriesService;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetPersonCategoriesRestControllerTest {

    @Autowired
    private GetPersonCategoriesRestController getPersonCategoriesRestController;
    @Autowired
    private GetPersonCategoriesService getPersonCategoriesMockService;

    @DisplayName("Get categories of a Person - Happy path ")
    @Test
    void getCategoriesPersonHappyPath() {
        //Arrange
        //Person
        LocalDateTime personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);
        Email personEmail = new Email("carlota@gmail.com");
        PersonID personID = new PersonID(personEmail);
        Person person = new Person(new Name("Carlota Castelo"), new Address("Matosinhos"), new Date(personBirthDate), personEmail, null, null);

        //Category
        Designation designationCategory1 = new Designation("Phones");
        Designation designationCategory2 = new Designation("Television");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        person.addCategory(category1);
        person.addCategory(category2);

        CategoryDTO categoryDTO1 = CategoryAssembler.mapToDTO(category1);
        CategoryDTO categoryDTO2 = CategoryAssembler.mapToDTO(category2);
        Set<CategoryDTO> categoryDTOSet = new HashSet<>();
        categoryDTOSet.add(categoryDTO1);
        categoryDTOSet.add(categoryDTO2);

        Mockito.when(getPersonCategoriesMockService.getPeronCategories("carlota@gmail.com")).thenReturn(categoryDTOSet);

        //Act
        ResponseEntity<Object> result = getPersonCategoriesRestController.getCategoriesCategory("carlota@gmail.com");

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }
}