package project.controllers.web;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.AddCategoryToGroupRequestInfoDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.AddCategoryToGroupRequestAssembler;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.services.AddCategoryToGroupService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddCategoryToGroupRestControllerTest {
    Designation designation;
    Description groupDescription;
    PersonID responsibleEmail;
    Group group;
    Person p1;
    LocalDateTime birthDate;

    @Autowired
    private AddCategoryToGroupRestController addCategoryToGroupRestController;
    @Autowired
    private AddCategoryToGroupService addCategoryToGroupMockService;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        birthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        p1 = new Person(new Name("Rui"), new Address("Porto"), new Date(birthDate), new Email("ruisemedo@gmail.com"), null, null);
        personRepository.save(p1);

        designation = new Designation("Garage");
        groupDescription = new Description("CarLovers");
        responsibleEmail = new PersonID(new Email("ruisemedo@gmail.com"));
        group = new Group(groupDescription, responsibleEmail);
        groupRepository.save(group);
    }

    @DisplayName("addCategoryToGroup - Happy Path")
    @Test
    void addCategoryToGroupHappyPath() {

        //Arrange
        Category category = new Category(designation);
        CategoryDTO expected = CategoryAssembler.mapToDTO(category);

        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation.getDesignation(),
                groupDescription.getDescription(), responsibleEmail.getEmail().getEmail());

        Mockito.when(addCategoryToGroupMockService.addCategoryToGroup(addCategoryToGroupRequestDTO)).thenReturn(expected);

        //Act
        AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO("Garage", "CarLovers", "ruisemedo@gmail.com");
        ResponseEntity<Object> result = addCategoryToGroupRestController.addCategoryToGroup(addCategoryToGroupRequestInfoDTO, "CarLovers");

        //Assert
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName("addCategoryToGroup - Group ID not found.")
    @Test
    void addCategoryToGroupNoGroupIDFound() {

        //Arrange
        Designation designation = new Designation("Football Items");
        Description groupDescription = new Description("Sport Department");
        Email responsibleEmail = new Email("antonio@sportsdirect.com");

        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation.getDesignation(),
                groupDescription.getDescription(), responsibleEmail.getEmail());

        Mockito.when(addCategoryToGroupMockService.addCategoryToGroup(addCategoryToGroupRequestDTO)).thenThrow(new NotFoundException("GroupID does not exist"));

        //Act
        AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO("Football Items", "Sport Department", "antonio@sportsdirect.com");
        Exception exception = assertThrows(NotFoundException.class, () -> addCategoryToGroupRestController.addCategoryToGroup(addCategoryToGroupRequestInfoDTO, "Sport Department"));

        //Assert
        assertEquals("GroupID does not exist", exception.getMessage());
    }
}