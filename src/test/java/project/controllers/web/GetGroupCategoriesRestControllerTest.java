package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.model.group.Group;
import project.model.shared.*;
import project.services.GetGroupCategoriesService;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetGroupCategoriesRestControllerTest {
    @Autowired
    private GetGroupCategoriesService getGroupCategoriesMockService;
    @Autowired
    private GetGroupCategoriesRestController getGroupCategoriesRestController;

    @DisplayName("Get categories of a Group - Happy path ")
    @Test
    void getCategoriesGroupHappyPath() {
        //Arrange

        //Group
        Description groupDescription = new Description("Group Vegan");
        Email email = new Email("joanita@gmail.com");
        PersonID personID = new PersonID(email);
        Group group = new Group(groupDescription, personID);
        GroupID groupID = new GroupID(groupDescription);

        //Category
        Designation designationCategory1 = new Designation("Flores");
        Designation designationCategory2 = new Designation("Vegetables");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        group.addCategory(category1);
        group.addCategory(category2);

        CategoryDTO categoryDTO1 = CategoryAssembler.mapToDTO(category1);
        CategoryDTO categoryDTO2 = CategoryAssembler.mapToDTO(category2);
        Set<CategoryDTO> categoryDTOSet = new HashSet<>();
        categoryDTOSet.add(categoryDTO1);
        categoryDTOSet.add(categoryDTO2);

        Mockito.when(getGroupCategoriesMockService.getGroupCategories("Group Vegan")).thenReturn(categoryDTOSet);

        //Act
        ResponseEntity<Object> result = getGroupCategoriesRestController.getCategoriesCategory("Group Vegan");

        //Assert

        assertEquals(200, result.getStatusCodeValue());
    }
}