package project.controllers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetPersonTransactionsInPeriodService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetPersonTransactionsInPeriodRestControllerTest {
    @Autowired
    GetPersonTransactionsInPeriodRestController getPersonTransactionsInPeriodRestController;
    @Autowired
    GetPersonTransactionsInPeriodService getPersonTransactionsInPeriodMockService;

    //BeforeEach
    Person person;
    Category personCategory1;
    Category personCategory2;
    Ledger ledger;
    AccountID debit01;
    AccountID debit02;
    AccountID credit01;
    AccountID credit02;
    Transaction transaction01;
    Transaction transaction02;
    Transaction transaction03;
    Transaction transaction04;
    Address birthAddress;
    Date birthDate;
    Name name;
    Email email;

    @BeforeEach
    void setUpForTests() {

        //Person1
        birthAddress = new Address("Porto, Portugal");
        birthDate = new Date(LocalDateTime.of(1955, 8, 12, 0, 0));
        name = new Name("Luis Carvalho");
        email = new Email("luis@family.com");
        person = new Person(name, birthAddress, birthDate, email, null, null);

        //Categories
        personCategory1 = new Category(new Designation("Shopping Maia"));
        personCategory2 = new Category(new Designation("Services Car"));

        //Ledger1
        ledger = new Ledger(new LedgerID(new OwnerID(person.getPersonID().getId())));

        //Accounts
        debit01 = new AccountID(new Denomination("Primary account pocket"), person.getPersonID());
        credit01 = new AccountID(new Denomination("Secondary account wallet"), person.getPersonID());

        debit02 = new AccountID(new Denomination("home account pocket"), person.getPersonID());
        credit02 = new AccountID(new Denomination("business account bank"), person.getPersonID());

        //Transaction1
        transaction01 = new Transaction(100.0,
                new Description("transaction simulated"),
                new TransactionDate(LocalDateTime.of(2020, 6, 12, 8, 0)),
                personCategory1,
                debit01,
                credit01,
                TransactionType.valueOf("CREDIT"));

        //Transaction2
        transaction02 = new Transaction(5.0,
                new Description("transaction small"),
                new TransactionDate(LocalDateTime.of(2018, 6, 12, 8, 0)),
                personCategory2,
                debit01,
                credit01,
                TransactionType.valueOf("DEBIT"));

        //Transaction3
        transaction03 = new Transaction(380.5,
                new Description("transaction big"),
                new TransactionDate(LocalDateTime.of(2017, 4, 12, 8, 0)),
                personCategory1,
                debit01,
                credit01,
                TransactionType.valueOf("CREDIT"));

        //Transaction4
        transaction04 = new Transaction(3.0,
                new Description("transaction small"),
                new TransactionDate(LocalDateTime.of(2020, 6, 19, 8, 0)),
                personCategory2,
                debit01,
                credit01,
                TransactionType.valueOf("DEBIT"));

        ledger.addTransaction(transaction01);
        ledger.addTransaction(transaction02);
        ledger.addTransaction(transaction03);
        ledger.addTransaction(transaction04);

    }

    @DisplayName("getPersonTransactionsInPeriod() - Happy Path")
    @Test
    public void getPersonTransactionInPeriodHappyPath() {
        //Arrange
        String personID = "luis@family.com";
        String accountDenomination = "Primary account pocket";
        String initialDate = "2020-04-05 07:30";
        String endDate = "2020-06-15 07:30";

        //Transactions
        Set<TransactionDTO> transactionDTOSet = new HashSet<>();
        transactionDTOSet.add(TransactionAssembler.mapToDTO(transaction01));
        transactionDTOSet.add(TransactionAssembler.mapToDTO(transaction04));

        Mockito.when(getPersonTransactionsInPeriodMockService.getPersonTransactionsInPeriod(personID, accountDenomination, initialDate, endDate)).thenReturn(transactionDTOSet);

        //Act
        ResponseEntity<Object> resultTransactionResponseDTO = getPersonTransactionsInPeriodRestController.getPersonTransactionInPeriod(personID, accountDenomination, initialDate, endDate);

        //Assert
        int status = resultTransactionResponseDTO.getStatusCodeValue();
        assertEquals(200, status);
    }

    @DisplayName("getPersonTransactionsInPeriod - Non existing Person")
    @Test
    public void getPersonTransactionInPeriodNonExistingPerson() {
        //Arrange
        String personID = "unknown.person@controller.com";
        String accountDenomination = "Primary account pocket";
        String initialDate = "2020-04-05 07:30";
        String endDate = "2020-06-15 07:30";

        Mockito.when(getPersonTransactionsInPeriodMockService.getPersonTransactionsInPeriod(personID, accountDenomination, initialDate, endDate)).thenThrow(new NotFoundException("PersonID does not exist"));

        //Act/Assert
        Exception exception = assertThrows(NotFoundException.class, () -> getPersonTransactionsInPeriodMockService.getPersonTransactionsInPeriod(personID, accountDenomination, initialDate, endDate));

        assertEquals("PersonID does not exist", exception.getMessage());

    }
}