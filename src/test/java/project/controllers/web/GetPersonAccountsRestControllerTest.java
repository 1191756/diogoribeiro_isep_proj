package project.controllers.web;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetPersonAccountsService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GetPersonAccountsRestControllerTest {
    @Autowired
    GetPersonAccountsRestController getPersonAccountsRestController;
    @Autowired
    GetPersonAccountsService getPersonAccountsMockService;

    @DisplayName("getPersonAccounts() - Happy Path")
    @Test
    public void getPersonAccountsHappyPath() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        //Persons

        Name name = new Name("Guilherme");
        Email guilhermeEmail = new Email("guilherme@family.com");
        Person guilherme = new Person(name, birthAddress, personsBirthdate, guilhermeEmail, null, null);

        //ACCOUNTS
        Account account1 = new Account(new Denomination("awsome account"),new Description("Created to test controller"),guilherme.getPersonID());

        Set<AccountDTO> accountsDTO = new HashSet<>();

        AccountDTO accountDTO1 = new AccountDTO();
        accountDTO1.setAccountID(account1.getAccountID());
        accountDTO1.setDescription(account1.getDescription());

        accountsDTO.add(accountDTO1);

        Mockito.when(getPersonAccountsMockService.getPersonAccounts(guilhermeEmail.toString())).thenReturn(accountsDTO);

        //Act
        ResponseEntity<Object> resultAccountsResponseDTO = getPersonAccountsRestController.getPersonAccounts(guilhermeEmail.toString());

        //Assert
        int status = resultAccountsResponseDTO.getStatusCodeValue();
        assertEquals(200, status);
    }

    @DisplayName("getPersonAccounts - Non existing Person")
    @Test
    public void getGroupAccountsNonExistingPerson() {

        //Arrange

        Mockito.when(getPersonAccountsMockService.getPersonAccounts("unknown.person@controller.com")).thenThrow(new NotFoundException("PersonID does not exist"));

        //Act/Assert
        Exception exception = assertThrows(NotFoundException.class, () -> getPersonAccountsRestController.getPersonAccounts("unknown.person@controller.com"));

        assertEquals("PersonID does not exist", exception.getMessage());

    }
}

