package project.controllers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMembersRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.dto.assemblers.AddMembersRequestAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.services.AddMemberToGroupService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddMemberToGroupRestControllerTest {
    Name p1Name;
    Address p1BirthPlace;
    Date p1BirthDate;
    Email p1Email;
    Person p1;
    PersonID p1ID;
    Name p2Name;
    Address p2BirthPlace;
    Date p2BirthDate;
    Email p2Email;
    Person p2;
    PersonID p2ID;
    Description groupDescription1;
    Group g1;
    GroupID g1ID;
    Description groupDescription2;
    Group g2;
    GroupID g2ID;
    Set<PersonID> membersSet;
    Set<Category> categories;
    GroupDTO expected;

    @Autowired
    private AddMemberToGroupService addMemberToGroupMockService;
    @Autowired
    private AddMemberToGroupRestController addMemberToGroupRestController;
    @Autowired
    private GroupRepository groupRepository;

    @BeforeEach
    void setUp() {
        p1Name = new Name("Pedro");
        p1BirthPlace = new Address("Porto");
        p1BirthDate = new Date(LocalDateTime.of(1982, 3, 14, 0, 0));
        p1Email = new Email("pedro.1234@gmail.com");
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2Name = new Name("Sara");
        p2BirthPlace = new Address("Aveiro");
        p2BirthDate = new Date(LocalDateTime.of(1988, 3, 18, 10, 0));
        p2Email = new Email("sararibeiro88@email.com");
        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        groupDescription1 = new Description("Family");
        g1 = new Group(groupDescription1, p1ID);
        g1ID = g1.getID();

        groupDescription2 = new Description("Family2");
        g2 = new Group(groupDescription2, p2ID);
        g2ID = g2.getID();

        // Categories
        Category categoryAlface = new Category(new Designation("Alface"));
        Category categoryTomate = new Category(new Designation("Tomate"));
        p1.addCategory(categoryAlface);
        p1.addCategory(categoryTomate);

        groupRepository.save(g1);

        membersSet = new HashSet<>();
        categories = new HashSet<>();
        categories.add(categoryAlface);
        categories.add(categoryTomate);

        membersSet.add(p1ID);
        expected = new GroupDTO();
        expected.setGroupID(g1ID);
        expected.setMembers(membersSet);
        expected.setResponsibles(membersSet);
        expected.setCategories(categories);
        expected.setCreationDate(new Date(LocalDateTime.now()));
    }

    @DisplayName("addMemberToGroup - Happy Path")
    @Test
    public void addMemberToGroup() {
        //Arrange
        String groupDescription = "Family";
        String personEmail = "maria@gmail.com";

        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);
        Mockito.when(addMemberToGroupMockService.addMember(addMemberRequestDTO)).thenReturn(expected);

        //Act
        ResponseEntity<Object> result = addMemberToGroupRestController.addMemberToGroup(groupDescription, personEmail);

        //Assert
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName("addMembersToGroup - Happy Path")
    @Test
    public void addMembersToGroup() {
        //Arrange
        String groupDescription = "Family";
        List<String> personEmails = new ArrayList<>();
        personEmails.add("sararibeiro88@gmail.com");
        personEmails.add("guga78@gmail.com");

        AddMembersRequestDTO addMembersRequestDTO = AddMembersRequestAssembler.mapToDTO(groupDescription, personEmails);
        Mockito.when(addMemberToGroupMockService.addMembers(addMembersRequestDTO)).thenReturn(expected);

        String members = "sararibeiro88@gmail.com, guga78@gmail.com";

        //Act
        ResponseEntity<Object> result = addMemberToGroupRestController.addMembersToGroup(groupDescription, members);

        //Assert
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName("addMemberToGroup - No GroupID Found")
    @Test
    public void addMemberToGroupNoGroupIDFound() {
        //Arrange
        String groupDescription = "University";
        String personEmail = "pedrobananas@gmail.com";

        Description FamilyDescription = new Description(groupDescription);
        Email pedroEmail = new Email(personEmail);
        PersonID personID = new PersonID(pedroEmail);
        GroupID groupID = new GroupID(FamilyDescription);


        AddMemberRequestDTO addMemberRequestDTO = new AddMemberRequestDTO(groupID, personID);
        Mockito.when(addMemberToGroupMockService.addMember(addMemberRequestDTO)).thenThrow(new NotFoundException("GroupID does not exist"));

        //Act //Assert
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addMemberToGroupRestController.addMemberToGroup(groupDescription, personEmail);
        });

        assertEquals("GroupID does not exist", exception.getMessage());
    }
}


