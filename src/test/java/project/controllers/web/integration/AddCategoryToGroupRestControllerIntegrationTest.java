package project.controllers.web.integration;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddCategoryToGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddCategoryToGroupRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;

    @DisplayName("addCategoryToGroup - Happy Path")
    @Test
    void addCategoryToGroupHappyPath() throws Exception {

        //Arrange
        final Address birthAddress = new Address("Porto, Portugal");
        final Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        final Name rafaelName = new Name("Rafa");
        final Email rafaelEmail = new Email("rafael@family.com");
        final Person rafael = new Person(rafaelName, birthAddress, personsBirthdate, rafaelEmail, null, null);
        final String groupDescription = "CategoryGroupHappy";

        personRepository.save(rafael);
        groupRepository.save(new Group(groupDescription, rafaelEmail.toString()));

        final String uri = "/groups/CategoryGroupHappy/categories";
        final String designation = "GroceriesShop Fresh Account";

        AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, rafaelEmail.toString());

        //Act
        String entryJson = super.mapToJson(addCategoryToGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String designation1 = design.read("$.designation");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals("GroceriesShop Fresh Account", designation1);
    }

    @DisplayName("addCategoryToGroup - Person is not responsible")
    @Test
    public void addCategoryToGroupNotResponsible() throws Exception {

        //Arrange
        final String uri = "/groups/CategoryGroupNotResponsible/categories";
        final String designation = "Responsible Groceries Account";
        final String groupDescription = "CategoryGroupNotResponsible";
        final String responsibleEmail = "jose@family.com";
        final String notResponsibleEmail = "mafalda@family.com";

        groupRepository.save(new Group(groupDescription, responsibleEmail));

        AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, notResponsibleEmail);

        //Act
        String entryJson = super.mapToJson(addCategoryToGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing Responsible", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }

    @DisplayName("addCategoryToGroupIDNotFound - GroupID not found")
    @Test
    void addCategoryToGroupIDNotFound() throws Exception {

        //Arrange
        final String uri = "/groups/Inexistant/categories";
        final String designation = "Groceries Fabulous Account";
        final String groupDescription = "Inexistant";
        final String responsibleEmail = "jose@family.com";

        AddCategoryToGroupRequestInfoDTO addCategoryToGroupRequestInfoDTO = new AddCategoryToGroupRequestInfoDTO(designation, groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addCategoryToGroupRequestInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing GroupID", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}