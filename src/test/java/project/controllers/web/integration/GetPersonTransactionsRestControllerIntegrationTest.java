package project.controllers.web.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.model.account.Account;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetPersonTransactionsRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    @Test
    void getPersonTransactionInPeriod() throws Exception {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        // Family with two sons
        Name jose = new Name("José Adolfo Panayiotou");
        Email joseEmail = new Email("josemanuelfaina@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Categories
        Category categoryAllowances = new Category(new Designation("Special Allowances"));
        Category categoryGroceries = new Category(new Designation("Weekly Groceries"));
        Category categoryUtilities = new Category(new Designation("Monthly Utilities"));
        father.addCategory(categoryAllowances);
        father.addCategory(categoryGroceries);
        father.addCategory(categoryUtilities);

        personRepository.save(father);

        Account account1 = new Account(new Denomination("Paycheck gig account"), new Description("Salary values"), father.getPersonID());
        Account account2 = new Account(new Denomination("Home spending big account"), new Description("Groceries, utilities, etc"), father.getPersonID());
        accountRepository.save(account1);
        accountRepository.save(account2);

        Ledger fatherLedger = new Ledger(new LedgerID(father.getPersonID()));

        Transaction transaction = new Transaction(50.0, new Description("Son's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 0)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);
        Transaction transaction2 = new Transaction(50.0, new Description("Daughter's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 1)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);

        fatherLedger.addTransaction(transaction);
        fatherLedger.addTransaction(transaction2);
        ledgerRepository.save(fatherLedger);

        final String uri = "/persons/josemanuelfaina@family.com/transactions";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        //Assert
        assertEquals(200, status);
    }
}