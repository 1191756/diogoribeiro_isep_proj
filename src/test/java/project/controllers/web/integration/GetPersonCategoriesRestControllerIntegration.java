package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.PersonRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetPersonCategoriesRestControllerIntegration extends AbstractTest {

    @Autowired
    private PersonRepository personRepository;

    @DisplayName("get person categories - Happy path")
    @Test
    @Transactional

    public void getPersonCategoriesHappyPath() throws Exception {

        //Arrange
        //Person
        LocalDateTime personBirthDate = LocalDateTime.of(2003, Month.APRIL, 26, 01, 30, 34);
        Email personEmail = new Email("miquelina@gmail.com");
        PersonID personID = new PersonID(personEmail);
        Person person = new Person(new Name("Miquelina Silva"), new Address("Algarve"), new Date(personBirthDate), personEmail, null, null);

        //Category
        Designation designationCategory1 = new Designation("River");
        Designation designationCategory2 = new Designation("Sea");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        person.addCategory(category1);
        person.addCategory(category2);

        personRepository.save(person);

        final String uri = "/persons/miquelina@gmail.com/categories";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertTrue(content.contains("\"designation\":\"River\""));
        assertTrue(content.contains("\"designation\":\"Sea\""));
    }

}
