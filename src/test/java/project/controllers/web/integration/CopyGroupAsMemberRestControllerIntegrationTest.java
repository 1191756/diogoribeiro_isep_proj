package project.controllers.web.integration;


import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddGroupRequestInfoDTO;
import project.repositories.GroupRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CopyGroupAsMemberRestControllerIntegrationTest extends AbstractTest {

    @Autowired
    GroupRepository groupRepository;

    @DisplayName("As a member, I want to copy a group, but only its members and categories. - Happy path ")
    @Test
    void copyGroupAsMemberHappyPath() throws Exception {

        //Arrange
        final String uri = "/persons";
        final String groupDescription = "Family";
        final String memberEmail = "jose@family.com";

        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, memberEmail);

        //Act
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String description = design.read("$.groupDescription");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals("Family", description);
    }
}