package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.model.group.Group;
import project.model.shared.*;
import project.repositories.GroupRepository;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetGroupCategoryRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    private GroupRepository groupRepository;

    @DisplayName("get group categories - Happy path")
    @Test
    @Transactional

    public void getGroupCategoriesHappyPath() throws Exception {

        //Arrange

        //Group
        Description groupDescription = new Description("Amazing Group");
        Email email = new Email("Alice@gmail.com");
        PersonID personID = new PersonID(email);
        Group group = new Group(groupDescription, personID);
        GroupID groupID = new GroupID(groupDescription);

        //Category
        Designation designationCategory1 = new Designation("Roses");
        Designation designationCategory2 = new Designation("Tulip");
        Category category1 = new Category(designationCategory1);
        Category category2 = new Category(designationCategory2);
        group.addCategory(category1);
        group.addCategory(category2);

        groupRepository.save(group);

        final String uri = "/groups/Amazing Group/categories";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertTrue(content.contains("\"designation\":\"Roses\""));
        assertTrue(content.contains("\"designation\":\"Tulip\""));
    }
}
