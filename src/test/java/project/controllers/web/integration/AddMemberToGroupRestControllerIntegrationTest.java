package project.controllers.web.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddMemberRequestInfoDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddMemberToGroupRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    GroupRepository groupRepo;
    @Autowired
    PersonRepository personRepository;

    @DisplayName("addMemberToGroup - Happy Path")
    @Test
    public void addMemberToGroup() throws Exception {
        //Arrange
        String uri = "/groups/University/members/";
        ObjectMapper objectMapper = new ObjectMapper();
        Name p1Name = new Name("Pedro Castro");
        Address pedroBirthPlace = new Address("Gaia");
        Date pedroBirthDate = new Date(LocalDateTime.of(1981, 3, 14, 0, 0));
        Email p1Email = new Email("pedrocastro2@gmail.com");
        Person pedro = new Person(p1Name, pedroBirthPlace, pedroBirthDate, p1Email, null, null);
        PersonID p1ID = pedro.getPersonID();
        Name p2Name = new Name("Pedro Castro");
        Address marcoBirthPlace = new Address("Gaia");
        Date marcoBirthDate = new Date(LocalDateTime.of(1981, 3, 14, 0, 0));
        Email p2Email = new Email("marcoalmendres@gmail.com");
        Person marco = new Person(p2Name, marcoBirthPlace, marcoBirthDate, p2Email, null, null);
        PersonID p2ID = marco.getPersonID();
        personRepository.save(marco);
        personRepository.save(pedro);

        //Group repository with group
        Description groupUniversityDescription = new Description("University");
        Group g1 = new Group(groupUniversityDescription, p1ID);
        groupRepo.save(g1);
        String groupDescription = "University";

        AddMemberRequestInfoDTO addMemberRequestInfoDTO = new AddMemberRequestInfoDTO(groupDescription, p2Email.toString());

        //Act
        String entryJson = super.mapToJson("[{\"value\":\"tarcisio@family.com\",\"label\":\"Tarcisio\"},{\"value\":\"jose@family.com\",\"label\":\"José\"},{\"value\":\"nuno@family.com\",\"label\":\"Nuno\"},{\"value\":\"maria@family.com\",\"label\":\"Maria\"}]");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String description = design.read("$.groupDescription");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals("University", description);
    }

    @DisplayName("addMemberGroup - GroupID not found")
    @Test
    public void addMemberToGroupIDNotFound() throws Exception {
        //Arrange
        String uri = "/groups/Family123/members/";

        String groupDescription = "Family123";
        String personEmail = "pedro.castro@gmail.com";

        AddMemberRequestInfoDTO addMemberRequestInfoDTO = new AddMemberRequestInfoDTO(groupDescription, personEmail);

        //Act
        String entryJson = super.mapToJson("[{\"value\":\"tarcisio@family.com\",\"label\":\"Tarcisio\"},{\"value\":\"jose@family.com\",\"label\":\"José\"},{\"value\":\"nuno@family.com\",\"label\":\"Nuno\"},{\"value\":\"maria@family.com\",\"label\":\"Maria\"}]");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing GroupID", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}