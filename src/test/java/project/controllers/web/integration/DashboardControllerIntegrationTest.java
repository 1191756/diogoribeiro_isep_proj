package project.controllers.web.integration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.model.account.Account;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DashboardControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    @Test
    void getPersonTransactionForPieChart() throws Exception {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name jose = new Name("José Adolfo Panayiotou");
        Email joseEmail = new Email("josemanuelfaina@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Categories
        Category categoryAllowances = new Category(new Designation("Special Allowances"));
        Category categoryGroceries = new Category(new Designation("Weekly Groceries"));
        Category categoryUtilities = new Category(new Designation("Monthly Utilities"));
        father.addCategory(categoryAllowances);
        father.addCategory(categoryGroceries);
        father.addCategory(categoryUtilities);

        personRepository.save(father);

        // GROUPS
        Description groupDescription = new Description("Reguengos");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();

        familyGroup.addCategory(categoryAllowances);
        familyGroup.addCategory(categoryGroceries);
        groupRepository.save(familyGroup);

        Account account1 = new Account(new Denomination("Paycheck gig account"), new Description("Salary values"), father.getPersonID());
        Account account2 = new Account(new Denomination("Home spending big account"), new Description("Groceries, utilities, etc"), father.getPersonID());
        accountRepository.save(account1);
        accountRepository.save(account2);

        Ledger fatherLedger = new Ledger(new LedgerID(father.getPersonID()));
        Ledger familyLedger = new Ledger(new LedgerID(familyGroup.getID()));

        Transaction transaction = new Transaction(50.0, new Description("Son's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 0)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);
        Transaction transaction2 = new Transaction(50.0, new Description("Daughter's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 1)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);

        familyLedger.addTransaction(transaction);
        familyLedger.addTransaction(transaction2);
        ledgerRepository.save(fatherLedger);

        fatherLedger.addTransaction(transaction);
        fatherLedger.addTransaction(transaction2);
        ledgerRepository.save(familyLedger);

        final String uri = "/dashboard/josemanuelfaina@family.com/pie";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(200, status);
    }

    @Test
    void getPersonTransactionForlinesChart() throws Exception {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name jose = new Name("José Adolfo Panayiotou");
        Email joseEmail = new Email("josemanuelfaina@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        //Categories
        Category categoryAllowances = new Category(new Designation("Special Allowances"));
        Category categoryGroceries = new Category(new Designation("Weekly Groceries"));
        Category categoryUtilities = new Category(new Designation("Monthly Utilities"));
        father.addCategory(categoryAllowances);
        father.addCategory(categoryGroceries);
        father.addCategory(categoryUtilities);

        personRepository.save(father);

        // GROUPS
        Description groupDescription = new Description("Reguengos");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();

        familyGroup.addCategory(categoryAllowances);
        familyGroup.addCategory(categoryGroceries);
        groupRepository.save(familyGroup);

        Account account1 = new Account(new Denomination("Paycheck gig account"), new Description("Salary values"), father.getPersonID());
        Account account2 = new Account(new Denomination("Home spending big account"), new Description("Groceries, utilities, etc"), father.getPersonID());
        accountRepository.save(account1);
        accountRepository.save(account2);

        Ledger fatherLedger = new Ledger(new LedgerID(father.getPersonID()));
        Ledger familyLedger = new Ledger(new LedgerID(familyGroup.getID()));

        Transaction transaction = new Transaction(50.0, new Description("Son's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 0)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);
        Transaction transaction2 = new Transaction(50.0, new Description("Daughter's allowance"), new TransactionDate(LocalDateTime.of(2020, 6, 2, 0, 0, 1)), categoryAllowances, account2.getAccountID(), account1.getAccountID(), TransactionType.DEBIT);

        familyLedger.addTransaction(transaction);
        familyLedger.addTransaction(transaction2);
        ledgerRepository.save(fatherLedger);

        fatherLedger.addTransaction(transaction);
        fatherLedger.addTransaction(transaction2);
        ledgerRepository.save(familyLedger);

        final String uri = "/dashboard/josemanuelfaina@family.com/lines";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(200, status);
    }

    @Test
    void getPersonLinksTest() throws Exception {
        //Arrange
        final String uri = "/dashboard/jose@family.com";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(200, status);
    }
}