package project.controllers.web.integration;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.dto.AddTransactionToGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AddTransactionToGroupRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    LedgerRepository ledgerRepository;
    //BeforeEach
    Person person1;
    Person person2;
    Category category1;
    Category category2;
    Group group1;
    Group group2;
    AccountID debit1;
    AccountID debit2;
    AccountID credit1;
    AccountID credit2;
    Transaction transaction1;
    Transaction transaction2;
    Transaction transaction3;
    Transaction transaction4;
    Description description1;
    Description description2;
    Address birthAddress1;
    Address birthAddress2;
    Date birthdate1;
    Date birthdate2;
    Name name1;
    Name name2;
    Email email1;
    Email email2;

    @BeforeEach
    void setUpForTests() {

        //Person1
        birthAddress1 = new Address("Maia, Portugal");
        birthdate1 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name1 = new Name("Ricardo Carvalho");
        email1 = new Email("ricardo@family.com");
        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);

        //Person2
        birthAddress2 = new Address("Maia, Portugal");
        birthdate2 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name2 = new Name("Vitor Carvalho");
        email2 = new Email("vitor@family.com");
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);

        //Categories
        category1 = new Category(new Designation("Shopping"));
        category2 = new Category(new Designation("Services"));

        //Group1
        description1 = new Description("FantasticGroup");

        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);

        //Group2
        description2 = new Description("AmazingGroup");

        group2 = new Group(description2, person2.getPersonID());

        group2.addCategory(category1);
        group2.addCategory(category2);

        //Accounts
        debit1 = new AccountID(new Denomination("Primary account"), group1.getID());
        credit1 = new AccountID(new Denomination("Secondary account"), group1.getID());

        debit2 = new AccountID(new Denomination("home account"), group2.getID());
        credit2 = new AccountID(new Denomination("business account"), group2.getID());

        //Transaction1
        transaction1 = new Transaction(100.0,
                new Description("big transaction"),
                new TransactionDate(LocalDateTime.now()),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction2
        transaction2 = new Transaction(5.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.now()),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        //Transaction3
        transaction3 = new Transaction(380.5,
                new Description("big transaction"),
                new TransactionDate(LocalDateTime.now()),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction4
        transaction4 = new Transaction(3.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.now()),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));
    }

    @DisplayName("addTransactionToGroup - Happy Path")
    @Test
    public void addTransactionToGroupHappyPath() throws Exception {
        //Arrange

        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);
        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);

        groupRepository.save(group1);

        //Transaction1
        transaction1 = new Transaction(1000.0,
                new Description("big transaction"),
                new TransactionDate(LocalDateTime.now()),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        AddTransactionToGroupRequestInfoDTO infoDTO = new AddTransactionToGroupRequestInfoDTO(person1.getPersonID().getEmail().toString(),
                group1.getID().toString(),
                transaction1.getAmount(),
                transaction1.getDescription().toString(),
                transaction1.getCategory().getDesignation().getDesignation(),
                transaction1.getDebit().getDenomination().toString(),
                transaction1.getCredit().getDenomination().toString(),
                transaction1.getType().toString());

        final String uri = "/groups/FantasticGroup/transaction";

        //Act
        String entryJson = super.mapToJson(infoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        double amount1 = design.read("$.amount");
        String description1 = design.read("$.description");
        String category = JsonPath.parse("{\"Category\" : Shopping}").read("$.Category");
        String debit = JsonPath.parse("{\"AccountID\" : Primary account}").read("$.AccountID");
        String credit = JsonPath.parse("{\"AccountID\" : Secondary account}").read("$.AccountID");
        String type = design.read("$.type");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals(1000.0, amount1);
        assertEquals("big transaction", description1);
        assertEquals("Shopping", category);
        assertEquals("Primary account", debit);
        assertEquals("Secondary account", credit);
        assertEquals("CREDIT", type);
    }

    @DisplayName("addTransactionToGroup - GroupID not found")
    @Test
    public void addAccountGroupIDNotFound() throws Exception {

        //Arrange
        final String uri = "/groups/UnsavedGroup/transaction";

        AddTransactionToGroupRequestInfoDTO infoDTO = new AddTransactionToGroupRequestInfoDTO(person1.getPersonID().getEmail().toString(),
                group1.getID().toString(),
                transaction1.getAmount(),
                transaction1.getDescription().toString(),
                transaction1.getCategory().getDesignation().getDesignation(),
                transaction1.getDebit().getDenomination().toString(),
                transaction1.getCredit().getDenomination().toString(),
                transaction1.getType().toString());

        //Act
        String entryJson = super.mapToJson(infoDTO);
        System.out.println(entryJson);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing GroupID", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }

    @DisplayName("addTransactionToGroup - Person is not a GroupMember")
    @Test
    public void addTransactionToGroupNotAMember() throws Exception {
        //Arrange
        final String uri = "/groups/FantasticGroup/transaction";

        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);
        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);

        groupRepository.save(group1);

        AddTransactionToGroupRequestInfoDTO infoDTO = new AddTransactionToGroupRequestInfoDTO(person2.getPersonID().getEmail().toString(),
                group1.getID().toString(),
                transaction1.getAmount(),
                transaction1.getDescription().toString(),
                transaction1.getCategory().getDesignation().getDesignation(),
                transaction1.getDebit().getDenomination().toString(),
                transaction1.getCredit().getDenomination().toString(),
                transaction1.getType().toString());

        //Act
        String entryJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing Member", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }

    @DisplayName("addTransactionToGroup - Group does not contain given category")
    @Test
    public void addTransactionToGroupNoCategory() throws Exception {
        //Arrange
        final String uri = "/groups/FantasticGroup/transaction";

        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);
        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);
        Category category3 = new Category(new Designation("NoCategory"));

        groupRepository.save(group1);

        AddTransactionToGroupRequestInfoDTO infoDTO = new AddTransactionToGroupRequestInfoDTO(person2.getPersonID().getEmail().toString(),
                group1.getID().toString(),
                transaction1.getAmount(),
                transaction1.getDescription().toString(),
                category3.getDesignation().getDesignation(),
                transaction1.getDebit().getDenomination().toString(),
                transaction1.getCredit().getDenomination().toString(),
                transaction1.getType().toString());

        //Act
        String entryJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing Category", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}