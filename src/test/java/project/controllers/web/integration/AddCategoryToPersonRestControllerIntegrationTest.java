package project.controllers.web.integration;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddCategoryToPersonRequestInfoDTO;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddCategoryToPersonRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;

    @DisplayName("addCategoryToPerson - Happy Path")
    @Test
    void addCategoryToPersonHappyPath() throws Exception {
        //Arrange
        final Address birthAddress = new Address("Porto, Portugal");
        final Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        final Name rafaelName = new Name("Rafa");
        final Email rafaelEmail = new Email("rafaelbenzeno@family.com");
        final Person rafael = new Person(rafaelName, birthAddress, personsBirthdate, rafaelEmail, null, null);

        personRepository.save(rafael);

        final String uri = "/persons/rafaelbenzeno@family.com/categories";
        final String designation = "GroceriesShop Fresh Account";

        AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTO = new AddCategoryToPersonRequestInfoDTO(designation, rafaelEmail.toString());

        //Act
        String entryJson = super.mapToJson(addCategoryToPersonRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String designation1 = design.read("$.designation");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals("GroceriesShop Fresh Account", designation1);
    }

    @DisplayName("addCategoryToPersonIDNotFound - PersonID not found")
    @Test
    void addCategoryToPersonIDNotFound() throws Exception {
        //Arrange
        final String uri = "/persons/josefarelos@family.com/categories";
        final String personEmail = "josefarelos@family.com";
        final String designation = "GroceriesShop Fresh Account";

        AddCategoryToPersonRequestInfoDTO addCategoryToPersonRequestInfoDTO = new AddCategoryToPersonRequestInfoDTO(designation, personEmail);

        //Act
        String entryJson = super.mapToJson(addCategoryToPersonRequestInfoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an existing PersonID", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}