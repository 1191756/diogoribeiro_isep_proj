package project.controllers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetGroupTransactionsInPeriodService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetGroupTransactionsInPeriodRestControllerTest {

    @Autowired
    GetGroupTransactionsInPeriodRestController getGroupTransactionsInPeriodRestController;
    @Autowired
    GetGroupTransactionsInPeriodService getGroupTransactionsInPeriodMockService;

    //BeforeEach
    Person person1;
    Person person2;
    Category personCategory1;
    Category personCategory2;
    Group group1;
    Group group2;
    Ledger ledger1;
    Ledger ledger2;
    AccountID debit01;
    AccountID debit02;
    AccountID credit01;
    AccountID credit02;
    Transaction transaction01;
    Transaction transaction02;
    Transaction transaction03;
    Transaction transaction04;
    Description description1;
    Description description2;
    Address birthAddress1;
    Address birthAddress2;
    Date birthdate1;
    Date birthdate2;
    Name name1;
    Name name2;
    Email email1;
    Email email2;

    @BeforeEach
    void setUpForTests() {
        //Person1
        birthAddress1 = new Address("Maia, Portugal");
        birthdate1 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name1 = new Name("Ricardo Carvalho");
        email1 = new Email("ricardo@family.com");
        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);

        //Person2
        birthAddress2 = new Address("Maia, Portugal");
        birthdate2 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name2 = new Name("Vitor Carvalho");
        email2 = new Email("vitor@family.com");
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);

        //Categories
        personCategory1 = new Category(new Designation("Shopping"));
        personCategory2 = new Category(new Designation("Services"));

        //Group1
        description1 = new Description("FantasticGroup");

        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(personCategory1);
        group1.addCategory(personCategory2);

        //Group2
        description2 = new Description("AmazingGroup");

        group2 = new Group(description2, person2.getPersonID());

        group2.addCategory(personCategory1);
        group2.addCategory(personCategory2);

        //Ledger1
        ledger1 = new Ledger(new LedgerID(new OwnerID(group1.getID().getId())));
        ledger2 = new Ledger(new LedgerID(new OwnerID(group2.getID().getId())));

        //Accounts
        debit01 = new AccountID(new Denomination("Primary account"), group1.getID());
        credit01 = new AccountID(new Denomination("Secondary account"), group1.getID());

        debit02 = new AccountID(new Denomination("home account"), group1.getID());
        credit02 = new AccountID(new Denomination("business account"), group1.getID());

        //Transaction1
        transaction01 = new Transaction(100.0,
                new Description("transaction simulated"),
                new TransactionDate(LocalDateTime.of(2020, 6, 12, 8, 0)),
                personCategory1,
                debit01,
                credit01,
                TransactionType.valueOf("CREDIT"));

        //Transaction2
        transaction02 = new Transaction(5.0,
                new Description("transaction small"),
                new TransactionDate(LocalDateTime.of(2018, 6, 12, 8, 0)),
                personCategory2,
                debit01,
                credit01,
                TransactionType.valueOf("DEBIT"));

        //Transaction3
        transaction03 = new Transaction(380.5,
                new Description("transaction big"),
                new TransactionDate(LocalDateTime.of(2017, 4, 12, 8, 0)),
                personCategory1,
                debit01,
                credit01,
                TransactionType.valueOf("CREDIT"));

        //Transaction4
        transaction04 = new Transaction(3.0,
                new Description("transaction small"),
                new TransactionDate(LocalDateTime.of(2020, 6, 19, 8, 0)),
                personCategory2,
                debit01,
                credit01,
                TransactionType.valueOf("DEBIT"));

        ledger1.addTransaction(transaction01);
        ledger1.addTransaction(transaction02);
        ledger2.addTransaction(transaction03);
        ledger2.addTransaction(transaction04);

    }

    @DisplayName("getGroupTransactionsInPeriod() - Happy Path")
    @Test
    public void getPersonTransactionInPeriodHappyPath() {
        //Arrange
        String personID = "luis@family.com";
        String groupID = "FantasticGroup";
        String accountDenomination = "Primary account";
        String initialDate = "2020-04-05 07:30";
        String endDate = "2020-06-15 07:30";

        //Transactions
        Set<TransactionDTO> transactionDTOSet = new HashSet<>();
        transactionDTOSet.add(TransactionAssembler.mapToDTO(transaction01));
        transactionDTOSet.add(TransactionAssembler.mapToDTO(transaction04));

        Mockito.when(getGroupTransactionsInPeriodMockService.getGroupTransactionsInPeriod(groupID, personID, accountDenomination, initialDate, endDate)).thenReturn(transactionDTOSet);

        //Act
        ResponseEntity<Object> resultTransactionResponseDTO = getGroupTransactionsInPeriodRestController.getPersonTransactionInPeriod(groupID, personID, accountDenomination, initialDate, endDate);

        //Assert
        int status = resultTransactionResponseDTO.getStatusCodeValue();
        assertEquals(200, status);
    }

    @DisplayName("getPersonTransactionsInPeriod - Non existing GroupID")
    @Test
    public void getPersonTransactionInPeriodNonExistingGroup() {
        //Arrange
        String personID = "unknown.person@controller.com";
        String groupID = "FantasticGroup";
        String accountDenomination = "Primary account";
        String initialDate = "2020-04-05 07:30";
        String endDate = "2020-06-15 07:30";

        Mockito.when(getGroupTransactionsInPeriodMockService.getGroupTransactionsInPeriod(groupID, personID, accountDenomination, initialDate, endDate)).thenThrow(new NotFoundException("GroupID does not exist"));

        //Act/Assert
        Exception exception = assertThrows(NotFoundException.class, () -> getGroupTransactionsInPeriodMockService.getGroupTransactionsInPeriod(groupID, personID, accountDenomination, initialDate, endDate));

        assertEquals("GroupID does not exist", exception.getMessage());

    }
}