package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.AddCategoryToGroupRequestAssembler;
import project.dto.assemblers.CategoryAssembler;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.services.AddCategoryToGroupService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddCategoryToGroupControllerTest {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    AddCategoryToGroupController addCategoryToGroupController;
    @Autowired
    AddCategoryToGroupService addCategoryToGroupMockService;

    @DisplayName("addCategoryToGroup() - Happy Path")
    @Test
    void addCategoryToGroup() {
        //Arrange
        LocalDateTime birthDate = LocalDateTime.of(1988, Month.MAY, 01, 0, 0, 0);
        Email personEmail = new Email("rosaliafernandesalbitre@gmail.com");
        Person person = new Person(new Name("Rosalia"), new Address("Porto"), new Date(birthDate), personEmail, null, null);

        personRepository.save(person);

        Description groupDescription = new Description("Group for accounts");
        Group accountGroup = new Group(groupDescription, person.getPersonID());

        groupRepository.save(accountGroup);

        Designation accountDesignation = new Designation("Shopping Account");
        Category category = new Category(accountDesignation);
        CategoryDTO categoryDTO = CategoryAssembler.mapToDTO(category);

        final String accountStringDesignation = "Group for accounts";
        final String groupStringdescription = "Group for accounts";
        final String responsibleEmail = "rosaliafernandesalbitre@gmail.com";
        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(accountStringDesignation,
                groupStringdescription,
                responsibleEmail);

        Mockito.when(addCategoryToGroupMockService.addCategoryToGroup(addCategoryToGroupRequestDTO)).thenReturn(categoryDTO);

        //Act
        CategoryDTO result = addCategoryToGroupController.addCategoryToGroup(groupStringdescription, responsibleEmail, accountStringDesignation);

        assertEquals(categoryDTO, result);
    }
}