package project.controllers.cli;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddTransactionToGroupRequestDTO;
import project.dto.TransactionDTO;
import project.dto.assemblers.AddTransactionToGroupRequestAssembler;
import project.model.group.Group;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.services.AddTransactionToGroupService;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddTransactionToGroupTest {
    @Autowired
    private AddTransactionToGroup addTransactionToGroupCtrl;
    @Autowired
    private AddTransactionToGroupService addTransactionToGroupMockService;
    @Autowired
    private GroupRepository groupRepository;

    @Test
    void addTransactionToGroup() {
        //Arrange
        PersonID personID = new PersonID(new Email("manuelesteves@gmail.com"));
        Group transactionGroup = new Group(new Description("Group for transactions"), personID);
        Double amount = 10.0;
        Description description = new Description("Transaction description");
        TransactionDate date = new TransactionDate(LocalDateTime.now());
        Category category = new Category(new Designation("Groupsie Category"));
        AccountID debit = new AccountID(new Denomination("Great Debit account"), transactionGroup.getID());
        AccountID credit = new AccountID(new Denomination("Meager Credit account"), transactionGroup.getID());
        TransactionType type = TransactionType.CREDIT;
        TransactionDTO expected = new TransactionDTO(amount, new Description("Monthly check"), date, category, debit, credit, type);

        groupRepository.save(transactionGroup);

        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO = AddTransactionToGroupRequestAssembler.mapToDTO("manuelesteves@gmail.com",
                "Group for transactions",
                10.0,
                "Transaction description",
                "Groupsie Category",
                "Great Debit account",
                "Meager Credit account",
                "CREDIT");

        Mockito.when(addTransactionToGroupMockService.addTransactionToGroup(addTransactionToGroupRequestDTO)).thenReturn(expected);

        //Act
        TransactionDTO result = addTransactionToGroupCtrl.addTransactionToGroup("manuelesteves@gmail.com",
                "Group for transactions",
                10.0,
                "Transaction description",
                "Groupsie Category",
                "Great Debit account",
                "Meager Credit account",
                "CREDIT");

        //Assert
        assertEquals(expected, result);
    }
}