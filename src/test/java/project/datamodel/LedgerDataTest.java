package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;
import project.model.shared.LedgerID;
import project.model.shared.OwnerID;
import project.model.shared.TransactionDate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LedgerDataTest {
    Long long1;
    OwnerID ownerID;
    LedgerID ledgerID;
    LedgerData ledgerData;
    TransactionData transactionData1;
    TransactionIDData transactionIdData;
    Double amount;
    Description description;
    String creditDenomination;
    String debitDenomination;
    String type;
    String categoryID;
    TransactionDate date;

    @BeforeEach
    void setUp() {
        long1 = 12434343454L;
        ownerID = new OwnerID(long1);
        ledgerID = new LedgerID(ownerID);
        ledgerData = new LedgerData(ledgerID);
        amount = 2555.3;
        description = new Description("Grocery House");
        debitDenomination = "Purchases";
        creditDenomination = "Credits";
        type = "CREDIT";
        categoryID = "Animals";
        transactionIdData = new TransactionIDData();
        date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
    }

    @Test
    void getLedgerID() {

        //Act
        LedgerID expected = new LedgerID(ownerID);
        LedgerID result = ledgerData.getLedgerID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setLedgerID() {

        //Arrange
        LedgerData ledgerData = new LedgerData();
        LedgerID expected = new LedgerID(ownerID);
        ledgerData.setLedgerID(expected);
        LedgerID result = ledgerData.getLedgerID();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setTransactions() {
        //Arrange
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData = new LedgerData(ledgerID);
        //Act
        ledgerData.setTransaction(transactionData1);
        //Assert
        assertTrue(ledgerData.getTransactions().contains(transactionData1));
    }

    @Test
    void getTransactions() {
        //Arrange
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData = new LedgerData(ledgerID);

        List<TransactionData> expected = new ArrayList<>();
        List<TransactionData> result;
        //Act
        ledgerData.setTransaction(transactionData1);
        result = ledgerData.getTransactions();
        expected.add(transactionData1);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        int expected = -1082406744;

        //Act
        int result = ledgerData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        //Act
        boolean result = ledgerData.equals(ledgerData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsFullObject() {
        //Arrange
        LedgerData ledgerData1 = new LedgerData(ledgerID);
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData1.setTransaction(transactionData1);
        ledgerData.setTransaction(transactionData1);
        //Act
        boolean result = ledgerData.equals(ledgerData1);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentLedgerID() {
        //Arrange
        long longTest = 234438857L;
        OwnerID ownerIDTest = new OwnerID(longTest);
        LedgerID ledgerIDTest = new LedgerID(ownerIDTest);
        LedgerData ledgerData1 = new LedgerData(ledgerIDTest);
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData1.setTransaction(transactionData1);
        ledgerData.setTransaction(transactionData1);
        //Act
        boolean result = ledgerData.equals(ledgerData1);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentTransactions() {
        //Arrange
        LedgerData ledgerData1 = new LedgerData(ledgerID);
        transactionData1 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData.setTransaction(transactionData1);
        amount = 999.9;
        TransactionData transactionData2 = new TransactionData(amount, description, date, categoryID, debitDenomination, creditDenomination, type, ledgerData);
        ledgerData1.setTransaction(transactionData2);
        //Act
        boolean result = ledgerData.equals(ledgerData1);
        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        LedgerData ledgerDataCopy = null;

        //Act
        boolean result = ledgerData.equals(ledgerDataCopy);

        //Assert
        assertFalse(result);
    }

    @Test
    void testToString() {
        //Arrange
        ledgerData.setTransaction(transactionData1);
        String expected = "LedgerData{ledgerID=LedgerID(id=12434343454), transactions=[project.datamodel.TransactionData@948e9f0f]}";

        //Act
        String result = ledgerData.toString();

        //Assert
        assertEquals(expected, result);
    }
}