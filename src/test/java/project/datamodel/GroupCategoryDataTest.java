package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GroupCategoryDataTest {
    @Test
    void getId() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        String expected = "Category Example";

        //Act
        String result = categoryData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        categoryData.setId("Category Example 2");

        String expected = "Category Example 2";

        //Act
        String result = categoryData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        String expected = "Category Example";

        //Act
        String result = categoryData.toString();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        //Act
        boolean result = categoryData.equals(categoryData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);
        GroupCategoryData categoryDataAlso = new GroupCategoryData("Category Example", groupData);

        //Act
        boolean result = categoryData.equals(categoryDataAlso);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);
        GroupCategoryData categoryDataAlso = null;

        //Act
        boolean result = categoryData.equals(categoryDataAlso);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        //Act
        boolean result = categoryData.equals(mariaEmail);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        int expected = Objects.hash("Category Example", groupData);

        //Act
        int result = categoryData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);

        int expected = -31151576;

        //Act
        int result = categoryData.hashCode();

        //Assert
        assertEquals(expected, result);
    }
}