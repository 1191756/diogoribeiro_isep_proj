package project;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.exceptions.RestExceptionHandler;
import project.services.*;

@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public AddAccountToGroupService addAccountToGroupMockService() {
        return Mockito.mock(AddAccountToGroupService.class);
    }

    @Bean
    @Primary
    public AddAccountToPersonService addAccountToPersonMockService() {
        return Mockito.mock(AddAccountToPersonService.class);
    }

    @Bean
    @Primary
    public AddCategoryToGroupService AddCategoryToGroupMockService() {
        return Mockito.mock(AddCategoryToGroupService.class);
    }

    @Bean
    @Primary
    public AddCategoryToPersonService AddCategoryToPersonMockService() {
        return Mockito.mock(AddCategoryToPersonService.class);
    }

    @Bean
    @Primary
    public AddMemberToGroupService addMemberToGroupMockService() {
        return Mockito.mock(AddMemberToGroupService.class);
    }

    @Bean
    @Primary
    public GetFamiliesService GetFamiliesMockService() {
        return Mockito.mock(GetFamiliesService.class);
    }

    @Bean
    @Primary
    public IsSiblingPersonService isSiblingPersonMockService() {
        return Mockito.mock(IsSiblingPersonService.class);
    }

    @Bean
    @Primary
    public AddGroupService AddGroupMockService() {
        return Mockito.mock(AddGroupService.class);
    }

    @Bean
    @Primary
    public RestExceptionHandler RestExceptionMockHandler() {
        return Mockito.mock(RestExceptionHandler.class);
    }

    @Bean
    @Primary
    public GetGroupAccountsService GetGroupAccountsMockService() {
        return Mockito.mock(GetGroupAccountsService.class);
    }

    @Bean
    @Primary
    public GetPersonGroupsAccountsService GetPersonGroupsAccountsMockService() {
        return Mockito.mock(GetPersonGroupsAccountsService.class);
    }

    @Bean
    @Primary
    public GetPersonAccountsService GetPersonAccountsMockService() {
        return Mockito.mock(GetPersonAccountsService.class);
    }

    @Bean
    @Primary
    public GetMembersService getMembersMockService() {
        return Mockito.mock(GetMembersService.class);
    }

    @Bean
    @Primary
    public GetGroupCategoriesService GetGroupCategoriesMockService() {
        return Mockito.mock(GetGroupCategoriesService.class);
    }

    @Bean
    @Primary
    public GetPersonCategoriesService GetPersonCategoriesMockService() {
        return Mockito.mock(GetPersonCategoriesService.class);
    }

    @Bean
    @Primary
    public PersonsService PersonsMockService() {
        return Mockito.mock(PersonsService.class);
    }

    @Bean
    @Primary
    public GroupsService GroupsMockService() {
        return Mockito.mock(GroupsService.class);
    }

    @Bean
    @Primary
    public GetPersonTransactionsInPeriodService GetPersonTransactionsInPeriodMockService() {
        return Mockito.mock(GetPersonTransactionsInPeriodService.class);
    }

    @Bean
    @Primary
    public GetGroupTransactionsInPeriodService GetGroupTransactionsInPeriodMockService() {
        return Mockito.mock(GetGroupTransactionsInPeriodService.class);
    }

    @Bean
    @Primary
    public AddTransactionToGroupService addTransactionToGroupMockService() {
        return Mockito.mock(AddTransactionToGroupService.class);
    }

    @Bean
    @Primary
    public CopyGroupAsMemberServiceAV copyGroupAsMemberServiceAV() {
        return Mockito.mock(CopyGroupAsMemberServiceAV.class);
    }
}


