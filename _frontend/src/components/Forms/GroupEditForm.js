import React from 'react';
import history from "../../services/history";
import {observer} from 'mobx-react';
import MaterialTextField from "../Inputs/MaterialTextField";
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import Select from 'react-select/creatable';
import makeAnimated from 'react-select/animated';
import GroupResponsiblesOptions from "../../_old/GroupResponsiblesOptions.js";
import GroupMembersOptions from "../../_old/GroupMembersOptions";
import 'mobx-react-lite/batchingForReactDom'
import GridItem from "../Grid/GridItem";
import GridContainer from "../Grid/GridContainer";
import {getLastPathValue} from "../../utils/Utils";

const navigateToGroups = () => {
    history.push('/groups');
};

const animatedComponents = makeAnimated();
const responsiblesOptions = GroupResponsiblesOptions(getLastPathValue());
const membersOptions = GroupMembersOptions(getLastPathValue());

export default observer(({form}) => (
    <form onSubmit={form.onSubmit}>
        <MaterialTextField defaultValue="teste" field={form.$('groupDescription')} fullWidth={true}
                           variant={'outlined'}/>
        <br/>
        <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
                <Select
                    {...form.$('responsibles').bind()}
                    isMulti
                    placeholder="Select the responsibles..."
                    name="responsibles"
                    closeMenuOnSelect={false}
                    components={animatedComponents}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    options={responsiblesOptions}
                    defaultValue={responsiblesOptions}
                />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
                <Select
                    defaultValue={membersOptions}
                    options={membersOptions}
                    {...form.$('members').bind()}
                    isMulti
                    placeholder="Select the members..."
                    name="members"
                    className="basic-multi-select"
                    classNamePrefix="select"

                />
            </GridItem>
        </GridContainer>
        <div style={{textAlign: 'center', marginTop: '20px'}}>
            <MaterialButton type="submit" variant="contained"
                            color="primary"
                            size="medium"
                            style={{margin: '0 10px 0 0'}}
                            startIcon={<SaveIcon/>} onClick={form.onSubmit}>Save</MaterialButton>
            <MaterialButton variant="contained"
                            size="medium"
                            startIcon={<CancelIcon/>}
                            color="default" onClick={navigateToGroups}>Cancel</MaterialButton>
        </div>
    </form>
));