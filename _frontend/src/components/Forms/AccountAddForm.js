import React from 'react';
import history from "../../services/history";
import {observer} from 'mobx-react';
import MaterialTextField from "../Inputs/MaterialTextField";
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

const navigateToAccounts = () => {
    history.push('/accounts');
};

export default observer(({form}) => (
    <form onSubmit={form.onSubmit}>
        <MaterialTextField field={form.$('denomination')} fullWidth={true} variant={'outlined'}/>
        <div style={{textAlign: 'center', marginTop: '20px'}}>
            <MaterialButton type="submit" variant="contained"
                            color="primary"
                            size="medium"
                            style={{margin: '0 10px 0 0'}}
                            startIcon={<SaveIcon/>} onClick={form.onSubmit}>Save</MaterialButton>
            <MaterialButton variant="contained"
                            size="medium"
                            startIcon={<CancelIcon/>}
                            color="default" onClick={navigateToAccounts}>Cancel</MaterialButton>
        </div>
    </form>
));