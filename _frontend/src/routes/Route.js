import React, {useContext} from "react";
import PropTypes from "prop-types";
import {Redirect, Route} from "react-router-dom";

import DefaultLayout from "../views/_layouts/default";
import AuthLayout from "../views/_layouts/auth";
import AppContext from "../context/AppContext";

export default function RouteWrapper({
                                         component: Component,
                                         isPrivate,
                                         ...rest
                                     }) {
    const {state} = useContext(AppContext);
    const {isLogged} = state;
    /*
    useEffect(() => {
        //TODO Change to User logged in, in the next Sprint Review
        dispatch(fetchUserStarted());
        fetch(`${URL_API}/persons/tarcisio@family.com`)
            .then(res => res.json())
            .then(res => dispatch(fetchUserSuccess(res)))
            .catch(err => dispatch(fetchUserFailure(err.message)))
        ;
    }, []);
    */
    /**
     * Redirect user to SignIn page if he tries to access a private route
     * without authentication.
     */
    if (isPrivate && !isLogged) {
        return <Redirect to="/"/>;
    }

    /**
     * Redirect user to Main page if he tries to access a non private route
     * (SignIn or SignUp) after being authenticated.
     */
    if (!isPrivate && isLogged) {
        return <Redirect to="/dashboard"/>;
    }

    const Layout = isLogged ? DefaultLayout : AuthLayout;

    /**
     * If not included on both previous cases, redirect user to the desired route.
     */
    return (
        <Route
            {...rest}
            render={props => (
                <Layout>
                    <Component {...props} />
                </Layout>
            )}
        />
    );
}

RouteWrapper.propTypes = {
    isPrivate: PropTypes.bool,
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

RouteWrapper.defaultProps = {
    isPrivate: false
};
