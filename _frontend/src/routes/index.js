import React from "react";
import {Switch} from "react-router-dom";
import Route from "./Route";

import SignIn from "../views/SignIn";
import SignUp from "../views/SignUp";

import Dashboard from "../views/Dashboard";
import Profile from "../views/Profile";
import Transactions from "../views/Transactions";
import Groups from "../views/Groups";
import AddGroups from "../views/Groups/Add";
import EditGroups from "../views/Groups/Edit";
import Accounts from "../views/Accounts";
import AddAccounts from "../views/Accounts/Add";
import AddGroupAccounts from "../views/Accounts/GroupAdd"
import Categories from "../views/Categories";
import AddGroupTransactionForm from "../views/Transactions/Add";
import GroupsTransactions from "../views/Transactions/View";
import AddCategories from "../views/Categories/Add";
import AddGroupCategoriesForm from "../views/Categories/GroupAdd";

export default function Routes() {
        return (
            <Switch>
                    <Route path="/" exact component={SignIn}/>
                    <Route path="/register" component={SignUp}/>

                    <Route exact path="/categories/add" component={AddCategories} isPrivate/>
                    <Route exact path="/categories/groupAdd" component={AddGroupCategoriesForm} isPrivate/>
                    <Route path="/dashboard" component={Dashboard} isPrivate/>
                    <Route exact path="/profile" component={Profile} isPrivate/>
                    <Route exact path="/groups" component={Groups} isPrivate/>
                    <Route exact path="/groups/add" component={AddGroups} isPrivate/>
                    <Route exact path="/groups/edit/:id" component={EditGroups} isPrivate/>
                    <Route exact path="/accounts" component={Accounts} isPrivate/>
                    <Route exact path="/accounts/add" component={AddAccounts} isPrivate/>
                    <Route exact path="/accounts/groupAdd" component={AddGroupAccounts} isPrivate/>
                    <Route exact path="/categories" component={Categories} isPrivate/>
                    <Route exact path="/categories/add" component={AddCategories} isPrivate/>
                    <Route exact path="/transactions/view/:id" component={GroupsTransactions} isPrivate/>
                    <Route exact path="/transactions" component={Transactions} isPrivate/>
                    <Route exact path="/transaction/add/:id" component={AddGroupTransactionForm} isPrivate/>
                    {/*<Route exact path="/transactions/edit/:id" component={EditTransactions} isPrivate/>*/}

                    {/* redirect user to SignIn page if route does not exist and user is not authenticated */}
                    <Route component={SignIn}/>
            </Switch>
    );
}
