import history from "../../../services/history";
import GridContainer from "../../../components/Grid/GridContainer";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import React, {useContext} from "react";
import {withFormik} from "formik";
import * as Yup from "yup";
import {toast} from "react-toastify";
import {css} from "glamor";
import {TextInput} from "../../../components/Inputs/TextInput";
import MaterialButton from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import {URL_API} from "../../../actions/Actions";
import AppContext from "../../../context/AppContext";
import {Label} from "../../../components/Inputs/Label";
import {ReactSelect} from "../../../components/Inputs/ReactSelect";
import Groups from "../../Groups";

const navigateToCategories = () => {
    history.push('/Categories');
};

let groupsList = [];

let groupID = '';

export default function AddGroupCategoriesForm(props) {
    const {state} = useContext(AppContext);
    const {groups} = state;
    const {data} = groups;
    groupsList = [];

    for (let j = 0; j < data.length; j++) {
        groupsList.push({
            value: data[j].groupDescription, label: data[j].groupDescription
        })
    }
    console.log(groupsList);

    return <GridContainer key={'addCategoryGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add category</h2>
                    <MyEnhancedForm key={'addCategoryForm'} groupCategory={{designation: ''}}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}

const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        designation: Yup.string()
            .min(2, "Category designation should be longer than 2")
            .required('Category designation is required.'),
    }),

    mapPropsToValues: ({category}) => ({
        ...category,
    }),
    handleSubmit: (payload, {setSubmitting, resetForm}) => {
        payload.responsibleEmail = 'jose@family.com';
        let data = payload;
        console.log(payload.group.value);
        groupID = payload.group.value;
        fetch(`${URL_API}/groups/${groupID}/categories`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data) + '"responsibleEmail":"Friends"'
            })
            .then(function (res) {
                toast.success("Category saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#28a745',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/categories')
            })
            .catch(function (res) {
                toast.error(res.message);
            });
        setSubmitting(false);
    },
    displayName: 'AddGroupCategoryForm',
});

const AddGroupCategoryForm = props => {
    const {
        values,
        touched,
        errors,
        dirty,
        setFieldValue,
        setFieldTouched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={'AddGroupCategoryFormTag'}>
            <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Group</Label>
                    <ReactSelect
                        id={`groupSelect`}
                        name="group"
                        error={touched.groupDescription && errors.groupDescription}
                        options={groupsList}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
            </GridContainer>

            <TextInput
                key={values.id}
                id="designation"
                name="designation"
                type="text"
                label="Designation"
                placeholder="Insert category designation..."
                error={touched.designation && errors.designation}
                value={values.designation}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary" onClick{...Groups()}
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                id="saveNewGroup"
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToCategories}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const MyEnhancedForm = formikEnhancer(AddGroupCategoryForm);