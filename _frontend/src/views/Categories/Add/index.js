import React from 'react';
import {URL_API} from "../../../actions/Actions";
import history from "../../../services/history";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import GridContainer from "../../../components/Grid/GridContainer";
import {withFormik} from 'formik';
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import {TextInput} from "../../../components/Inputs/TextInput";
import '../../../assets/css/formik.css'
import * as Yup from 'yup';
import {toast} from "react-toastify";
import {css} from "glamor";

const navigateToCategories = () => {
    history.push('/Categories');
};

export default function AddCategoriesForm(props) {
    return <GridContainer key={'addCategoryGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add category</h2>
                    <MyEnhancedForm key={'addCategoryForm'} category={{designation: ''}}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}
const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        designation: Yup.string()
            .min(2, "The category designation should be longer than 2")
            .required('Category designation is required.'),
    }),

    mapPropsToValues: ({category}) => ({
        ...category,
    }),
    handleSubmit: (payload, {setSubmitting, resetForm}) => {
        let data = payload;
        console.log(payload)
        data.personID = 'jose@family.com';
        fetch(`${URL_API}/persons/${data.personID}/categories`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then(function (res) {
                toast.success("Designation saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#28a745',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/categories')
            })
            .catch(function (res) {
                toast.error(res.message);
            });
        setSubmitting(false);
    },
    displayName: 'AddCategoryForm',
});

const AddCategoryForm = props => {
    const {
        values,
        touched,
        errors,
        dirty,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={'AddCategoryFormTag'}>
            {console.log(touched, errors, values, dirty)}
            <TextInput
                key={values.id}
                id="designation"
                name="designation"
                type="text"
                label="Designation"
                placeholder="Insert category designation..."
                error={touched.designation && errors.designation}
                value={values.designation}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToCategories}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const MyEnhancedForm = formikEnhancer(AddCategoryForm);