import React from 'react';
import {URL_API} from "../../../actions/Actions";
import history from "../../../services/history";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import GridContainer from "../../../components/Grid/GridContainer";
import {withFormik} from 'formik';
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import {TextInput} from "../../../components/Inputs/TextInput";
import '../../../assets/css/formik.css'
import * as Yup from 'yup';
import {toast} from "react-toastify";
import {css} from "glamor";

const navigateToAccounts = () => {
    history.push('/Accounts');
};

export default function AddAccounts(props) {
    return <GridContainer key={'addAccountGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add account</h2>
                    <MyEnhancedForm key={'addAccountForm'} account={{denomination: ''}}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}
const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        denomination: Yup.string()
            .min(2, "The account denomination should be longer than 2")
            .required('Account denomination is required.'),
        description: Yup.string()
            .min(2, "The account description should be longer than 2")
            .required('Account description is required.')
    }),

    mapPropsToValues: ({account}) => ({
        ...account,
    }),
    handleSubmit: (payload, {setSubmitting, resetForm}) => {
        let data = payload;
        data.ownerID = 'jose@family.com';
        fetch(`${URL_API}/persons/${data.ownerID}/accounts`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then(function (res) {
                toast.success("Account saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#007E33',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/accounts')
            })
            .catch(function (res) {
                toast.error(res.message);
            });
        setSubmitting(false);
    },
    displayName: 'AddAccountForm',
});

const AddAccountForm = props => {
    const {
        values,
        touched,
        errors,
        dirty,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={'AddAccountFormTag'}>
            {console.log(touched, errors, values, dirty)}
            <TextInput
                key={values.id}
                id="denomination"
                name="denomination"
                type="text"
                label="Denomination"
                placeholder="Insert account denomination..."
                error={touched.denomination && errors.denomination}
                value={values.denomination}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <TextInput
                key={values.id}
                id="description"
                name="description"
                type="text"
                label="Description"
                placeholder="Insert account description..."
                error={touched.description && errors.description}
                value={values.description}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToAccounts}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const MyEnhancedForm = formikEnhancer(AddAccountForm);