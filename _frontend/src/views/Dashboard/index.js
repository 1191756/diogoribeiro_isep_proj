import React, {useContext, useEffect} from "react";
import {
    fetchHypermediaFailure,
    fetchHypermediaStarted,
    fetchHypermediaSuccess,
    fetchLinesDashboardFailure,
    fetchLinesDashboardStarted,
    fetchLinesDashboardSuccess,
    fetchPersonsFailure,
    fetchPersonsStarted,
    fetchPersonsSuccess,
    fetchPieDashboardFailure,
    fetchPieDashboardStarted,
    fetchPieDashboardSuccess,
    fetchTransactionsDashboardFailure,
    fetchTransactionsDashboardStarted,
    fetchTransactionsDashboardSuccess,
    URL_API
} from "../../actions/Actions";
import AppContext from "../../context/AppContext";
import {ResponsivePie} from '@nivo/pie';
import {ResponsiveLine} from '@nivo/line';
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import CardBody from "../../components/Card/CardBody";
import Card from "../../components/Card/Card";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import {toast} from "react-toastify";
import {css} from "glamor";
import Table from "../../components/Table/Table";

export default function Dashboard() {
    const {state, dispatch} = useContext(AppContext);
    const {user, dashboardpie, dashboardlines, hypermedia} = state;
    const {data} = hypermedia;

    useEffect(() => {
        dispatch(fetchHypermediaStarted());
        fetch(`${URL_API}/dashboard/${user.personID}`)
            .then(res => res.json())
            .then(res => dispatch(fetchHypermediaSuccess(res)))
            .catch(err => dispatch(fetchHypermediaFailure(err.message)))
    }, [dispatch]);
    useEffect(() => {
        dispatch(fetchPieDashboardStarted());
        fetch(`${URL_API}/dashboard/${user.personID}/pie/`)
            .then(res => res.json())
            .then(res => dispatch(fetchPieDashboardSuccess(res)))
            .catch(err => dispatch(fetchPieDashboardFailure(err.message)))
    }, [dispatch, user.personID]);

    useEffect(() => {
        dispatch(fetchLinesDashboardStarted());
        fetch(`${URL_API}/dashboard/${user.personID}/lines/`)
            .then(res => res.json())
            .then(res => dispatch(fetchLinesDashboardSuccess(res)))
            .catch(err => dispatch(fetchLinesDashboardFailure(err.message)))
    }, [dispatch, user.personID]);

    useEffect(() => {
        dispatch(fetchPersonsStarted());
        fetch(`${URL_API}/persons`)
            .then(res => res.json())
            .then(res => dispatch(fetchPersonsSuccess(res)))
            .catch(err => dispatch(fetchPersonsFailure(err.message)))
    }, [dispatch]);

    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    let linesData = [
        {
            "id": monthNames[new Date().getMonth()],
            "data": dashboardlines.data
        }];
    let personalTransactions = PersonalTransactionsInPeriod();
    return (
        <GridContainer>
            <GridItem xs={12} sm={12} md={12} style={{marginTop: '-30px'}}>
                <Card>
                    <CardBody>
                        <h2 style={{marginTop: '0'}}>Welcome to Personal Finances from Group 5!</h2>
                        <p>This site is a frontend showcase of all the user stories developed, by Group 5, during SWitCH
                            2019.</p>
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6} style={{marginTop: '-30px'}}>
                <Card>
                    <CardBody style={{height: '350px', paddingBottom: '50px'}}>
                        <h2 style={{marginTop: '0'}}>Categories used in transactions</h2>
                        <ResponsivePie
                            data={dashboardpie.data}
                            margin={{top: 20, right: 30, bottom: 80, left: 0}}
                            innerRadius={0.5}
                            padAngle={0.7}
                            cornerRadius={3}
                            colors={{scheme: 'paired'}}
                            borderWidth={1}
                            background={{from: 'color', modifiers: [['darker', 0.5]]}}
                            borderColor={{from: 'color', modifiers: [['darker', 0.2]]}}
                            radialLabelsSkipAngle={10}
                            radialLabelsTextXOffset={6}
                            radialLabelsTextColor="#333333"
                            radialLabelsLinkOffset={0}
                            radialLabelsLinkDiagonalLength={16}
                            radialLabelsLinkHorizontalLength={24}
                            radialLabelsLinkStrokeWidth={1}
                            radialLabelsLinkColor={{from: 'color'}}
                            slicesLabelsSkipAngle={10}
                            slicesLabelsTextColor="#333333"
                            animate={true}
                            motionStiffness={90}
                            motionDamping={15}
                            legends={[
                                {
                                    anchor: 'top-right',
                                    direction: 'column',
                                    translateY: 56,
                                    itemWidth: 80,
                                    itemHeight: 22,
                                    itemTextColor: '#999',
                                    symbolSize: 18,
                                    symbolShape: 'square',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemTextColor: '#000'
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6} style={{marginTop: '-30px'}}>
                <Card>
                    <CardBody style={{height: '350px', paddingBottom: '50px'}}>
                        <h2 style={{marginTop: 0, marginBottom: 0}}>Last transactions values per day</h2>
                        <ResponsiveLine
                            data={linesData}
                            margin={{top: 50, right: 110, bottom: 50, left: 60}}
                            xScale={{type: 'point'}}
                            yScale={{type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false}}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                orient: 'bottom',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'date',
                                legendOffset: 36,
                                legendPosition: 'middle'
                            }}
                            axisLeft={{
                                orient: 'left',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'value',
                                legendOffset: -40,
                                legendPosition: 'middle'
                            }}
                            colors={{scheme: 'paired'}}
                            pointSize={10}
                            pointColor={{theme: 'background'}}
                            pointBorderWidth={2}
                            pointBorderColor={{from: 'serieColor'}}
                            pointLabel="y"
                            pointLabelYOffset={-12}
                            useMesh={true}
                            legends={[
                                {
                                    anchor: 'bottom-right',
                                    direction: 'column',
                                    justify: false,
                                    translateX: 100,
                                    translateY: 0,
                                    itemsSpacing: 0,
                                    itemDirection: 'left-to-right',
                                    itemWidth: 80,
                                    itemHeight: 20,
                                    itemOpacity: 0.75,
                                    symbolSize: 12,
                                    symbolShape: 'square',
                                    symbolBorderColor: 'rgba(0, 0, 0, .5)',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemBackground: 'rgba(0, 0, 0, .03)',
                                                itemOpacity: 1
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={12} style={{marginTop: '-30px'}}>
                <Card>
                    <CardBody style={{paddingBottom: '50px'}}>
                        <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                                <h2 style={{marginTop: 0, marginBottom: 0}}>Transactions in the last 2 weeks</h2>
                                {personalTransactions}
                            </GridItem>
                        </GridContainer>

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    );
}

function PersonalTransactionsInPeriod(props) {
    const {state, dispatch} = useContext(AppContext);
    const {dashboardtransactions, accounts, user} = state;
    const {loading, error, data} = dashboardtransactions;
    const {personID} = user;

    //TODO Change to be a dropdown selection
    //let accountID = accounts.data[props.match.params.id].denomination;
    let accountID = "General spending account";
    let todayDate = new Date();
    let weekBeforeDate = new Date();
    weekBeforeDate.setDate(todayDate.getDate() - 15);
    let todayDateStr = todayDate.toISOString().substring(0, 10);
    let weekBeforeDateStr = weekBeforeDate.toISOString().substring(0, 10);
    useEffect(() => {
        dispatch(fetchTransactionsDashboardStarted());
        //fetch(`${URL_API}/persons/${personID}/accounts/${accountID}/transactions/ 00:00/`)
        fetch(`http://localhost:8080/persons/jose@family.com/accounts/${accountID}/transactions/${weekBeforeDateStr} 00:00/${todayDateStr} 00:00`)
            .then(res => res.json())
            .then(res => dispatch(fetchTransactionsDashboardSuccess(res)))
            .catch(err => dispatch(fetchTransactionsDashboardFailure(err.message)))
        ;
    }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Date',
                accessor: 'date',
                Cell: ({row}) => (
                    <>{row.values.date.substring(0, 10)}</>
                )
            }, {
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: 'Category',
                accessor: 'category'
            }, {
                Header: 'Debit',
                accessor: 'debit'
            }, {
                Header: 'Credit',
                accessor: 'credit'
            }, {
                Header: 'Type',
                accessor: 'type'
            }, {
                Header: 'Amount',
                accessor: 'amount',
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        {row.values.amount}
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <div style={{textAlign: 'center', marginBottom: '20px'}}>
                        </div>
                        <Table columns={columns} data={data}/>
                        <div style={{textAlign: 'center', marginTop: '20px'}}>
                        </div>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <div style={{textAlign: 'center', marginBottom: '20px'}}>
                    </div>
                    <div>
                        You haven't created any transactions yet.
                    </div>
                    <div style={{textAlign: 'center', marginTop: '20px'}}>
                    </div>
                </GridItem>;
            }
        }
    }
    return result;
}


