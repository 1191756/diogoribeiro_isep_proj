import {
    DELETE_GROUP,
    FETCH_GROUP_TRANSACTIONS_FAILURE,
    FETCH_GROUP_TRANSACTIONS_STARTED,
    FETCH_GROUP_TRANSACTIONS_SUCCESS,
    FETCH_GROUPS_FAILURE,
    FETCH_GROUPS_STARTED,
    FETCH_GROUPS_SUCCESS,
    FETCH_HYPERMEDIA_FAILURE,
    FETCH_HYPERMEDIA_STARTED,
    FETCH_HYPERMEDIA_SUCCESS,
    FETCH_LINES_DASHBOARD_FAILURE,
    FETCH_LINES_DASHBOARD_STARTED,
    FETCH_LINES_DASHBOARD_SUCCESS,
    FETCH_PERSONAL_TRANSACTIONS_FAILURE,
    FETCH_PERSONAL_TRANSACTIONS_STARTED,
    FETCH_PERSONAL_TRANSACTIONS_SUCCESS,
    FETCH_PERSONS_FAILURE,
    FETCH_PERSONS_STARTED,
    FETCH_PERSONS_SUCCESS,
    FETCH_PIE_DASHBOARD_FAILURE,
    FETCH_PIE_DASHBOARD_STARTED,
    FETCH_PIE_DASHBOARD_SUCCESS,
    FETCH_TRANSACTIONS_DASHBOARD_FAILURE,
    FETCH_TRANSACTIONS_DASHBOARD_STARTED,
    FETCH_TRANSACTIONS_DASHBOARD_SUCCESS,
    FETCH_TRANSACTIONS_FAILURE,
    FETCH_TRANSACTIONS_STARTED,
    FETCH_TRANSACTIONS_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_USER_STARTED,
    FETCH_USER_SUCCESS,
    LOGGEDIN,
    LOGGEDOUT,
} from '../actions/Actions'

import {
    FETCH_ACCOUNTS_FAILURE,
    FETCH_ACCOUNTS_GROUP_FAILURE,
    FETCH_ACCOUNTS_GROUP_STARTED,
    FETCH_ACCOUNTS_GROUP_SUCCESS,
    FETCH_ACCOUNTS_STARTED,
    FETCH_ACCOUNTS_SUCCESS
} from '../actions/AccountActions'

import {
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_GROUP_FAILURE,
    FETCH_CATEGORIES_GROUP_STARTED,
    FETCH_CATEGORIES_GROUP_SUCCESS,
    FETCH_CATEGORIES_STARTED,
    FETCH_CATEGORIES_SUCCESS
} from "../actions/CategoryActions";

function reducer(state, action) {
    switch (action.type) {
        case FETCH_HYPERMEDIA_STARTED:
            return {
                ...state,
                hypermedia: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_HYPERMEDIA_SUCCESS:
            return {
                ...state,
                hypermedia: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_HYPERMEDIA_FAILURE:
            return {
                ...state,
                hypermedia: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_GROUPS_STARTED:
            return {
                ...state,
                groups: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUPS_SUCCESS:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUPS_FAILURE:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case DELETE_GROUP:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: null,
                    data: [],
                }
            };
        case LOGGEDIN:
            return {
                ...state,
                isLogged: true,
            };
        case LOGGEDOUT:
            return {
                ...state,
                isLogged: false,
            };
        case FETCH_PERSONS_STARTED:
            return {
                ...state,
                persons: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PERSONS_SUCCESS:
            return {
                ...state,
                persons: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PERSONS_FAILURE:
            return {
                ...state,
                persons: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_USER_STARTED:
            return {
                ...state,
                user: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                user: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_USER_FAILURE:
            return {
                ...state,
                user: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_ACCOUNTS_STARTED:
            return {
                ...state,
                accounts: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_ACCOUNTS_FAILURE:
            return {
                ...state,
                accounts: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_TRANSACTIONS_STARTED:
            return {
                ...state,
                transactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_TRANSACTIONS_FAILURE:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_PERSONAL_TRANSACTIONS_STARTED:
            return {
                ...state,
                transactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PERSONAL_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PERSONAL_TRANSACTIONS_FAILURE:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_GROUP_TRANSACTIONS_STARTED:
            return {
                ...state,
                groupTransactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                groupTransactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_TRANSACTIONS_FAILURE:
            return {
                ...state,
                groupTransactions: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_ACCOUNTS_GROUP_STARTED:
            return {
                ...state,
                groupAccounts: {
                    loading: true,
                    error: null,
                    data: [],
                }
            };
        case FETCH_ACCOUNTS_GROUP_SUCCESS:
            return {
                ...state,
                groupAccounts: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_ACCOUNTS_GROUP_FAILURE:
            return {
                ...state,
                groupAccounts: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_CATEGORIES_STARTED:
            return {
                ...state,
                categories: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_CATEGORIES_FAILURE:
            return {
                ...state,
                categories: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_CATEGORIES_GROUP_STARTED:
            return {
                ...state,
                groupCategories: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_CATEGORIES_GROUP_SUCCESS:
            return {
                ...state,
                groupCategories: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_CATEGORIES_GROUP_FAILURE:
            return {
                ...state,
                groupCategories: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_PIE_DASHBOARD_STARTED:
            return {
                ...state,
                dashboardpie: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_PIE_DASHBOARD_SUCCESS:
            return {
                ...state,
                dashboardpie: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_PIE_DASHBOARD_FAILURE:
            return {
                ...state,
                dashboardpie: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_LINES_DASHBOARD_STARTED:
            return {
                ...state,
                dashboardlines: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_LINES_DASHBOARD_SUCCESS:
            return {
                ...state,
                dashboardlines: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_LINES_DASHBOARD_FAILURE:
            return {
                ...state,
                dashboardlines: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_TRANSACTIONS_DASHBOARD_STARTED:
            return {
                ...state,
                dashboardtransactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_TRANSACTIONS_DASHBOARD_SUCCESS:
            return {
                ...state,
                dashboardtransactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_TRANSACTIONS_DASHBOARD_FAILURE:
            return {
                ...state,
                dashboardtransactions: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        default:
            return state
    }
}

export default reducer;
