export const getLastPathValue = () => {
    let pathname = window.location.pathname;
    pathname = pathname.substring(pathname.lastIndexOf('/') + 1);

    return pathname;
}
export function findWithAttr(array, attr, value) {
    for (var i = 0; i < array.length; i += 1) {
        if (array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}
